drop SEQUENCE T_GEN_MSG;
CREATE SEQUENCE SEQ_T_GEN_MSG  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 7721 NOCACHE  NOORDER  NOCYCLE ;
drop table T_GEN_MSG;

-- Create table
create table T_GEN_MSG
(
  msgkey         VARCHAR2(10) not null,
  msgcategory    VARCHAR2(30) default ' ' not null,
  msgid          VARCHAR2(50) default ' ' not null,
  msgtitle       VARCHAR2(100) default ' ' not null,
  msgtext        VARCHAR2(4000) default ' ' not null,
  attachfile     BLOB,
  udf1           VARCHAR2(50) default ' ' not null,
  udf2           VARCHAR2(50) default ' ' not null,
  udf3           VARCHAR2(50) default ' ' not null,
  udf4           NUMBER(10) default 0 not null,
  udf5           NUMBER(10) default 0 not null,
  receiver1      VARCHAR2(30) default ' ' not null,
  receiver2      VARCHAR2(30) default ' ' not null,
  receiver3      VARCHAR2(50) default ' ' not null,
  dingdinglink   VARCHAR2(200) default ' ' not null,
  dingdingsecret VARCHAR2(200) default ' ' not null,
  status         NUMBER(10) default 0 not null,
  type           VARCHAR2(10) default 'STD' not null,
  adddate        DATE default SYSDATE not null,
  addwho         VARCHAR2(30) default USER not null,
  editdate       DATE default SYSDATE not null,
  editwho        VARCHAR2(30) default USER not null
)
comment on column T_GEN_MSG.msgkey
  is '信息键值';
comment on column T_GEN_MSG.msgcategory
  is '分类，标识信息类型';
comment on column T_GEN_MSG.msgid
  is '主键';
comment on column T_GEN_MSG.msgtitle
  is '信息标题';
comment on column T_GEN_MSG.msgtext
  is '信息文本';
comment on column T_GEN_MSG.attachfile
  is '信息附件';
comment on column T_GEN_MSG.udf1
  is '自定义1（保留，暂未使用）';
comment on column T_GEN_MSG.udf2
  is '自定义2（保留，暂未使用）';
comment on column T_GEN_MSG.udf3
  is '自定义3（保留，暂未使用）';
comment on column T_GEN_MSG.udf4
  is '自定义4（保留，暂未使用）';
comment on column T_GEN_MSG.udf5
  is '自定义5（保留，暂未使用）';
comment on column T_GEN_MSG.receiver1
  is '收件人1（主收件人）';
comment on column T_GEN_MSG.receiver2
  is '收件人2（抄送人）';
comment on column T_GEN_MSG.receiver3
  is '收件人3（密送人）';
comment on column T_GEN_MSG.dingdinglink
  is '钉钉信息URL';
comment on column T_GEN_MSG.dingdingsecret
  is '钉钉密钥';
comment on column T_GEN_MSG.status
  is '状态（保留，暂未使用）';
comment on column T_GEN_MSG.type
  is '类别（保留，暂未使用）';
/
CREATE OR REPLACE TRIGGER TRG_T_GEN_MSG BEFORE INSERT ON T_GEN_MSG
FOR EACH ROW
BEGIN SELECT TRIM(TO_CHAR(SEQ_T_GEN_MSG.NEXTVAL,'0000000000')) INTO :NEW.MSGKEY FROM DUAL; END;

