package com.pgl.ss.autoexecutor.automsg;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

//import com.pgl.ss.autoexecutor.autotask.AutoTaskData;
public interface AutoMsgDAO {

	/*long insertDataByView(
			@Param("addwho")String addwho,
			@Param("executor")String executor,
			@Param("viewname")String viewname);*/

	List<Map> getDataByView(
			@Param("viewname")String viewname,
			@Param("key1") String key1,
			@Param("key2") String key2,
			@Param("key3") String key3,
			@Param("k_1") String k_1,
			@Param("k_2") String k_2,
			@Param("k_3") String k_3,
			@Param("orderStatement") String orderStatement);

	MessageData getOneMessageData(    		
			@Param("viewname")String viewname);

	MessageData getOneMessageData2(
			@Param("viewname")String viewname,
			@Param("key1")String key1,
			@Param("key2") String key2,
			@Param("key3") String key3);
	
	List<MessageData> getMessageData(    		
			@Param("viewname")String viewname);

	@SuppressWarnings("rawtypes")
	List<Map> getAttachmentData(
			@Param("viewname")String viewname);
	
	MessageData getOneMessageDataWithKey(    		
			@Param("viewname")String viewname,
			@Param("key1")String key1,
			@Param("key2")String key2,
			@Param("key3")String key3);
	
	List<MessageData> getMessageDataWithKey(    		
			@Param("viewname")String viewname,
			@Param("key1")String key1,
			@Param("key2")String key2,
			@Param("key3")String key3);
}
