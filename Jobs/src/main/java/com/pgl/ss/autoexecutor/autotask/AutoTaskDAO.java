package com.pgl.ss.autoexecutor.autotask;

import java.rmi.MarshalledObject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;




public interface AutoTaskDAO {
	Map getTaskByKey(@Param("viewname") String viewname,
											 @Param("key1") String key1,
											 @Param("key2") String key2,
											 @Param("key3") String key3,
											 @Param("key4") String key4,
											 @Param("key5") String key5);

	List<Map> getRuleData(@Param("viewname") String viewname,
													@Param("key1") String key1,
													@Param("key2") String key2,
													@Param("key3") String key3,
													@Param("key4") String key4,
													@Param("key5") String key5,
													@Param("orderStatement")String orderStatement);

    Map<String, String> getOneMap(@Param("viewname")String viewname);

	long updateStatusByKey2(
			@Param("table") String table,
			@Param("tableRule") String tableRule,
			@Param("statusColumn") String statusColumn,
			@Param("status")Integer status,
			@Param("editwho")String editwho,
			@Param("executor")String executor,
			@Param("notes")String notes,
			@Param("autotaskkey")String autotaskkey);

	long updateStatusByCategory2(
			@Param("table") String table,
			@Param("tableRule") String tableRule,
			@Param("statusColumn") String statusColumn,
			@Param("tstatus")Integer tstatus,
			@Param("editwho")String editwho,
			@Param("executor")String executor,
			@Param("notes")String notes,
			@Param("batchgroup") String batchgroup,
			@Param("taskcategory") String taskcategory,
			@Param("fstatus") Integer fstatus);

	List<Map<String, String>> getData2(
			@Param("table") String table,
			@Param("tableRule") String tableRule,
			@Param("statusColumn") String statusColumn,
			@Param("taskcategory")String taskcategory,
			@Param("status")Integer status,
			@Param("executor")String executor);

	long insertDataByView2(
			@Param("table") String table,
			@Param("tableRule") String tableRule,
			@Param("statusColumn") String statusColumn,
			@Param("addwho")String addwho,
			@Param("executor")String executor,
			@Param("viewname")String viewname);

	Map<String, Object> executeProduce2(@Param("paramMap")Map<String, Object> paramMap);
	Map<String, Object> executeProduce3(@Param("paramMap")Map<String, Object> paramMap);

	long updateStatusByCategory(
			@Param("tstatus")Integer tstatus,
			@Param("editwho")String editwho,
			@Param("executor")String executor,
			@Param("notes")String notes,
			@Param("batchgroup") String batchgroup, 
			@Param("taskcategory") String taskcategory,
			@Param("fstatus") Integer fstatus);
	long delete(    		
			@Param("autotaskkey")String autotaskkey);

	@SuppressWarnings("rawtypes")
	List<Map> getDataByView(
			@Param("viewname")String viewname,
			@Param("taskcategory")String taskcategory,
			@Param("status")Integer status,
			@Param("executor")String executor);
	
	List<AutoTaskData> getData(  
			@Param("taskcategory")String taskcategory,
			@Param("status")Integer status,
			@Param("executor")String executor);
	
	AutoTaskData getOneDataByStatus(    		
			@Param("taskcategory")String taskcategory,
			@Param("status")Integer status);

	AutoTaskData getOneUnProcData(    		
			@Param("taskcategory")String taskcategory,
			@Param("executor")String executor);

	ResultData getCkeckResult(    		
			@Param("viewname")String viewname,
			@Param("autotaskkey")String autotaskkey);
	
	long updateStatusByKey(
			@Param("status")Integer status,
			@Param("editwho")String editwho,
			@Param("executor")String executor,
			@Param("notes")String notes,
			@Param("autotaskkey")String autotaskkey);

	/**
	 * 更新推送结果和失败次数
	 * add by lzw 2021-07-26
	 */
    long updateFailureCountByKey(
            @Param("status")Integer status,
            @Param("editwho")String editwho,
            @Param("notes")String notes,
            @Param("autotaskkey")String autotaskkey);
	
	long insertData(	
			@Param("key1")String key1,
			@Param("key2")String key2,
			@Param("key3")String key3,
			@Param("taskcategory")String taskcategory,
			@Param("status")Integer status,
			@Param("deptask")String deptask,
			@Param("expiredate")String expiredate,
			@Param("taskflag1")String taskflag1,
			@Param("taskflag2")String taskflag2,
			@Param("taskflag3")String taskflag3,
			@Param("taskflag4")String taskflag4,
			@Param("taskflag5")String taskflag5, 
			@Param("addwho")String addwho,  
			@Param("executor")String executor,  
			@Param("batchgroup")String batchgroup,
			@Param("notes")String notes);


	long updateStatusByView(    		
			@Param("status")Integer status,
			@Param("editwho")String editwho,
			@Param("executor")String executor,
			@Param("notes")String notes,
			@Param("viewname")String viewname);
	
	long insertDataByView(	
			@Param("addwho")String addwho,
			@Param("executor")String executor,
			@Param("viewname")String viewname);
	
	long updateData(	
			@Param("key1")String key1,
			@Param("key2")String key2,
			@Param("key3")String key3,
			@Param("taskcategory")String taskcategory,
			@Param("status")Integer status,
			@Param("deptask")String deptask,
			@Param("failurecount")String failurecount,
			@Param("expiredate")String expiredate,
			@Param("taskflag1")String taskflag1,
			@Param("taskflag2")String taskflag2,
			@Param("taskflag3")String taskflag3,
			@Param("taskflag4")String taskflag4,
			@Param("taskflag5")String taskflag5, 
			@Param("editwho")String editwho, 
			@Param("executor")String executor,  
			@Param("batchgroup")String batchgroup,
			@Param("autotaskkey")String autotaskkey,
			@Param("notes")String notes);

	long insertEmptyData(	
			@Param("key1")String key1,
			@Param("key2")String key2,
			@Param("key3")String key3,
			@Param("taskcategory")String taskcategory,
			@Param("addwho")String addwho);
}