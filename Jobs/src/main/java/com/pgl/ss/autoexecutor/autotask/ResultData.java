package com.pgl.ss.autoexecutor.autotask;

public class ResultData {
	  public String getKey1() {
		return key1;
	}
	public void setKey1(String key1) {
		this.key1 = key1;
	}
	public String getKey2() {
		return key2;
	}
	public void setKey2(String key2) {
		this.key2 = key2;
	}
	public String getKey3() {
		return key3;
	}
	public void setKey3(String key3) {
		this.key3 = key3;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "RunResult [key1=" + key1 + ", key2=" + key2 + ", key3=" + key3 + ", category=" + category + ", code="
				+ code + ", message=" + message + "]";
	}
	
	private String key1;
	private String key2;
	private String key3;
	private String category;
	private Integer code;
	private String message;
}
