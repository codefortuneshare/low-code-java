package com.pgl.ss.autoexecutor.automsg;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class MessageData implements Serializable {
	private static final long serialVersionUID = 1L;

	private String sheetName;


	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getReveiver1() {
		return reveiver1;
	}
	public void setReveiver1(String reveiver1) {
		this.reveiver1 = reveiver1;
	}
	public String getReveiver2() {
		return reveiver2;
	}
	public void setReveiver2(String reveiver2) {
		this.reveiver2 = reveiver2;
	}
	public String getReveiver3() {
		return reveiver3;
	}
	public void setReveiver3(String reveiver3) {
		this.reveiver3 = reveiver3;
	}
	public String getUdf01() {
		return udf01;
	}
	public void setUdf01(String udf01) {
		this.udf01 = udf01;
	}
	public String getUdf02() {
		return udf02;
	}
	public void setUdf02(String udf02) {
		this.udf02 = udf02;
	}
	public String getUdf03() {
		return udf03;
	}
	public void setUdf03(String udf03) {
		this.udf03 = udf03;
	}
	public String getUdf04() {
		return udf04;
	}
	public void setUdf04(String udf04) {
		this.udf04 = udf04;
	}
	public String getUdf05() {
		return udf05;
	}
	public void setUdf05(String udf05) {
		this.udf05 = udf05;
	}
	public String getKey1() { return key1; }

	public String getKey2() { return key2; }

	public String getKey3() { return key3; }


	public String getOrder1() { return order1; }

	public void setKey1(String key1) { this.key1 = key1; }

	public void setKey2(String key2) { this.key2 = key2; }

	public void setKey3(String key3) { this.key3 = key3; }

	public void setOrder1(String order1) { this.order1 = order1; }

	public String getAttachView() { return attachView; }

	public void setAttachView(String attachView) { this.attachView = attachView; }

	public String getAttachDir() { return AttachDir; }

	public void setAttachDir(String attachDir) { AttachDir = attachDir; }

	@Override
	
	public String toString() {
		return "MessageData [title=" + title + ", message=" + message + ", reveiver1=" + reveiver1 + ", reveiver2="
				+ reveiver2 + ", reveiver3=" + reveiver3 + ", udf01=" + udf01 + ", udf02=" + udf02 + ", udf03=" + udf03
				+ ", udf04=" + udf04 + ", udf05=" + udf05 + "]";
	}
	private String title; //title为空，这代表空内容
	private String message;
	private String reveiver1; //主送
	private String reveiver2; //抄送
	private String reveiver3; //密送
	private String udf01;
	private String udf02;
	private String udf03;
	private String udf04;
	private String udf05;
	@Expose
	private String key1;
	@Expose
	private String key2;
	@Expose
	private String key3;
	private String k_1;
	private String K_2;
	private String K_3;
	private String order1;
	private String attachView;

	public String getK_1() {
		return k_1;
	}

	public String getK_2() {
		return K_2;
	}

	public String getK_3() {
		return K_3;
	}

	public void setK_1(String k_1) {
		this.k_1 = k_1;
	}

	public void setK_2(String k_2) {
		K_2 = k_2;
	}

	public void setK_3(String k_3) {
		K_3 = k_3;
	}

	private String AttachDir;
}
