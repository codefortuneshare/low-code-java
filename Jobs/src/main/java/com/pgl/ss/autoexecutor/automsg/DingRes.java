package com.pgl.ss.autoexecutor.automsg;

/**
 * 钉钉返回消息体，errcode=0 表示推送成功
 * add by lzw 2021-07-26
 */
public class DingRes {
    private Integer errcode;
    private String errmsg;

    public DingRes() {
    }

    public DingRes(Integer errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
}
