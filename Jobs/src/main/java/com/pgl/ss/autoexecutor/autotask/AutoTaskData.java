package com.pgl.ss.autoexecutor.autotask;

import java.io.Serializable;
//import java.util.List;

public class AutoTaskData implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String getAutotaskkey() {
		return autotaskkey;
	}
	public void setAutotaskkey(String autotaskkey) {
		this.autotaskkey = autotaskkey;
	}
	public String getKey1() {
		return key1;
	}
	public void setKey1(String key1) {
		this.key1 = key1;
	}
	public String getKey2() {
		return key2;
	}
	public void setKey2(String key2) {
		this.key2 = key2;
	}
	public String getKey3() {
		return key3;
	}
	public void setKey3(String key3) {
		this.key3 = key3;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeptask() {
		return deptask;
	}
	public void setDeptask(String dependenttask) {
		this.deptask = dependenttask;
	}
	public String getFailurecount() {
		return failurecount;
	}
	public void setFailurecount(String failurecount) {
		this.failurecount = failurecount;
	}
	public String getExpiredate() {
		return expiredate;
	}
	public void setExpiredate(String expiredate) {
		this.expiredate = expiredate;
	}
	public String getTaskcategory() {
		return taskcategory;
	}
	public void setTaskcategory(String taskcategory) {
		this.taskcategory = taskcategory;
	}
	public String getTaskflag1() {
		return taskflag1;
	}
	public void setTaskflag1(String taskflag1) {
		this.taskflag1 = taskflag1;
	}
	public String getTaskflag2() {
		return taskflag2;
	}
	public void setTaskflag2(String taskflag2) {
		this.taskflag2 = taskflag2;
	}
	public String getTaskflag3() {
		return taskflag3;
	}
	public void setTaskflag3(String taskflag3) {
		this.taskflag3 = taskflag3;
	}
	public String getTaskflag4() {
		return taskflag4;
	}
	public void setTaskflag4(String taskflag4) {
		this.taskflag4 = taskflag4;
	}
	public String getTaskflag5() {
		return taskflag5;
	}
	public void setTaskflag5(String taskflag5) {
		this.taskflag5 = taskflag5;
	}
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
	public String getAddwho() {
		return addwho;
	}
	public void setAddwho(String addwho) {
		this.addwho = addwho;
	}
	public String getEditdate() {
		return editdate;
	}
	public void setEditdate(String editdate) {
		this.editdate = editdate;
	}
	public String getEditwho() {
		return editwho;
	}
	public void setEditwho(String editwho) {
		this.editwho = editwho;
	}
	public String getExecutor() {
		return executor;
	}
	public void setExecutor(String executor) {
		this.executor = executor;
	}
	public String getBatchgroup() {
		return batchgroup;
	}
	public void setBatchgroup(String batchgroup) {
		this.batchgroup = batchgroup;
	}

	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}



	@Override
	public String toString() {
		return "AutoTaskData [notes=" + notes + ", autotaskkey=" + autotaskkey + ", key1=" + key1 + ", key2=" + key2
				+ ", key3=" + key3 + ", status=" + status + ", deptask=" + deptask
				+ ", failurecount=" + failurecount + ", expiredate=" + expiredate + ", taskcategory=" + taskcategory
				+ ", taskflag1=" + taskflag1 + ", taskflag2=" + taskflag2 + ", taskflag3=" + taskflag3 + ", taskflag4="
				+ taskflag4 + ", taskflag5=" + taskflag5 + ", adddate=" + adddate + ", addwho=" + addwho + ", editdate="
				+ editdate + ", editwho=" + editwho + ", executor=" + executor + ", batchgroup=" + batchgroup + "]";
	}
	private String autotaskkey;
	private String key1; 
	private String key2; 
	private String key3;
	private String status;
	private String deptask;
	private String failurecount;
	private String expiredate;
	private String taskcategory;
	private String taskflag1;
	private String taskflag2;
	private String taskflag3;
	private String taskflag4;
	private String taskflag5;
	private String adddate;
	private String addwho;
	private String editdate;
	private String editwho;
	private String executor;
	private String batchgroup;
	private String notes;
//	private static final int Code_Sucess = 9;
//    private static final int Code_Fail = 7;
//	private static final int Code_None =0;
//	private static final int Code_InPric =5;
}