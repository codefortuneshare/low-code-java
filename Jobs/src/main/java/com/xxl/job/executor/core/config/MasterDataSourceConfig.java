package com.xxl.job.executor.core.config;

import com.alibaba.druid.pool.DruidDataSource;

import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "com.pgl.ss.autoexecutor", sqlSessionTemplateRef = "masterSqlSessionTemplate")
public class MasterDataSourceConfig {
    public static final String[] Default_Setting_Paths = new String[] { "config\\dbconnect.setting",
        	"\\dbconnect.setting",
        	"dbconnect.setting" };
  private String password;

    @Bean(name = "masterDataSource")
    @Primary
    public DataSource dataSource() {
    	Props exectorProps = getDefaultSetting(null);
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(exectorProps.getStr("DBDriver").trim());
        dataSource.setUrl(exectorProps.getStr("DBURL").trim());
        dataSource.setUsername(exectorProps.getStr("UserName").trim());
        dataSource.setPassword(exectorProps.getStr("Password").trim());
        return dataSource;
    }

    @Bean(name = "masterSqlSessionFactory")
    @Primary
    public SqlSessionFactory sqlSessionFactory(@Qualifier("masterDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setCallSettersOnNulls(true);
        bean.setDataSource(dataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
        bean.setConfiguration(configuration);
        return bean.getObject();
    }

    @Bean(name = "masterTransactionManager")
    @Primary
    public DataSourceTransactionManager transactionManager(@Qualifier("masterDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "masterSqlSessionTemplate")
    @Primary
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("masterSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
	private static Props getDefaultSetting(String spath[]) {
		String tmpStr=System.getProperty("user.dir");
		String fileSeparator = System.getProperty("file.separator");  
		if (spath==null||spath.length==0||spath[0].equals("")) {
			for (String SettingPath : Default_Setting_Paths) {
				try {
					return new Props(SettingPath);
				} catch (IORuntimeException ignore) {
					// ignore
					try {

						return new Props(tmpStr+fileSeparator+SettingPath);
					} catch (IORuntimeException ignore1) {
						// ignore
						
					}
				}
			}
		}else {
			try {
				return new Props(spath[0]);
			} catch (IORuntimeException ignore) {
				// ignore
			}
		}
		return null;
	}
}