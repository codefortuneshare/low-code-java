package com.xxl.job.executor.service.jobhandler;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.extra.mail.GlobalMailAccount;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import cn.hutool.setting.dialect.Props;
import com.pgl.ss.autoexecutor.automsg.AutoMsgDAO;
import com.pgl.ss.autoexecutor.automsg.MessageData;
import com.pgl.ss.utils.ExcelFileUtil;
import com.pgl.ss.utils.RuleUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobFileAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeUtility;
import java.io.File;
import java.text.Normalizer;
import java.util.*;



@Service
public class MailSenderHandler2 /*extends IJobHandler*/ {
    private static Logger logger = LoggerFactory.getLogger(MailSenderHandler2.class);
    public static final String[] Default_Setting_Paths = new String[] { "config/MailSenderHandler2.setting",
            "MailSenderHandler2.setting" };
    //  @Value("${Proc.BuildTaskViewName}")
//  private String buildTaskViewName;
    @Autowired
    AutoMsgDAO msgBuilderDAO;


    TimeInterval timer = DateUtil.timer();
    private String[] columnArray = {"KEY1", "KEY2", "KEY3",
            "O_1", "O_2", "O_3", "O_4", "O_5", "O_6", "O_7", "O_8", "O_9"};

    /**
     * 解析Order字段, 生成排序语句
     * @param orderColumn
     * @return
     */
    public static String resolveOrderColumn(String orderColumn) {
        if (orderColumn == null) {
            return null;
        }
        String[] array = strToListByNum(orderColumn, 2);
        String result = "ORDER BY";
        for (int i = 0; i < array.length; i++) {
            array[i].charAt(0);
            result+=" O_"+array[i].charAt(0);
            if (array[i].charAt(1) == '1') {
                result+=" DESC,";
            } else if (array[i].charAt(1) == '0') {
                result+=" ASC,";
            }
        }
        result = result.substring(0,result.length() - 1);
        return result;
    }

    /**
     * 每隔多少字符为一个元素放入数组中
     */
    public static String[] strToListByNum(String str, int num) {
        int m=str.length()/2;
        if(m*2<str.length()){
            m++;
        }
        String[] strs=new String[m];
        int j=0;
        for(int i=0;i<str.length();i++){
            if(i%2==0){//每隔两个
                strs[j]=""+str.charAt(i);
            }else{
                strs[j]=strs[j]+""+str.charAt(i);
                j++;
            }
        }
        System.out.println(Arrays.toString(strs));
        return strs;
    }

    private static Props getDefaultSetting(String spath[]) {
        String tmpStr=System.getProperty("user.dir");
        String fileSeparator = System.getProperty("file.separator");
        if (spath==null||spath.length==0||spath[0].equals("")) {
            for (String SettingPath : Default_Setting_Paths) {
                try {
                    return new Props(tmpStr+fileSeparator+SettingPath);
                } catch (IORuntimeException ignore) {
                    // ignore
                    try {

                        return new Props(tmpStr+fileSeparator+SettingPath);
                    } catch (IORuntimeException ignore1) {
                        // ignore

                    }
                }
            }
        }else {
            try {
                return new Props(tmpStr+fileSeparator+spath[0]);
            } catch (IORuntimeException ignore) {
                // ignore
            }
        }
        return null;
    }
    /**
     * 根据邮件配置记录发送邮件给指定人
     */
    private void sendEmail(MessageData msgData) throws Exception {
        String tmpStr = System.getProperty("user.dir");
        String fileSeparator = System.getProperty("file.separator");
        MailAccount account = new MailAccount(tmpStr+fileSeparator+Default_Setting_Paths[0]);
        List<String> to = null;
        List<String> cc = null;
        List<String> bcc = null;
        if (msgData.getReveiver1()!=null) {
            String[] toArray = msgData.getReveiver1().split(",");
            to = new ArrayList<>(toArray.length);
            Collections.addAll(to, toArray);
        }
        if (msgData.getReveiver2()!=null) {
            String[] ccArray = msgData.getReveiver2().split(",");
            cc = new ArrayList<>(ccArray.length);
            Collections.addAll(cc, ccArray);
        }
        if (msgData.getReveiver3()!=null) {
            String[] bccArray = msgData.getReveiver3().split(",");
            bcc = new ArrayList<>(bccArray.length);
            Collections.addAll(bcc, bccArray);
        }
        if (msgData.getAttachView() == null || msgData.getAttachDir() == null) {
            MailUtil.send(account, to, cc, bcc, msgData.getTitle(), msgData.getMessage(), true);
            return;
        }
        //附件条件查询Key1
        String k_1 = msgData.getK_1();
        //附件条件查询Key2
        String k_2 = msgData.getK_1();
        //附件条件查询Key3
        String k_3 = msgData.getK_3();
        //邮件列表查询维度Key1
        String key1 = msgData.getKey1();
        //邮件列表查询维度Key2
        String key2 = msgData.getKey2();
        //邮件列表查询维度Key3
        String key3 = msgData.getKey3();
        String[] sheetNameArray = msgData.getSheetName().split(";");
        String[] excelArray = msgData.getAttachView().split(";");
        String dirPathsStr = Optional.ofNullable(msgData.getAttachDir())
                .orElseThrow(()-> new RuntimeException("邮件警告视图未配置ATTACHDir字段"));
        String[] dirArray = dirPathsStr.split(",");
        List<List<?>> sheetDataArray = new ArrayList<>();
        List<File> fileList = new ArrayList<>();
        System.getProperties().setProperty("mail.mime.splitlongparameters", "false");
        //循环创建EXCEL
        for (int i = 0; i < excelArray.length; i++) {
            String[] sheetView = excelArray[i].split(",");
            String[] sheetName = sheetNameArray[i].split(",");
            //循环创建Sheet
            for (int z = 0; z < sheetView.length; z++) {
                List<Map> sheetData  = msgBuilderDAO.getDataByView(sheetView[z],key1, key2, key3,k_1, k_2, k_3,"");
                //删除KEY1、KEY2、KEY3、O_1..... O_9字段
                RuleUtil.removeExtactColumn(sheetData);
                sheetDataArray.add(sheetData);
            }
            //若存在附件,则删除再新建；若不存在, 则新建
            ExcelFileUtil.map2ExcelFile(sheetDataArray, sheetName, dirArray[i]);
            File file = new File(dirArray[i]);
            fileList.add(file);
            sheetDataArray.clear();
        }
        //发送邮件
        int size = fileList.size();
        File[] fileArray  = (File[])fileList.toArray(new File[size]);

        MailUtil.send(account, to, cc, bcc, msgData.getTitle(), msgData.getMessage(), true, fileArray);
    }

    @XxlJob("mail2")
    public void execute() throws Exception {
//    if(buildTaskViewName.equals("")) {
//    	logger.error( "无配置内容可执行");
//    	return new ReturnT<String>(ReturnT.FAIL_CODE, "无配置内容可执行");
//    }
        Props props = getDefaultSetting(null);
        if (props == null || props.getStr("MessageView") == null || props.getStr("MessageView").equals("")) {
            XxlJobHelper.handleFail("无配置内容可执行");
            return;
        }
        List<MessageData> listMessageData = msgBuilderDAO.getMessageData(props.getStr("MessageView"));
        if (listMessageData == null || listMessageData.size() < 1) {
            logger.info("无邮件任务");
            XxlJobHelper.handleFail("无邮件任务");
            return;
        }
        logger.debug("开始发送，当前数据数量： {}", listMessageData.size());
        for (MessageData msgData : listMessageData) {
            if (msgData == null || msgData.getTitle() == null) {
                XxlJobHelper.handleFail(msgData.getTitle()+"无内容可发送");
            } else {
                sendEmail(msgData);
            }
        }
        XxlJobHelper.log(new StringBuilder().append("程序执行时长(s)：").append(timer.intervalSecond()).toString());
        XxlJobHelper.handleSuccess("Call MailSenderHandler2 Success");
    }
}
