package com.xxl.job.executor.core.config;

import com.xxl.job.core.executor.XxlJobExecutor;

import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;

import java.io.File;
import java.io.IOException;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * xxl-job config
 *
 * @author xuxueli 2017-04-28
 */
@Configuration
@ComponentScan(basePackages = "com.xxl.job.executor.service.jobhandler")
public class XxlJobConfig {
    private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);
//    public Props exectorProps;
    public static final String[] Default_Setting_Paths = new String[] { "config\\xxljob.setting",
    	"\\xxljob.setting",
    	"xxljob.setting" };

////    @Value("${xxl.job.admin.addresses}")
//    private String addresses;
//
////    @Value("${xxl.job.executor.appname}")
//    private String appname;
//
////    @Value("${xxl.job.executor.ip}")
//    private String ip;
//
////    @Value("${xxl.job.executor.port}")
//    private int port;
//
////    @Value("${xxl.job.executor.logpath}")
//    private String logpath;
//
////    @Value("${xxl.job.accessToken}")
//    private String accessToken;

    @Bean(initMethod = "start", destroyMethod = "destroy")

//    
    public XxlJobExecutor xxlJobExecutor() {
        logger.info(">>>>>>>>>>> xxl-job config init.");
        Props exectorProps = getDefaultSetting(null);
		XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
		xxlJobSpringExecutor.setAccessToken(exectorProps.getStr("JobAccessToken").trim());
		xxlJobSpringExecutor.setAdminAddresses(exectorProps.getStr("JobAdminAddress").trim());
		xxlJobSpringExecutor.setAppname(exectorProps.getStr("JobExectorAppName").trim());
		xxlJobSpringExecutor.setAddress(exectorProps.getStr("JobAddress").trim());
		xxlJobSpringExecutor.setIp(exectorProps.getStr("JobExectorIP").trim());
		xxlJobSpringExecutor.setLogPath(exectorProps.getStr("JobExectorLogPath").trim());
		xxlJobSpringExecutor.setPort(exectorProps.getInt("JobExectorPort"));
		xxlJobSpringExecutor.setLogRetentionDays(exectorProps.getInt("LogRetentionDays"));
		return xxlJobSpringExecutor;
    }

	private static Props getDefaultSetting(String spath[]) {
		String tmpStr=System.getProperty("user.dir");
		String fileSeparator = System.getProperty("file.separator");  
		if (spath==null||spath.length==0||spath[0].equals("")) {
			for (String SettingPath : Default_Setting_Paths) {
				try {
					return new Props(SettingPath);
				} catch (IORuntimeException ignore) {
					// ignore
					try {

						return new Props(tmpStr+fileSeparator+SettingPath);
					} catch (IORuntimeException ignore1) {
						// ignore
						
					}
				}
			}
		}else {
			try {
				return new Props(spath[0]);
			} catch (IORuntimeException ignore) {
				// ignore
			}
		}
		return null;
	}
}