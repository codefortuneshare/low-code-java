package com.xxl.job.executor.service.jobhandler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;
import com.pgl.ss.autoexecutor.automsg.AutoMsgDAO;
import com.pgl.ss.autoexecutor.automsg.MessageData;
import com.pgl.ss.autoexecutor.autotask.AutoTaskDAO;
import com.pgl.ss.autoexecutor.autotask.AutoTaskData;
import com.pgl.ss.autoexecutor.autotask.ResultData;
import com.pgl.ss.utils.DingTalkUtil;
import com.pgl.ss.utils.exceed.ExceedUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobFileAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;

//import com.pgl.AutoDorid.print.domain.PrnData;
//import com.pgl.ss.autoexecutor.utils.forteconnect.ForteMassage;
//import com.pgl.ss.autoexecutor.utils.forteconnect.ForteReturn;
//import com.pgl.ss.autoexecutor.utils.forteconnect.ForteSocket;
//import com.pgl.ss.autoexecutor.utils.forteconnect.thread.ForteSocketThreadCallBack;
//import com.xxl.job.executor.core.config.ForteSocketConfig;
//import java.util.HashMap;
//import java.util.HashMap;
//import java.util.concurrent.ExecutionException;
//import java.util.List;
//import java.util.concurrent.Future;
//import java.util.concurrent.RejectedExecutionException;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.TimeoutException;
//import org.springframework.beans.factory.annotation.Value;


@Service
public class ExceedHandler3 {
    private static Logger logger = LoggerFactory.getLogger(ExceedHandler3.class);
    public static final String[] Default_Setting_Paths = new String[]{"config/ExceedHandler3.setting",
            "ExceedHandler3.setting"};
    private String taskProcedure;
    //  @Value("${Proc.TaskCategory}")
    private String taskCategory;
    //  @Value("${Proc.TheProcName}")
    private String theProcName;
    //  @Value("${Proc.CheckBeforeViewName}")
    private String checkBeforeViewName;
    //  @Value("${Proc.CheckAfterViewName}")
    private String checkAfterViewName;
    //  @Value("${Proc.BuildTaskViewName}")
    private String buildTaskViewName;
    //  @Value("${Proc.ExecutorCode}")
    private String executorCode;
    //  @Value("${Proc.ServerName}")
    private String serverName;
    private String sendDingMsg;
    private String getMsgDataViewName = "";
    private MessageData msgData;
    private String writeMsgProcedure;
    private String token;
    private String secret;
//  @Value("${Proc.DingTalk.Secret}")
//  private String dtSecret;
//  @Value("${Proc.DingTalk.Token}")
//  private String dtToken;

    @Autowired
    AutoTaskDAO autoTaskDAO;

    @Autowired
    AutoMsgDAO autoMsgDAO;

    //  @Autowired
    ExecutorService executorService;

    /**
     * 删除元素
     *
     * @param
     * @return
     */
    public String[] deleteArrayNa(String[] string) {
        String[] array = string;
        List<String> list = new ArrayList<>(array.length);
        for (int j = 0; j < string.length; j++) {
           //#NA代表该KEY1不传
            if (array[j].contains("#NA")) {
                continue;
            }
            if (j == 2) {//若为第三个KEY, 则逗号拆分
                String[] params = array[j].split(",");
                for (int i = 0; i < params.length; i++) {
                    if ("null".equalsIgnoreCase(params[i])) {
                        list.add("");
                    } else {
                        list.add(params[i]);
                    }
                }
            } else {//若为第三个KEY, 则逗号拆分
                if (array[j].contains("null")) {
                    list.add("");
                } else {
                    list.add(array[j]);
                }
            }
        }
        String[] strArr = list.toArray(new String[]{});
        return strArr;
    }

    //  @Autowired
//  ForteSocketConfig forteSocketConfig;
    private static Props getDefaultSetting(String spath[]) {
        String tmpStr = System.getProperty("user.dir");
        String fileSeparator = System.getProperty("file.separator");
        if (spath == null || spath.length == 0 || spath[0].equals("")) {
            for (String SettingPath : Default_Setting_Paths) {
                try {
                    return new Props(SettingPath);
                } catch (IORuntimeException ignore) {
                    // ignore
                    try {
                        return new Props(tmpStr + fileSeparator + SettingPath);
                    } catch (IORuntimeException ignore1) {
                        // ignore
                    }
                }
            }
        } else {
            try {
                return new Props(spath[0]);
            } catch (IORuntimeException ignore) {
                // ignore
            }
        }
        return null;
    }

    TimeInterval timer = DateUtil.timer();

    @XxlJob("exceed3")
    public void execute() throws Exception {
        Map<String, String> result;
        Long lRecordCount = (long) 0;
        ResultData ck;
        String sResurt;
        Props props = getDefaultSetting(null);
        if (props == null) {
            XxlJobHelper.handleFail("无配置内容可执行");
            return;
        }
        Map<String, Object> paramMap = new HashMap<>();
        token = props.getStr("Token").trim();
        secret = props.getStr("Secret").trim();
        writeMsgProcedure = props.getStr("WriteMsgProcedure").trim();
        //存储过程名称
        taskProcedure = props.getStr("TaskProcedure").trim();
//	 #自动任务分类标识
        taskCategory = props.getStr("TaskCategory").trim();
//	 #自动任务执行者标记（用于区分执行进程）
        executorCode = props.getStr("ExecutorCode").trim();
//	 #自动任务服务器名称（用于标记服务器）
        serverName = props.getStr("ServerName").trim();
//	 #自动执行任务创建视图，若如则不执行
        buildTaskViewName = props.getStr("BuildTaskViewName").trim();
//	 #执行前检查视图，若如则不执行
        checkBeforeViewName = props.getStr("CheckBeforeViewName").trim();
//	 #执行数据生成视图
//	 setGetDataViewName(props.getStr("GetDataViewName"));
//	 #执行后检查视图，若如则不执行
        checkAfterViewName = props.getStr("CheckAfterViewName").trim();
//	 #是否发送Ding信息'N'：不发送,'Y'：发送
        sendDingMsg = props.getStr("SendDingMsg").trim();
        theProcName = props.getStr("ProcName").trim();
        getMsgDataViewName = props.getStr("GetMsgDataViewName").trim();
        if (sendDingMsg.equals("Y")) {
            DingTalkUtil.LoadSetting(props);
        }
//	插入待处理任务数据
        if (buildTaskViewName.equals("") == false) {
            lRecordCount = autoTaskDAO.insertDataByView(serverName, " ", buildTaskViewName);
            logger.debug("插入数据行数:{}", lRecordCount);
        }
//  更新任务为本服务待处理内容
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");//设置日期格式
        String batchCode = df.format(new Date());
        lRecordCount = autoTaskDAO.updateStatusByCategory(5, serverName, executorCode, " ", batchCode, taskCategory, 0);
        logger.debug("更新数据行数:{}", lRecordCount);
//	读取任务数据集
        List<AutoTaskData> listTD = autoTaskDAO.getData(taskCategory, 5, executorCode);
        if (listTD == null || listTD.size() == 0) {
            logger.info("调用成功，无任务可执行");
            XxlJobHelper.handleFail("无任务可执行");
            return;
        }
        String tmpParams[] = null;
        String filterParams[] = null;
        String notes = "nocontent";
        for (AutoTaskData autoTaskData : listTD) {
            sResurt = theProcName + "{" + autoTaskData.getKey1() + "，" + autoTaskData.getKey2() + "，"
                    + autoTaskData.getKey3() + "}";
            tmpParams = new String[]{autoTaskData.getKey1(), autoTaskData.getKey2(), autoTaskData.getKey3()};
            //过滤FORTE参数
            filterParams = deleteArrayNa(tmpParams);
            if (checkBeforeViewName.equals("") == false) {
                ck = this.autoTaskDAO.getCkeckResult(checkBeforeViewName, autoTaskData.getAutotaskkey());
                if ((ck == null) || (ck.getCode().intValue() != 1)) {
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, "不符合执行条件",
                            autoTaskData.getAutotaskkey());
                    continue;
                }
            }
            if (theProcName.equals("") == false) {
                logger.debug("执行调用：" + Arrays.toString(filterParams));
                ExceedUtil.LoadSetting(props);
                result = ExceedUtil.runFunc(theProcName, filterParams);
                logger.debug("执用完成：" + result.toString());
                if (result.get("code").equals("7")) {
                    notes = "执行错误:" + result.get("msg");
                    notes = notes.length()<=200?notes:notes.substring(0, 200);
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, notes,
                            autoTaskData.getAutotaskkey());
                    continue;
                }
            }
            lRecordCount = autoTaskDAO.updateStatusByKey(9, serverName, executorCode, "成功",
                    autoTaskData.getAutotaskkey());
            if (checkAfterViewName.equals("") == false) {
                ck = this.autoTaskDAO.getCkeckResult(checkAfterViewName, autoTaskData.getAutotaskkey());
                if ((ck == null) || (ck.getCode().intValue() != 1)) {
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, "执后检查失败",
                            autoTaskData.getAutotaskkey());
                }
            }
            if (!taskProcedure.equals("")) {
                paramMap.put("produceName", taskProcedure);
                paramMap.put("key1", filterParams[0]);
                paramMap.put("key2", filterParams[1]);
                paramMap.put("key3", filterParams[2]);
                try {
                    autoTaskDAO.executeProduce3(paramMap);
                } catch (Exception e) {
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, "ProcedureFailed",
                            autoTaskData.getAutotaskkey());
                    e.printStackTrace();
                }
            }
        }
        if ("N".equalsIgnoreCase(sendDingMsg)) {
            paramMap.put("produceName", writeMsgProcedure);
            paramMap.put("key1", taskCategory);
            paramMap.put("key2", batchCode);
            paramMap.put("key3", token + "," + secret);
            paramMap.put("key4", getMsgDataViewName);
            try {
                autoTaskDAO.executeProduce2(paramMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            msgData=autoMsgDAO.getOneMessageDataWithKey(getMsgDataViewName, taskCategory, batchCode, " ");
            if (msgData!=null) {
                DingTalkUtil.sendTextByRobot(msgData.getTitle() + ":" + msgData.getMessage());
            }
        }
        XxlJobHelper.log(new StringBuilder().append("程序执行时长(s)：").append(timer.intervalSecond()).toString());
        XxlJobHelper.handleSuccess("Call ExceedHandler3 Success");
    }







}
