package com.xxl.job.executor;

import com.xxl.job.executor.service.jobhandler.*;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@SpringBootApplication
// 扫描MyBatis的SQL动态代理接口的配置文件
@MapperScan("com.xxl.job.executor.pgl.dao")
//@EnableScheduling
public class Application {


    //@Autowired
    //private ExceedHandler3 exceed3;
    //@Scheduled(cron="*/180 * * * * ?")
    //public void test1() throws Exception {
       //exceed3.execute();
    //}


	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.println("执行器xxl-job-executor-mk 启动成功");
	}

}