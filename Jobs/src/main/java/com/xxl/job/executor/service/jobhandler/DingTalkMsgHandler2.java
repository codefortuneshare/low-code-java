package com.xxl.job.executor.service.jobhandler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;
import com.alibaba.fastjson.JSON;
import com.pgl.ss.autoexecutor.automsg.AutoMsgDAO;
import com.pgl.ss.autoexecutor.automsg.MessageData;
import com.pgl.ss.autoexecutor.autotask.AutoTaskDAO;
import com.pgl.ss.utils.DingTalkUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.pgl.ss.autoexecutor.automsg.DingRes;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  通过T_autotask 管理 T_gen_MSG表数据，
 *  实现钉钉信息重发功能，通过配置确定许可重试次数  V_AUTOTASK_DingMsg
 *  add by lzw 2021-07-26
 */
@Service
public class DingTalkMsgHandler2  {
	private static Logger logger = LoggerFactory.getLogger(DingTalkMsgHandler2.class);

	public static final String[] Default_Setting_Paths = new String[] { "config/DingTalkMsgHandler2.setting",
			"DingTalkMsgHandler2.setting" };
//  @Value("${Proc.BuildTaskViewName}")
//  private String buildTaskViewName;

    @Autowired
    AutoTaskDAO autoTaskDAO;
	@Autowired
	AutoMsgDAO autoMsgDAO;

    private String executorCode;
    private String serverName;

    private static Props getDefaultSetting(String spath[]) {
        String tmpStr = System.getProperty("user.dir");
        String fileSeparator = System.getProperty("file.separator");
        if (spath == null || spath.length == 0 || spath[0].equals("")) {
            for (String SettingPath : Default_Setting_Paths) {
                try {
                    return new Props(SettingPath);
                } catch (IORuntimeException ignore) {
                    // ignore
                    try {
                        return new Props(tmpStr + fileSeparator + SettingPath);
                    } catch (IORuntimeException ignore1) {
                        // ignore

                    }
                }
            }
        } else {
            try {
                return new Props(spath[0]);
            } catch (IORuntimeException ignore) {
                // ignore
            }
        }
        return null;
    }

	TimeInterval timer = DateUtil.timer();

    @XxlJob("dingMsg2")
	public void execute() throws Exception {
	    //配置
		Props props = getDefaultSetting(null);
		String messageView = props.getStr("MessageView");
		if (props == null || messageView == null) {
			logger.error("无配置内容可执行");
            XxlJobHelper.handleFail("无配置内容可执行");
            return;
		}
        executorCode = props.getStr("ServerName").trim();
        serverName = props.getStr("ExecutorCode").trim();
		//获取数据
		List<MessageData> listPO = autoMsgDAO.getMessageData(messageView);
		if (listPO == null || listPO.size() < 1) {
            logger.error("无数据可发送");
            XxlJobHelper.handleFail("无数据可发送");
            return;
		}
		//循环发送
		logger.debug("开始发送，当前信息数量：{}", listPO.size());
		int status;
		String notes;
		for (MessageData msgData : listPO) {
		    //更新状态为处理中
            status = 5;
            notes = "开始发送";
            autoTaskDAO.updateStatusByKey(status,serverName,executorCode,notes,msgData.getOrder1());//order1 = autotaskkey
            //发送
            DingRes res = senMsg(msgData);
            //更新发送结果
            notes = res.getErrmsg();
            status = res.getErrcode() == 0 ? 9:7;
            autoTaskDAO.updateFailureCountByKey(status,serverName,notes,msgData.getOrder1());//order1 = autotaskkey
		}
       XxlJobHelper.log(new StringBuilder().append("程序执行时长(s)：").append(timer.intervalSecond()).toString());
       XxlJobHelper.handleSuccess("Call DingTalkMsgHandler2 Success");
	}
    /**
     *  钉钉信息发送
     */
    private static DingRes senMsg(MessageData msgData) {
        DingRes res = null;
        String content = msgData.getTitle() + ":" + msgData.getMessage();
        if (msgData == null || msgData.getTitle() == null || msgData.getTitle().trim().equals("")) {
            res = new DingRes(-1,"内容或标题为空");
        }
        else {
            try {
                //udf01 = token,  udf02 = secret
                String msg  = DingTalkUtil.sendTextByRobot(msgData.getUdf01(),msgData.getUdf01(),content);
                res = JSON.parseObject(msg, DingRes.class);
            } catch (Exception e) {
                res = new DingRes(-1,e.getMessage()+"");
            }
        }
        return res;
    }
}
