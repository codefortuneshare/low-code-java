package com.xxl.job.executor.service.jobhandler;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.setting.dialect.Props;
import com.pgl.ss.autoexecutor.automsg.AutoMsgDAO;
import com.pgl.ss.autoexecutor.automsg.MessageData;
import com.pgl.ss.autoexecutor.autotask.AutoTaskDAO;
import com.pgl.ss.autoexecutor.autotask.AutoTaskData;
import com.pgl.ss.autoexecutor.autotask.ResultData;
import com.pgl.ss.utils.*;
import com.pgl.ss.utils.exceed.ExceedUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobFileAppender;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.SaxonApiUncheckedException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;




@Service
public class DBOutHandler  {
    private static Logger logger = LoggerFactory.getLogger(DBOutHandler.class);
    public static final String[] Default_Setting_Paths = new String[]{"config/DBOutHandler.setting",
            "DBOutHandler.setting"};

    private String taskProcedure;
    private String taskCategory;
    private String theProcName;
    private String checkBeforeViewName;
    private String checkAfterViewName;
    private String buildTaskViewName;
    private String executorCode;
    private String serverName;
    private String sendDingMsg;
    private String getMsgDataViewName = "";
    private MessageData msgData;
    private String writeMsgProcedure;
    private String token;
    private String secret;
    private String statusColumn;
    private String idColumn;
    private String tableRule;
    private String table;
    private String fileSuffix;
    private String batchCode;
    private Map<String, String> paramMap = new HashMap<>();
    private Map<String, Object> objMap = new HashMap<>();
    private Props props;
    private String genDataView;
    @Autowired
    AutoTaskDAO autoTaskDAO;
    @Autowired
    AutoMsgDAO autoMsgDAO;

    private String[] columnArray = {"KEY1", "KEY2", "KEY3",
            "O_1", "O_2", "O_3", "O_4", "O_5", "O_6", "O_7", "O_8", "O_9"};
    /**
     * @Param: ['0000324364','ASNCompleted']]
     * @Action 生成数据文件
     */
    private void genData(Map<String, String> paramMap) throws Exception {
        //数据任务
        Map<String, String> map = autoTaskDAO.getTaskByKey(genDataView, paramMap.get("KEY1"), paramMap.get("KEY2"), paramMap.get("KEY3"), paramMap.get("KEY4"), paramMap.get("KEY5"));
        if (map == null) {
            logger.info(paramMap.get("KEY1")+"无数据任务");
            XxlJobHelper.handleFail(paramMap.get("KEY1")+"无数据任务");
            return;
        }
        String dataView = map.get("DATAVIEW");
        String rootTag = map.get("ROOTTAG");
        String childTag = map.get("CHILDTAG");
        String oridirs = map.get("ORIGDIR");
        String xlsdirs = map.get("XSLDIR");
        String outdirs = map.get("OUTDIR");
        String encodeArray = map.get("ENCODING");
        String isXls3Grammer = map.get("ISXLS3GRAMMER");
        String key1 = map.get("KEY1");
        String key2 = map.get("KEY2");
        String key3 = map.get("KEY3");
        String key4 = map.get("KEY4");
        String key5 = map.get("KEY5");
        String order = map.get("ORDER");
        Integer status = 9;
        String notes = "成功";
        List<Map> dataList = autoTaskDAO.getRuleData(dataView, key1, key2, key3, key4, key5, order);
        if (dataList.size() == 0) {
            logger.info(dataView+"["+key1+"]调用成功，无数据可生成");
            XxlJobHelper.handleFail(dataView+"["+key1+"] 无数据可生成");
            return;
        }
        RuleUtil.removeExtactColumn(dataList);
        logger.debug(dataView+"["+key1+"]开始生成数据");
        String path = "";
        try {
            path = FileUitl.genFile(dataList, oridirs, encodeArray, outdirs, rootTag, childTag, xlsdirs, Boolean.getBoolean(isXls3Grammer));
        } catch (SaxonApiException | SaxonApiUncheckedException e) {
                notes = "XSL转换"+dataView+"["+oridirs+"]异常";
                status = 7;
                if (StringUtils.isNotEmpty(writeMsgProcedure) && "N".equalsIgnoreCase(sendDingMsg)) {
                    objMap.put("produceName", writeMsgProcedure);
                    objMap.put("key1", "XSL转换["+oridirs+"]异常:");
                    objMap.put("key2", e.getMessage());
                    objMap.put("key3", token+","+secret);
                    objMap.put("key4", taskCategory+","+batchCode);
                    autoTaskDAO.executeProduce2(objMap);
                } else {
                    DingTalkUtil.sendTextByRobotAsync("XSL转换"+"["+oridirs+"]异常:\n"+ e.getMessage() +"\n");
                }
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        File file = new File(path);
        if (!file.exists()) {
            status = 7;
            notes = "未生成数据文件";
        }
        autoTaskDAO.updateStatusByKey2(table, tableRule,
                statusColumn, status,
                taskCategory, executorCode, notes, paramMap.get(idColumn));
        logger.info("视图"+dataView+"生成数据路径:"+path+",日志XML路径:"+oridirs);
    }


    TimeInterval timer = DateUtil.timer();

    @XxlJob("DBOutHandler")
    public void DBOutHandler() throws Exception {
        Map<String, String> result;
        Map<String, Object> paramMap = new HashMap<>();
        Long lRecordCount = (long) 0;
        ResultData ck;
        String sResurt;
        Props props = PropsUtil.getDefaultSetting(null, Default_Setting_Paths);
        if (props == null) {
            logger.error("无配置内容可执行");
            XxlJobHelper.handleFail("无配置内容可执行");
            return;
        }
        genDataView = props.getStr("GenDataView").trim();
        tableRule = props.getStr("TableRule").trim();
        table = props.getStr("Table").trim();
        idColumn = props.getStr("IdColumn").trim();
        statusColumn = props.getStr("StatusColumn").trim();
        token = props.getStr("Token").trim();
        secret = props.getStr("Secret").trim();
        writeMsgProcedure = props.getStr("WriteMsgProcedure").trim();
        sendDingMsg = props.getStr("SendDingMsg").trim();
        if (sendDingMsg.equals("Y")) {
            DingTalkUtil.LoadSetting(props);
        }
        //存储过程名称
        taskProcedure = props.getStr("TaskProcedure").trim();
//	 #自动任务分类标识
        taskCategory = props.getStr("TaskCategory").trim();
//	 #自动任务执行者标记（用于区分执行进程）
        executorCode = props.getStr("ExecutorCode").trim();
//	 #自动任务服务器名称（用于标记服务器）
        serverName = props.getStr("ServerName").trim();
//	 #自动执行任务创建视图，若如则不执行
        buildTaskViewName = props.getStr("BuildTaskViewName").trim();
//	 #执行前检查视图，若如则不执行
        checkBeforeViewName = props.getStr("CheckBeforeViewName").trim();
//	 #执行数据生成视图
//	 setGetDataViewName(props.getStr("GetDataViewName"));
//	 #执行后检查视图，若如则不执行
        checkAfterViewName = props.getStr("CheckAfterViewName").trim();
//	 #是否发送Ding信息'N'：不发送,'Y'：发送
        sendDingMsg = props.getStr("SendDingMsg").trim();
        theProcName = props.getStr("ProcName").trim();
//	插入待处理任务数据
        if (buildTaskViewName.equals("") == false) {
            lRecordCount = autoTaskDAO.insertDataByView2(table, tableRule, statusColumn, serverName, " ", buildTaskViewName);
            logger.debug("插入数据行数:{}", lRecordCount);
        }
        //  更新任务为本服务待处理内容
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");//设置日期格式
        batchCode = df.format(new Date());
        lRecordCount = autoTaskDAO.updateStatusByCategory2(table, tableRule, statusColumn, 5, serverName, executorCode, " ", batchCode, taskCategory, 0);
        logger.debug("更新数据行数:{}", lRecordCount);
//	读取任务数据集
        List<Map<String, String>> listTD = autoTaskDAO.getData2(table, tableRule, statusColumn,taskCategory, 5, executorCode);
        if (listTD == null || listTD.size() == 0) {
            logger.info("调用成功，无任务可执行");
            XxlJobHelper.handleFail("调用成功，无任务可执行");
            return;
        }
        for (Map<String, String> dataMap : listTD) {
            if (StringUtils.isNotEmpty(checkBeforeViewName)) {
                ck = this.autoTaskDAO.getCkeckResult(checkBeforeViewName, dataMap.get(idColumn));
                if ((ck == null) || (ck.getCode().intValue() != 1)) {
                    continue;
                }
            }
            if (StringUtils.isNotEmpty(theProcName)) {
                String[] filterParams = RuleUtil.getForteParam(dataMap.get("key1"), dataMap.get("key2"), dataMap.get("key3"));
                logger.debug("执行调用：" + Arrays.toString(filterParams));
                ExceedUtil.LoadSetting(props);
                result = ExceedUtil.runFunc(theProcName, filterParams);
                logger.debug("执用完成：" + result.toString());
                if (result.get("code").equals("7")) {
                    lRecordCount = autoTaskDAO.updateStatusByKey2(
                            table,
                            tableRule,
                            statusColumn,
                            7,
                            serverName,
                            executorCode,
                            "执行错误:" + result.get("msg"),
                            dataMap.get(idColumn));
                    continue;
                }
            }
            genData(dataMap);
            if (StringUtils.isNotEmpty(taskProcedure)) {
                paramMap.put("produceName", taskProcedure);
                paramMap.put("key1", dataMap.get("KEY1"));
                paramMap.put("key2", dataMap.get("KEY2"));
                paramMap.put("key3", dataMap.get("KEY3"));
                try {
                    autoTaskDAO.executeProduce3(paramMap);
                } catch (Exception e) {
                    lRecordCount = autoTaskDAO.updateStatusByKey2(table, tableRule, statusColumn, 7, serverName, executorCode, "ProcedureFailed",
                            dataMap.get(idColumn));
                    e.printStackTrace();
                }
            }
            if (StringUtils.isNotEmpty(checkAfterViewName)) {
                ck = this.autoTaskDAO.getCkeckResult(checkAfterViewName, String.valueOf(dataMap.get(idColumn)));
                if ((ck == null) || (ck.getCode().intValue() != 1)) {
                    lRecordCount = autoTaskDAO.updateStatusByKey2(table,
                            tableRule,
                            statusColumn,
                            7,
                            serverName,
                            executorCode,
                            "执后检查失败",
                            dataMap.get(idColumn));
                }
            }
        }
        XxlJobHelper.log(new StringBuilder().append("程序执行时长(s)：").append(timer.intervalSecond()).toString());
        XxlJobHelper.handleSuccess("Call DBOutHandler Success");
    }

}
