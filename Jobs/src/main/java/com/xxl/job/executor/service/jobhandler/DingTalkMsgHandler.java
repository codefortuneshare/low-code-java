package com.xxl.job.executor.service.jobhandler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;

import com.pgl.ss.autoexecutor.automsg.AutoMsgDAO;
import com.pgl.ss.autoexecutor.automsg.MessageData;
import com.pgl.ss.utils.DingTalkUtil;
//import com.pgl.ss.utils.DingTalkUtil;
//import com.pgl.ss.utils.dingtalk.DingTalkAsync;
//import com.pgl.ss.autoexecutor.utils.forteconnect.ForteMassage;
//import com.pgl.ss.autoexecutor.utils.forteconnect.ForteReturn;
//import com.pgl.ss.autoexecutor.utils.forteconnect.ForteSocket;
//import com.pgl.ss.autoexecutor.utils.forteconnect.thread.ForteSocketThreadCallBack;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;

import java.util.List;

//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.ExecutionException;
//import java.util.List;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Future;
//import java.util.concurrent.RejectedExecutionException;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/*@JobHander("DingTalkMsgHandler")*/
@Service
public class DingTalkMsgHandler /*extends IJobHandler*/ {
	private static Logger logger = LoggerFactory.getLogger(DingTalkMsgHandler.class);

	public static final String[] Default_Setting_Paths = new String[] { "config/DingTalkMsgHandler.setting",
			"DingTalkMsgHandler.setting" };
//  @Value("${Proc.BuildTaskViewName}")
//  private String buildTaskViewName;

	@Autowired
	AutoMsgDAO msgBuilderDAO;

	private static Props getDefaultSetting(String spath[]) {
		String tmpStr=System.getProperty("user.dir");
		String fileSeparator = System.getProperty("file.separator");  
		if (spath==null||spath.length==0||spath[0].equals("")) {
			for (String SettingPath : Default_Setting_Paths) {
				try {
					return new Props(SettingPath);
				} catch (IORuntimeException ignore) {
					// ignore
					try {

						return new Props(tmpStr+fileSeparator+SettingPath);
					} catch (IORuntimeException ignore1) {
						// ignore
						
					}
				}
			}
		}else {
			try {
				return new Props(spath[0]);
			} catch (IORuntimeException ignore) {
				// ignore
			}
		}
		return null;
	}

	TimeInterval timer = DateUtil.timer();

	@XxlJob("DingTalkMsgHandler")
	public void execute() throws Exception {
		Props props = getDefaultSetting(null);
		if (props == null || props.getStr("MessageView") == null || props.getStr("MessageView").equals("")) {
			logger.error("无配置内容可执行");
			return;
			/*return new ReturnT<String>(ReturnT.FAIL_CODE, "无配置内容可执行");*/
		}
		List<MessageData> listPO = msgBuilderDAO.getMessageData(props.getStr("MessageView"));
		if (listPO == null || listPO.size() < 1) {
			logger.error("无内容可发送");
			return;
			/*return new ReturnT<String>("无内容可发送");*/
		}

		logger.debug("开始发送，当前信息数量：{}", listPO.size());
		for (MessageData msgData : listPO) {
			if (msgData == null || msgData.getTitle().trim().equals("")) {
				/*XxlJobLogger.log("无内容可发送");*/
				logger.info("无内容可发送");
			} else {
//		    DingTalkUtil.sendTextByRobot(dtToken, dtSecret, msgData.getTitle()+":"+msgData.getMessage());
				DingTalkUtil.LoadSetting(props);
				DingTalkUtil.sendTextByRobotAsync(msgData.getTitle() + ":" + msgData.getMessage());
				logger.debug("发送成功:" + msgData.getTitle() + ":" + msgData.getMessage());
			}
		}
		XxlJobHelper.log(new StringBuilder().append("程序执行时长(s)：").append(timer.intervalSecond()).toString());
		XxlJobHelper.handleSuccess("Call DingTalkMsgHandler Success");
	}
}
