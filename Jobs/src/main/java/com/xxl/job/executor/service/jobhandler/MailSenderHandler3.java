package com.xxl.job.executor.service.jobhandler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import cn.hutool.setting.dialect.Props;
import com.pgl.ss.autoexecutor.automsg.AutoMsgDAO;
import com.pgl.ss.autoexecutor.automsg.MessageData;
import com.pgl.ss.autoexecutor.autotask.AutoTaskDAO;
import com.pgl.ss.autoexecutor.autotask.AutoTaskData;
import com.pgl.ss.autoexecutor.autotask.ResultData;
import com.pgl.ss.utils.DingTalkUtil;
import com.pgl.ss.utils.ExcelFileUtil;
import com.pgl.ss.utils.PropsUtil;
import com.pgl.ss.utils.RuleUtil;
import com.pgl.ss.utils.exceed.ExceedUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobFileAppender;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;



@Service
public class MailSenderHandler3 {
    private static Logger logger = LoggerFactory.getLogger(ExceedHandler3.class);
    public static final String[] Default_Setting_Paths = new String[]{"config/MailSenderHandler3.setting",
            "MailSenderHandler3.setting"};
    private String taskProcedure;
    //  @Value("${Proc.TaskCategory}")
    private String taskCategory;
    //  @Value("${Proc.TheProcName}")
    private String theProcName;
    //  @Value("${Proc.CheckBeforeViewName}")
    private String checkBeforeViewName;
    //  @Value("${Proc.CheckAfterViewName}")
    private String checkAfterViewName;
    //  @Value("${Proc.BuildTaskViewName}")
    private String buildTaskViewName;
    //  @Value("${Proc.ExecutorCode}")
    private String executorCode;
    //  @Value("${Proc.ServerName}")
    private String serverName;
    private String sendEmailMsg;
    private String sendDingMsg;
    private String getMsgDataViewName = "";
    private MessageData msgData;
    private String writeMsgProcedure;
    private String token;
    private String secret;


    @Autowired
    AutoTaskDAO autoTaskDAO;
    @Autowired
    AutoMsgDAO msgBuilderDAO;


    //  @Autowired
    ExecutorService executorService;

    TimeInterval timer = DateUtil.timer();
    private String[] columnArray = {"KEY1", "KEY2", "KEY3",
            "O_1", "O_2", "O_3", "O_4", "O_5", "O_6", "O_7", "O_8", "O_9"};

    /**
     * 解析Order字段, 生成排序语句
     * @param orderColumn
     * @return
     */
    public static String resolveOrderColumn(String orderColumn) {
        if (orderColumn == null) {
            return null;
        }
        String[] array = strToListByNum(orderColumn, 2);
        String result = "ORDER BY";
        for (int i = 0; i < array.length; i++) {
            array[i].charAt(0);
            result+=" O_"+array[i].charAt(0);
            if (array[i].charAt(1) == '1') {
                result+=" DESC,";
            } else if (array[i].charAt(1) == '0') {
                result+=" ASC,";
            }
        }
        result = result.substring(0,result.length() - 1);
        return result;
    }

    /**
     * 每隔多少字符为一个元素放入数组中
     */
    public static String[] strToListByNum(String str, int num) {
        int m=str.length()/2;
        if(m*2<str.length()){
            m++;
        }
        String[] strs=new String[m];
        int j=0;
        for(int i=0;i<str.length();i++){
            if(i%2==0){//每隔两个
                strs[j]=""+str.charAt(i);
            }else{
                strs[j]=strs[j]+""+str.charAt(i);
                j++;
            }
        }
        System.out.println(Arrays.toString(strs));
        return strs;
    }


    /**
     * 根据邮件配置记录发送邮件给指定人
     */
    private void sendEmail(MessageData msgData, String key1, String key2, String key3) throws Exception {
        String tmpStr = System.getProperty("user.dir");
        String fileSeparator = System.getProperty("file.separator");
        MailAccount account = new MailAccount(tmpStr+fileSeparator+Default_Setting_Paths[0]);
        List<String> to = new ArrayList<>();
        List<String> cc = new ArrayList<>();
        List<String> bcc = new ArrayList<>();
        if (StringUtils.isNotBlank(msgData.getReveiver1())) {
            String[] toArray = msgData.getReveiver1().split(",");
            to = new ArrayList<>(toArray.length);
            Collections.addAll(to, toArray);
        }
        if (StringUtils.isNotBlank(msgData.getReveiver2())) {
            String[] ccArray = msgData.getReveiver2().split(",");
            cc = new ArrayList<>(ccArray.length);
            Collections.addAll(cc, ccArray);
        }
        if (StringUtils.isNotBlank(msgData.getReveiver3())) {
            String[] bccArray = msgData.getReveiver3().split(",");
            bcc = new ArrayList<>(bccArray.length);
            Collections.addAll(bcc, bccArray);
        }
        if (msgData.getAttachView() == null || msgData.getAttachDir() == null) {
            MailUtil.send(account, to, cc, bcc, msgData.getTitle(), msgData.getMessage(), true);
            return;
        }
        //附件条件查询K_1
        String k_1 = msgData.getK_1();
        //附件条件查询K_2
        String k_2 = msgData.getK_1();
        //附件条件查询K_3
        String k_3 = msgData.getK_3();
        //附件排序字段order1
        String order1 = msgData.getOrder1();
        String[] viewArray = msgData.getAttachView().split(",");
        String orderStrColumn = resolveOrderColumn(order1);
        //附件对应路径
        String[] sheetNameArray = msgData.getSheetName().split(";");
        String[] excelArray = msgData.getAttachView().split(";");
        String dirPathsStr = Optional.ofNullable(msgData.getAttachDir())
                .orElseThrow(()-> new RuntimeException("邮件警告视图未配置ATTACHDir字段"));
        String[] dirArray = dirPathsStr.split(",");
        List<List<?>> sheetDataArray = new ArrayList<>();
        List<File> fileList = new ArrayList<>();
        //循环创建EXCEL
        for (int i = 0; i < excelArray.length; i++) {
            String[] sheetView = excelArray[i].split(",");
            String[] sheetName = sheetNameArray[i].split(",");
            //循环创建Sheet
            for (int z = 0; z < sheetView.length; z++) {
                List<Map> sheetData  = msgBuilderDAO.getDataByView(sheetView[z],key1, key2, key3,k_1, k_2, k_3,"");
                //删除KEY1、KEY2、KEY3、O_1..... O_9字段
                RuleUtil.removeExtactColumn(sheetData);
                sheetDataArray.add(sheetData);
            }
            //若存在附件,则删除再新建；若不存在, 则新建
            ExcelFileUtil.map2ExcelFile(sheetDataArray, sheetName, dirArray[i]);
            File file = new File(dirArray[i]);
            fileList.add(file);
        }
        //发送邮件
        int size = fileList.size();
        File[] fileArray  = (File[])fileList.toArray(new File[size]);
        MailUtil.send(account, to, cc, bcc, msgData.getTitle(), msgData.getMessage(), true, fileArray);
    }


    @XxlJob("mail3")
    public void execute() throws Exception {
        Map<String, String> result;
        Long lRecordCount = (long) 0;
        ResultData ck;
        String sResurt;
        Props props = PropsUtil.getDefaultSetting(null, Default_Setting_Paths);
        if (props == null) {
            XxlJobHelper.handleFail("无配置内容可执行");
            return;
        }
        Map<String, Object> paramMap = new HashMap<>();
        sendEmailMsg = props.getStr("SendEmailMsg").trim();
        writeMsgProcedure = props.getStr("WriteMsgProcedure").trim();
        //存储过程名称
        taskProcedure = props.getStr("TaskProcedure").trim();
//	 #自动任务分类标识
        taskCategory = props.getStr("TaskCategory").trim();
//	 #自动任务执行者标记（用于区分执行进程）
        executorCode = props.getStr("ExecutorCode").trim();
//	 #自动任务服务器名称（用于标记服务器）
        serverName = props.getStr("ServerName").trim();
//	 #自动执行任务创建视图，若如则不执行
        buildTaskViewName = props.getStr("BuildTaskViewName").trim();
//	 #执行前检查视图，若如则不执行
        checkBeforeViewName = props.getStr("CheckBeforeViewName").trim();
        checkAfterViewName = props.getStr("CheckAfterViewName").trim();
//	 #是否发送Ding信息'N'：不发送,'Y'：发送
        theProcName = props.getStr("ProcName").trim();
        getMsgDataViewName = props.getStr("GetMsgDataViewName").trim();
//	插入待处理任务数据
        if (buildTaskViewName.equals("") == false) {
            lRecordCount = autoTaskDAO.insertDataByView(serverName, " ", buildTaskViewName);
            logger.debug("插入数据行数:{}", lRecordCount);
        }
//  更新任务为本服务待处理内容
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");//设置日期格式
        String batchCode = df.format(new Date());
        lRecordCount = autoTaskDAO.updateStatusByCategory(5, serverName, executorCode, " ", batchCode, taskCategory, 0);
        logger.debug("更新数据行数:{}", lRecordCount);
//	读取任务数据集
        List<AutoTaskData> listTD = autoTaskDAO.getData(taskCategory, 5, executorCode);
        if (listTD == null || listTD.size() == 0) {
            logger.info("调用成功，无任务可执行");
            XxlJobHelper.handleFail("无任务可执行");
            return;
        }
        String filterParams[] = null;
        for (AutoTaskData autoTaskData : listTD) {
            //过滤FORTE参数
            filterParams = RuleUtil.getForteParam(autoTaskData.getKey1(), autoTaskData.getKey2(), autoTaskData.getKey3());
            if (checkBeforeViewName.equals("") == false) {
                ck = this.autoTaskDAO.getCkeckResult(checkBeforeViewName, autoTaskData.getAutotaskkey());
                if ((ck == null) || (ck.getCode().intValue() != 1)) {
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, "不符合执行条件",
                            autoTaskData.getAutotaskkey());
                    continue;
                }
            }
            if (theProcName.equals("") == false) {
                logger.debug("执行调用：" + Arrays.toString(filterParams));
                ExceedUtil.LoadSetting(props);
                result = ExceedUtil.runFunc(theProcName, filterParams);
                logger.debug("执用完成：" + result.toString());
                if (result.get("code").equals("7")) {
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, "执行错误:" + result.get("msg"),
                            autoTaskData.getAutotaskkey());
                    continue;
                }
            }
            lRecordCount = autoTaskDAO.updateStatusByKey(9, serverName, executorCode, "成功",
                    autoTaskData.getAutotaskkey());
            if (checkAfterViewName.equals("") == false) {
                ck = this.autoTaskDAO.getCkeckResult(checkAfterViewName, autoTaskData.getAutotaskkey());
                if ((ck == null) || (ck.getCode().intValue() != 1)) {
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, "执后检查失败",
                            autoTaskData.getAutotaskkey());
                }
            }
            if (!taskProcedure.equals("")) {
                paramMap.put("produceName", taskProcedure);
                paramMap.put("key1", filterParams[0]);
                paramMap.put("key2", filterParams[1]);
                paramMap.put("key3", filterParams[2]);
                try {
                    autoTaskDAO.executeProduce3(paramMap);
                } catch (Exception e) {
                    lRecordCount = autoTaskDAO.updateStatusByKey(7, serverName, executorCode, "ProcedureFailed",
                            autoTaskData.getAutotaskkey());
                    e.printStackTrace();
                }
            }
        }
        if ("N".equalsIgnoreCase(sendEmailMsg)) {
            paramMap.put("produceName", writeMsgProcedure);
            paramMap.put("key1", taskCategory);
            paramMap.put("key2", batchCode);
            paramMap.put("key3", token + "," + secret);
            paramMap.put("key4", getMsgDataViewName);
            try {
                autoTaskDAO.executeProduce2(paramMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            msgData=msgBuilderDAO.getOneMessageDataWithKey(getMsgDataViewName, "", "", "");
            sendEmail(msgData, taskCategory, batchCode, " ");
        }
        XxlJobHelper.log(new StringBuilder().append("程序执行时长(s)：").append(timer.intervalSecond()).toString());
        XxlJobHelper.handleSuccess("Call ExceedHandler3 Success");
    }







}
