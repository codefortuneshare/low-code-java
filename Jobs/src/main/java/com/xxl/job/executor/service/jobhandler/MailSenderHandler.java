
package com.xxl.job.executor.service.jobhandler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.extra.mail.GlobalMailAccount;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import cn.hutool.setting.dialect.Props;

import com.pgl.ss.autoexecutor.automsg.AutoMsgDAO;
import com.pgl.ss.autoexecutor.automsg.MessageData;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import com.xxl.job.core.log.XxlJobFileAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MailSenderHandler /*extends IJobHandler*/ {
	private static Logger logger = LoggerFactory.getLogger(MailSenderHandler.class);
	public static final String[] Default_Setting_Paths = new String[] { "config/MailSenderHandler.setting",
			"MailSenderHandler.setting" };
//  @Value("${Proc.BuildTaskViewName}")
//  private String buildTaskViewName;

	@Autowired
	AutoMsgDAO msgBuilderDAO;


	TimeInterval timer = DateUtil.timer();


	private static Props getDefaultSetting(String spath[]) {
		String tmpStr=System.getProperty("user.dir");
		String fileSeparator = System.getProperty("file.separator");
		if (spath==null||spath.length==0||spath[0].equals("")) {
			for (String SettingPath : Default_Setting_Paths) {
				try {
					return new Props(SettingPath);
				} catch (IORuntimeException ignore) {
					// ignore
					try {

						return new Props(tmpStr+fileSeparator+SettingPath);
					} catch (IORuntimeException ignore1) {
						// ignore

					}
				}
			}
		}else {
			try {
				return new Props(spath[0]);
			} catch (IORuntimeException ignore) {
				// ignore
			}
		}
		return null;
	}
	@XxlJob("MailSenderHandler")
	public void execute() throws Exception {
//    if(buildTaskViewName.equals("")) {
//    	logger.error( "无配置内容可执行");
//    	return new ReturnT<String>(ReturnT.FAIL_CODE, "无配置内容可执行");
//    }
		Props props = getDefaultSetting(null);
		if (props == null || props.getStr("MessageView") == null || props.getStr("MessageView").equals("")) {
			logger.error("无配置内容可执行");
			return;
		}
		List<MessageData> listPO = msgBuilderDAO.getMessageData(props.getStr("MessageView"));
		if (listPO == null || listPO.size() < 1) {
			logger.error("无内容可发送");
			return;
		}
		logger.debug("开始发送，当前数据数量： {}", listPO.size());
		for (MessageData msgData : listPO) {
			if (msgData == null || msgData.getTitle().trim().equals("")) {
				logger.info("无内容可发送");
			} else {
				String tmpStr=System.getProperty("user.dir");
				String fileSeparator = System.getProperty("file.separator");
				MailAccount account = new MailAccount(tmpStr+fileSeparator+Default_Setting_Paths[0]);
				String to = msgData.getReveiver1() + "," + msgData.getReveiver2() + "," + msgData.getReveiver3();
				MailUtil.send(account, to, msgData.getTitle(), msgData.getMessage(), true);
				logger.debug("发送成功:" + msgData.getTitle() + ":" + msgData.getMessage());
			}
		}
		XxlJobHelper.log(new StringBuilder().append("程序执行时长(s)：").append(timer.intervalSecond()).toString());
		XxlJobHelper.handleSuccess("Call MailSenderHandler Success");
	}
}
