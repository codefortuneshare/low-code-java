package com.xxl.job.executor.test;


import com.xxl.job.executor.Application;
import com.xxl.job.executor.service.jobhandler.MailSenderHandler2;
import com.xxl.job.executor.service.jobhandler.MailSenderHandler3;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class )
public class MailSenderHandler2Test {
   @Autowired
   private MailSenderHandler2 mail2;
   @Autowired
   private MailSenderHandler3 mail3;



   @Test
   public void testMail3() throws Exception {
      mail3.execute();
   }

   @Test
   public void testMail2() throws Exception {
      mail2.execute();
   }
}
