package com.pgl.ss.utils;

import java.util.Base64;

public class EncryptUtil {
	public static String  encode(String strTmp) {
		try{
			String strEncode = Base64.getEncoder().encodeToString((strTmp).getBytes());
			byte[] byteEncode = strEncode.getBytes();
			byte byteTmp;
			int iLen=byteEncode.length;
			byteTmp=byteEncode[(iLen-2)];
			byteEncode[(iLen-2)] = byteEncode[(iLen-1)];
			byteEncode[(iLen-1)]=byteTmp;
			strEncode = Base64.getEncoder().encodeToString(byteEncode);
			return strEncode;
		}
		catch(Exception e){
			throw new RuntimeException("Error:"+e.getMessage());
		}
	}
	public static String decode(String strTmp) {
		try{
			byte[] byteDecode = Base64.getDecoder().decode(strTmp);
			byte byteTmp;
			int iLen=byteDecode.length;
			byteTmp=byteDecode[(iLen-2)];
			byteDecode[(iLen-2)] = byteDecode[(iLen-1)];
			byteDecode[(iLen-1)]=byteTmp;			
			byteDecode = Base64.getDecoder().decode(new String(byteDecode));
			return new String(byteDecode);
		}
		catch(Exception e){
			throw new RuntimeException("Error:"+e.getMessage());
		}
	}
}
