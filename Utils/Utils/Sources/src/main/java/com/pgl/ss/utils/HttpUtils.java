package com.pgl.ss.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
* @author zhangzihao
* @version 创建时间：2018年5月8日 下午3:55:38
* 类说明:
*/
public class HttpUtils {
	public static void main(String[] args) {
		String parma = "{\"orderNum\":\"string\"}";
//		String result = sendPost("http://10.2.64.58:8088/forte/executeSql",parma);
		String result = sendPost("http://10.10.9.103:10058/api/express/getEtmsInfo",
				parma,"UTF-8","json");
		System.out.println("返回值："+result);
	}
	
	public static String sendPost(String http, String content, String encode,String contentType) {
		try {
			URL url = new URL(http);
			
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setConnectTimeout(10000);
			con.setReadTimeout(60 * 1000 * 2);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-type","application/"+contentType);
		    con.setDoInput(true);
			con.setDoOutput(true);
			
			OutputStream out = con.getOutputStream();
			
			out.write(content.getBytes(encode));
			out.flush();
//			System.out.println("url:"+http+"\nContent-type:"+con.getContentType()+";content:"+content);
//			System.out.println(con.getResponseCode()+":"+con.getResponseMessage());
			
			InputStream in = con.getInputStream();

			byte[] buf = new byte[content.getBytes(encode).length];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while (true) {
				int readsize = in.read(buf);
				if (readsize == -1)
					break;
				baos.write(buf, 0, readsize);
			}
			in.close();

			return new String(baos.toByteArray(), encode);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	public static String sendPost(String http, String content, String encode) {
		String contentType = "x-www-form-urlencoded";
		return sendPost(http,content,encode,contentType);
		
	}

	
	/**
	 * 
	 */ 
	public static String sendPost(String url, String param) {
        
        String result = "";
        URLConnection  conn =null;
        BufferedReader in=null;
        PrintWriter out=null;
        
      try{
        URL realUrl = new URL(url);
          // 打开和URL之间的连接
        conn = realUrl.openConnection();
          // 设置通用的请求属性
          conn.setRequestProperty("accept", "*/*");
          //conn.setRequestProperty("connection", "Keep-Alive");
          //conn.setRequestProperty("user-agent",
          //        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
          conn.setRequestProperty("Content-type","application/x-www-form-urlencoded");
          // 发送POST请求必须设置如下两行
          conn.setDoOutput(true);
          conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
          out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
           in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            
//            JSONObject jsonObject = new JSONObject(result);
//      if(jsonObject.getString("success").equals("false"))
//      {
//        return result = "!"+jsonObject.getString("message");
//        
//      }
//      else{
//      result = jsonObject.getString("datas");
//      }
        } catch (Exception e) {
          result="!"+e.getMessage();
           // System.out.println("发送 POST 请求出现异常！"+e);
        }
        //使用finally块来关闭输出流、输入流
        finally
        {
          try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
               result = "!"+ex.getMessage();
            }
        }
        return result;
    }    

}
