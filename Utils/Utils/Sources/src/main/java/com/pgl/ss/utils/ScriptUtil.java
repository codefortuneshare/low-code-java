package com.pgl.ss.utils;


import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

public class ScriptUtil
{
  /* Error */
  public static void markScriptFile(String scriptFileName, String content)
    throws IOException
  {
    // Byte code:
    //   0: new 19	java/io/File
    //   3: dup
    //   4: getstatic 21	com/xxl/job/core/log/XxlJobFileAppender:logPath	Ljava/lang/String;
    //   7: invokespecial 27	java/io/File:<init>	(Ljava/lang/String;)V
    //   10: astore_2
    //   11: aload_2
    //   12: invokevirtual 30	java/io/File:exists	()Z
    //   15: ifne +8 -> 23
    //   18: aload_2
    //   19: invokevirtual 34	java/io/File:mkdirs	()Z
    //   22: pop
    //   23: new 19	java/io/File
    //   26: dup
    //   27: aload_2
    //   28: ldc 37
    //   30: invokespecial 39	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   33: astore_3
    //   34: aload_3
    //   35: invokevirtual 30	java/io/File:exists	()Z
    //   38: ifne +8 -> 46
    //   41: aload_3
    //   42: invokevirtual 34	java/io/File:mkdirs	()Z
    //   45: pop
    //   46: aconst_null
    //   47: astore 4
    //   49: new 42	java/io/FileOutputStream
    //   52: dup
    //   53: aload_0
    //   54: invokespecial 44	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   57: astore 4
    //   59: aload 4
    //   61: aload_1
    //   62: ldc 45
    //   64: invokevirtual 47	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   67: invokevirtual 53	java/io/FileOutputStream:write	([B)V
    //   70: aload 4
    //   72: invokevirtual 57	java/io/FileOutputStream:close	()V
    //   75: goto +23 -> 98
    //   78: astore 5
    //   80: aload 5
    //   82: athrow
    //   83: astore 6
    //   85: aload 4
    //   87: ifnull +8 -> 95
    //   90: aload 4
    //   92: invokevirtual 57	java/io/FileOutputStream:close	()V
    //   95: aload 6
    //   97: athrow
    //   98: aload 4
    //   100: ifnull +8 -> 108
    //   103: aload 4
    //   105: invokevirtual 57	java/io/FileOutputStream:close	()V
    //   108: return
    // Line number table:
    //   Java source line #31	-> byte code offset #0
    //   Java source line #32	-> byte code offset #11
    //   Java source line #33	-> byte code offset #18
    //   Java source line #37	-> byte code offset #23
    //   Java source line #38	-> byte code offset #34
    //   Java source line #39	-> byte code offset #41
    //   Java source line #43	-> byte code offset #46
    //   Java source line #45	-> byte code offset #49
    //   Java source line #46	-> byte code offset #59
    //   Java source line #47	-> byte code offset #70
    //   Java source line #48	-> byte code offset #75
    //   Java source line #49	-> byte code offset #80
    //   Java source line #50	-> byte code offset #83
    //   Java source line #51	-> byte code offset #85
    //   Java source line #52	-> byte code offset #90
    //   Java source line #54	-> byte code offset #95
    //   Java source line #51	-> byte code offset #98
    //   Java source line #52	-> byte code offset #103
    //   Java source line #55	-> byte code offset #108
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	109	0	scriptFileName	String
    //   0	109	1	content	String
    //   10	18	2	filePathDir	java.io.File
    //   33	9	3	filePathSourceDir	java.io.File
    //   47	57	4	fileOutputStream	FileOutputStream
    //   78	3	5	e	Exception
    //   83	13	6	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   49	75	78	java/lang/Exception
    //   49	83	83	finally
  }
  
  public static int execToFile(String command, String scriptFile, String logFile, String... params)
    throws IOException
  {
    FileOutputStream fileOutputStream = new FileOutputStream(logFile, true);
    PumpStreamHandler streamHandler = new PumpStreamHandler(fileOutputStream, fileOutputStream, null);
    
    CommandLine commandline = new CommandLine(command);
    commandline.addArgument(scriptFile);
    if ((params != null) && (params.length > 0)) {
      commandline.addArguments(params);
    }
    DefaultExecutor exec = new DefaultExecutor();
    exec.setExitValues(null);
    exec.setStreamHandler(streamHandler);
    int exitValue = exec.execute(commandline);
    return exitValue;
  }
}
