package com.pgl.ss.utils;

import cn.hutool.core.util.ObjectUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *  根据视图参数生成排序语句、条件语句
 */
public class RuleUtil {
    private RuleUtil() {}

    //查询结果的取消字段
    private static final String[] resCancelColumn = {"KEY1", "KEY2", "KEY3",
            "O_1", "O_2", "O_3", "O_4", "O_5", "O_6", "O_7", "O_8", "O_9", "K_1", "K_2", "k_3"};


    /**
     * 根据T_AUTOTASKKEY1、KEY2、KEY3生成ForteParam
     * @param key1
     * @param key2
     * @param key3
     * @return
     */
    public static String[] getForteParam(String key1, String key2, String key3) {
        String[] keyArray = {key1, key2, key3};
        List<String> list = new ArrayList<>(keyArray.length);
        for (int j = 0; j < keyArray.length; j++) {
            //#NA代表该KEY1不传
            if (keyArray[j].contains("#NA")) {
                continue;
            }
            if (j == 2) {//若为第三个KEY, 则逗号拆分
                String[] thirdParams = keyArray[j].split(",");
                for (int i = 0; i < thirdParams.length; i++) {
                    if ("null".equalsIgnoreCase(keyArray[i])) {
                        list.add("");
                    } else {
                        list.add(thirdParams[i]);
                    }
                }
            } else {//若为第三个KEY, 则逗号拆分
                if (keyArray[j].contains("null")) {
                    list.add("");
                } else {
                    list.add(keyArray[j]);
                }
            }
        }
        String[] forteParamArray = list.toArray(new String[]{});
        return forteParamArray;
    }


    /**
     * 集合过滤指定字段
     * @param srcList
     * @return
     */
     public static void removeExtactColumn(List<Map> srcList) {
            for (Map record:srcList) {
                for (String column: resCancelColumn) {
                    record.remove(column);
                }
            }
    }

    /**
     * 【每n位字符串】作为【一元素】放入数组
     * For Example:
     * @param str: 1021
     * @param num: 2
     * @return [10,21]
     */
    public static String[] strToListByNum(String str, int num) {
        int m=str.length()/2;
        if(m*2<str.length()){
            m++;
        }
        String[] strs=new String[m];
        int j=0;
        for(int i=0;i<str.length();i++){
            if(i%2==0){//每隔两个
                strs[j]=""+str.charAt(i);
            }else{
                strs[j]=strs[j]+""+str.charAt(i);
                j++;
            }
        }
        System.out.println(Arrays.toString(strs));
        return strs;
    }

    /**
     * 根据规则生成排序、查询语句
     * @param rule:['123','456','1120']
     * @return: where key1 = '123' and key2 ='456' order by o_1 desc,order by o_2 asc
     */
    public static String getWhereOrOrderStatement(String rule) {
        if (rule == null || rule == "") {
            return null;
        }
        String[] ruleArray = rule.split(",");
        StringBuilder sdf = new StringBuilder("");
        for (int i = 0; i < ruleArray.length - 1; i++) {
            sdf.append("where key" + (i+1) + "=" + ruleArray[i] + " and ");
        }
        String whereStr = sdf.toString();
        sdf.delete(whereStr.length() - 5, whereStr.length() - 1);
        String[] array = strToListByNum(ruleArray[ruleArray.length - 1], 2);
        sdf.append(" ORDER BY");
        for (int i = 0; i < array.length; i++) {
            sdf.append(" O_"+ (i+1));
            if (array[i].charAt(1) == '1') {
                sdf.append(" DESC,");
            } else if (array[i].charAt(1) == '0') {
                sdf.append(" ASC,");
            }
        }
        sdf.deleteCharAt(sdf.toString().length() - 1);
        return sdf.toString();
    }
}
