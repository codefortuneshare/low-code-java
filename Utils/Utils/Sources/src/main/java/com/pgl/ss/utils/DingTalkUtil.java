package com.pgl.ss.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
/**
 * 发送钉钉通知异步请求不需要返回信息
 * @author zhangzihao
 *
 */
public class DingTalkUtil {
	private static final Logger logger = LoggerFactory.getLogger(DingTalkUtil.class);
	public static final String URL = "https://oapi.dingtalk.com/robot/send";
	public static final String TOKENSECRET_SPLIT = "_";
	public static final String[] Default_Setting_Paths = new String[]{"config/dingtalk.setting", "config/dingtalkAccount.setting", "dingtalk.setting"};
	public static String token;
	public static String secret;
	private static Boolean settingloaded=false;

	/**
	 * 创建默认帐户
	 * 
	 * @return MailAccount
	 */
	private static boolean getDefaultSetting() {
		for (String SettingPath : Default_Setting_Paths) {
			Props props;
			try {
				props=new Props(SettingPath);
				return LoadSetting(props);
			} catch (IORuntimeException ignore) {
				//ignore
			}
		}
		return false;
	}	
	public static boolean LoadSetting(Props props) {
		token=props.getStr("Token").trim();
		secret=props.getStr("Secret").trim();
		settingloaded=true;
		return true;
	}

	public static void sendTextByRobotAsync(String content) {
		Thread t = new Thread(()-> {
			String res = sendTextByRobot(content);
			logger.debug("钉钉通知返回：{}",res);
		});
		t.start();
	}
	
	public static void sendTextByRobotAsync(String accessToken, String secret, String content) {
		Thread t = new Thread(()-> {
			String res = sendTextByRobot(accessToken, secret, content);
			logger.debug("钉钉通知返回：{}",res);
		});
		t.start();
	}
	/**
	 * 发送普通文本
	 * @param tokenSecret 格式就是token在前Secret在后中间以下划线（_）分割
	 * @param title
	 * @param content
	 * @return
	 */
	public static void sendTextByRobotAsync(String tokenSecret, String content) {
		Thread t = new Thread(()-> {
			String res = sendTextByRobot(tokenSecret, content);
			logger.debug("钉钉通知返回：{}",res);			
		});
		
		t.start();

	}
	/**
	 * link类型
	 * @param accessToken
	 * @param secret
	 * @param title
	 * @param text
	 * @param picUrl
	 * @param messageUrl
	 * @return
	 */
	public static void sendLinkByRobotAsync(String accessToken,String secret,
			String title, String text,String picUrl,String messageUrl) {
		new Thread(()-> {
			String res = sendLinkByRobot(accessToken, secret, title, text, picUrl, messageUrl);
			logger.debug("钉钉通知返回：{}",res);
		}).start();
	}
	/**
	 *  link类型
	 * @param tokenSecret 格式就是token在前Secret在后中间以下划线（_）分割
	 * @param title
	 * @param text
	 * @param picUrl
	 * @param messageUrl
	 * @return
	 */
	public static void sendLinkByRobotAsync(String tokenSecret,
			String title, String text,String picUrl,String messageUrl) {
		new Thread(()-> {
			String res = sendLinkByRobot(tokenSecret, title, text, picUrl, messageUrl);
			logger.debug("钉钉通知返回：{}",res);
		}).start();
	}



	  private static String signData(String secret, Long timestamp)
	  {
	    String returnString = null;
	    String stringToSign = timestamp + "\n" + secret;
	    try
	    {
	      Mac mac = Mac.getInstance("HmacSHA256");
	      mac.init(new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256"));
	      byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
	      returnString = URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
	    } catch (NoSuchAlgorithmException e) {
	      e.printStackTrace();
	    } catch (InvalidKeyException e) {
	      e.printStackTrace();
	    } catch (UnsupportedEncodingException e) {
	      e.printStackTrace();
	    }
	    return returnString;
	  }

	
	  public static String sendTextByRobot(String content)
	  {
		  if (settingloaded==false) {
			  if (!getDefaultSetting()) {
				  /**
				   * 以钉钉的格式返回信息
				   * update by lzw 2021-07-26
				   */
				  return "{\"errcode\":-1,\"errmsg\":\"err：缺少配置，发送失败\"}";
			  }
		  }
		  return sendTextByRobot(token, secret, content);
	  }
	  
	  public static String sendTextByRobot(String accessToken, String secret, String content)
	  {
	    String accessTokenUrl = getUrl(accessToken, secret);
	    Map<String, String> textMap = new HashMap<String, String>();
	    textMap.put("content", content);
	    Map<String, Object> jsonMap = new HashMap<String, Object>();
	    jsonMap.put("msgtype", "text");
	    jsonMap.put("text", textMap);
	    String responseStr = sendPost(accessTokenUrl, JSON.toJSONString(jsonMap));
	    logger.info(responseStr);
	    return responseStr;
	  }

	/**
	 * 发送post请求，出现异常时以钉钉的格式返回信息
	 * add by lzw 2021-08-23
	 */
	public static String sendTextByRobot2(String accessToken, String secret, String content){
		String accessTokenUrl = getUrl(accessToken, secret);
		Map<String, String> textMap = new HashMap<String, String>();
		textMap.put("content", content);
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		jsonMap.put("msgtype", "text");
		jsonMap.put("text", textMap);
		String responseStr = sendPost2(accessTokenUrl, JSON.toJSONString(jsonMap));
		logger.debug(responseStr);
		return responseStr;
	}

	  public static String sendTextByRobot(String tokenSecret, String content)
	  {
	    String[] array = getTokenSecretByStr(tokenSecret);
	    if (array == null) {
	      logger.info("token无法识别,tokenSecret={}", tokenSecret);
	      return null;
	    }
	    return sendTextByRobot(array[0], array[1], content);
	  }

	  public static String sendLinkByRobot(String accessToken, String secret, String title, String text, String picUrl, String messageUrl)
	  {
	    String accessTokenUrl = getUrl(accessToken, secret);
	    Map<String, String> linkMap = new HashMap<String, String>();
	    linkMap.put("title", title);
	    linkMap.put("text", text);
	    linkMap.put("picUrl", picUrl);
	    linkMap.put("messageUrl", messageUrl);
	    Map<String, Object> jsonMap = new HashMap<String, Object>();
	    jsonMap.put("msgtype", "link");
	    jsonMap.put("link", linkMap);
	    String responseStr = sendPost(accessTokenUrl, JSON.toJSONString(jsonMap));
	    logger.debug(responseStr);
	    return responseStr;
	  }

	  public static String sendLinkByRobot(String tokenSecret, String title, String text, String picUrl, String messageUrl)
	  {
	    String[] array = getTokenSecretByStr(tokenSecret);
	    if (array == null) {
	      logger.info("token无法识别,tokenSecret={}", tokenSecret);
	      return null;
	    }
	    return sendLinkByRobot(array[0], array[1], title, text, picUrl, messageUrl);
	  }
	  private static String[] getTokenSecretByStr(String tokenSecret) {
	    if (tokenSecret == null) {
	      return null;
	    }

	    if ((tokenSecret.indexOf("_") < 0) || 
	      (tokenSecret
	      .indexOf("_") != 
	      tokenSecret.lastIndexOf("_"))) {
	      return null;
	    }
	    String[] array = tokenSecret.split("_");
	    if (array.length != 2) {
	      return null;
	    }
	    return array;
	  }

	  private static String getUrl(String accessToken, String secret)
	  {
	    String accessTokenUrl = "https://oapi.dingtalk.com/robot/send?access_token=" + accessToken;
	    Long timestamp = Long.valueOf(System.currentTimeMillis());
	    String sign = signData(secret, timestamp);
	    accessTokenUrl = accessTokenUrl + "&timestamp=" + timestamp + "&sign=" + sign;
		  logger.debug("url："+accessTokenUrl);
	    return accessTokenUrl;
	  }

	  private static String sendPost(String curUrl, String context)
	  {
	    logger.debug("请求地址:{}", curUrl);
	    logger.debug("发送数据:{}" + context);
	    OkHttpClient client = new OkHttpClient();
	    String responseBodyStr = null;
	    @SuppressWarnings("deprecation")
		RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), context);

	    Request request = new Request.Builder().url(curUrl)
	      .post(requestBody)
	      .build();
	    Response response = null;
	    try {
	      response = client.newCall(request).execute();
	      responseBodyStr = response.body().string();
	    } catch (IOException e) {
	      logger.info("post请求异常IOException" + e.getMessage());
	      e.printStackTrace();
	    }
	    if ((responseBodyStr == null) || (responseBodyStr.isEmpty())) {
	      logger.info("返回数据为空请检查地址情况，{}", curUrl);
	    }
	    return responseBodyStr;
	  }

	/**
	 * 发送post请求，出现异常时以钉钉的格式返回信息
	 * add by lzw 2021-07-26
	 */
	public static String sendPost2(String url, String body) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		post.addHeader("Content-type","application/json;charset=utf-8");
		StringEntity entity = new StringEntity(body,"UTF-8");
		post.setEntity(entity);
		HttpResponse response = null;
		try {
			response = client.execute(post);
			int resStatu = response.getStatusLine().getStatusCode();// 返回码
			if (resStatu == HttpStatus.SC_OK) {// 200正常 其它算错误
				HttpEntity httpResponseEntity = response.getEntity();
				String re = EntityUtils.toString(httpResponseEntity,"UTF-8");
				logger.debug("响应内容："+re);
				//释放资源
				EntityUtils.consume(httpResponseEntity);
				return re;
			}
			return "{\"errcode\":"+resStatu+",\"errmsg\":\""+resStatu+"\"}";
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return "{\"errcode\":-1,\"errmsg\":\""+e.getMessage()+"\"}";
		} catch (IOException e) {
			e.printStackTrace();
			return "{\"errcode\":-1,\"errmsg\":\""+e.getMessage()+"\"}";
		}
	}

	  public static void main(String[] args) {
	    String token = "e9ea7e55d23607ce599650bdca70a0c6a4ef155d57a2eca83358076104bee697_SEC43826ee79cfdb8b2d1a6dcc3e53a9c7175b927d94d70630e9c3e4a18e2b1c64b";

	    String res = sendLinkByRobot(token, "123", "456", "", "http://www.baidu.com");
	    System.out.println(res);
	  }
}

