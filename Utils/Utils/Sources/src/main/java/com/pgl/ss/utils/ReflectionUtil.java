package com.pgl.ss.utils;

import java.lang.reflect.InvocationTargetException;
//import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;


public class ReflectionUtil {
	//private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static String ReflectionRun(String className, String methodName,Object... args) {
		Object oReturn = null;
		try {
			Class<?> clt=Class.forName(className);
			//java.lang.reflect.Method mythod=clt.getMethod(methodName,args[0].getClass());
			if (args==null||args.length==0) {
				oReturn=clt.getMethod(methodName).invoke(clt.newInstance());
			}
			else if (args.length==1) {
				oReturn=clt.getMethod(methodName,args[0].getClass()).invoke(clt.newInstance(),args[0]);
			}
			else if (args.length==2) {
				oReturn=clt.getMethod(methodName,args[0].getClass(),args[1].getClass()).invoke(clt.newInstance(),args[0],args[1]);
			}
			else if (args.length==3) {
				oReturn=clt.getMethod(methodName,args[0].getClass(),args[1].getClass(),args[2].getClass()).invoke(clt.newInstance(),args[0],args[1],args[2]);
			}
			else if (args.length==4) {
				oReturn=clt.getMethod(methodName,args[0].getClass(),args[1].getClass(),args[2].getClass(),args[3].getClass()).invoke(clt.newInstance(),args[0],args[1],args[2],args[3]);
			}
			else if (args.length==5) {
				oReturn=clt.getMethod(methodName,args[0].getClass(),args[1].getClass(),args[2].getClass(),args[3].getClass(),args[4].getClass()).invoke(clt.newInstance(),args[0],args[1],args[2],args[3],args[4]);
			}
			return oReturn.toString();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:No Such Method";
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Security Exception";
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Illegal Access";
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Illegal Argument";
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Invocation Target";
		} catch (ClassNotFoundException e) {		
			e.printStackTrace();
			return "err:Unknow Class";
		} catch (InstantiationException e) {
			e.printStackTrace();
			return "err:Method Instantiation Error";
		}
		}
	public static String ReflectionRun(Object obj, String methodName,Object... args) {
		Object oReturn = null;
		try {
			if (args==null||args.length==0) {
				oReturn=obj.getClass().getMethod(methodName).invoke(obj);
			}
			else if (args.length==1) {
				oReturn=obj.getClass().getMethod(methodName,args[0].getClass()).invoke(obj,args[0]);
			}
			else if (args.length==2) {
				oReturn=obj.getClass().getMethod(methodName,args[0].getClass(),args[1].getClass()).invoke(obj,args[0],args[1]);
			}
			else if (args.length==3) {
				oReturn=obj.getClass().getMethod(methodName,args[0].getClass(),args[1].getClass(),args[2].getClass()).invoke(obj,args[0],args[1],args[2]);
			}
			else if (args.length==4) {
				oReturn=obj.getClass().getMethod(methodName,args[0].getClass(),args[1].getClass(),args[2].getClass(),args[3].getClass()).invoke(obj,args[0],args[1],args[2],args[3]);
			}
			else if (args.length==5) {
				oReturn=obj.getClass().getMethod(methodName,args[0].getClass(),args[1].getClass(),args[2].getClass(),args[3].getClass(),args[4].getClass()).invoke(obj,args[0],args[1],args[2],args[3],args[4]);
			}
			return oReturn.toString();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:No Such Method";
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Security Exception";
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Illegal Access";
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Illegal Argument";
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "err:Invocation Target";
//		} catch (ClassNotFoundException e) {		
//			e.printStackTrace();
//			return "err:Unknow Class";
//		} catch (InstantiationException e) {
//			e.printStackTrace();
//			return "err:Method Instantiation Error";
		}
		}
	
	/**
	 * 前台传过来的参数转成map
	 * @param params
	 * @return
	 */
	public Map<String ,String> putParamsToMap(String[] params){
		Map<String ,String> map = new HashMap<>();
		for(String param:params) {
			String[] key_v = param.split("=");
			if(key_v.length!=2) {
				return null;
			}
			map.put(key_v[0].replaceAll(" +",""), key_v[1].replaceAll(" +",""));
		}
		return map;
	}
	  public String getUUID()
	  {
	    UUID uuid = UUID.randomUUID();
	    String str = uuid.toString();
	    String uuidStr = str.replace("-", "");
	    return uuidStr;
	  }
	}

	


