package com.pgl.ss.utils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
// 扫描MyBatis的SQL动态代理接口的配置文件
//@MapperScan("com.xxl.job.executor.pgl.dao")

public class Application {

	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        System.out.println("执行器xxl-job-executor-mk 启动成功");
	}

}