package com.pgl.ss.utils;

//import java.io.File;
//import java.io.IOException;
//import java.net.URLEncoder;
//import java.util.ArrayList;

import java.io.*;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;


/*import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;*/


/*import cn.hutool.extra.mail.MailAccount;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;*/
import cn.hutool.core.lang.Assert;
import cn.hutool.poi.excel.RowUtil;
import org.apache.poi.poifs.filesystem.FileMagic;
/*import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;*/

import java.io.InputStream;
import java.util.List;
//import java.util.Map;
//import java.util.NoSuchElementException;
import java.util.Map.Entry;
/*import java.util.Properties;*/
/*import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;*/
//import org.apache.poi.ss.usermodel.CellStyle;
//import org.apache.poi.ss.usermodel.HorizontalAlignment;

//import javax.servlet.http.HttpServletResponse;
//import org.apache.commons.lang.*;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.poi.excel.ExcelUtil;

//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.util.StringUtils;
//import org.springframework.web.multipart.MultipartFile;

import cn.hutool.poi.excel.ExcelWriter;
import org.apache.poi.ss.util.CellRangeAddress;
/*import org.springframework.util.ObjectUtils;*/

/*//import cn.afterturn.easypoi.excel.ExcelExportUtil;
//import cn.afterturn.easypoi.excel.ExcelImportUtil;
//import cn.afterturn.easypoi.excel.entity.ExportParams;
//import cn.afterturn.easypoi.excel.entity.ImportParams;
//import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;*/
/**
 * 通过Excel文件生成工具.

 */
public class ExcelFileUtil {


	/**
	 * 判断是否为Excel文件
	 * @param
	 * @return
	 * @throws IOException
	 */
	public static Boolean isExcelFile(File file) throws IOException {
        boolean result = false;
        try {
			FileMagic fileMagic = FileMagic.valueOf(file);
			if (Objects.equals(fileMagic, FileMagic.OLE2) || Objects.equals(fileMagic, FileMagic.OOXML)) {
				result = true;
			}
		} catch (IOException e) {
        	e.printStackTrace();
		}
        return result;
	}

	/**
	 * 创建多个Sheet的Excel
	 * @param dataList
	 * @param sheetName
	 * @param genPath
	 */
	public static void createExcel(List<List<?>> dataList, String[] sheetName, String genPath) throws IOException {
		Workbook wb = new HSSFWorkbook();
		HashMap<String,Object> mapTmp = null;
		CellRangeAddress region = null;
		Sheet newSheet = null;
		Row row = null;





		//标题相关对象
		Cell cell = null;
		CellStyle style = wb.createCellStyle();
		Font baseFont = wb.createFont();
		baseFont.setFontName("微软雅黑");
		baseFont = wb.createFont();
		baseFont.setBold(true);
		baseFont.setFontName("微软雅黑");
		baseFont.setColor(Short.valueOf("30"));
		baseFont.setFontHeightInPoints((short)20);
		style.setFont(baseFont);
		style.setWrapText(true);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		//表头相关对象
		Cell colCell = null;
		CellStyle colStyle = wb.createCellStyle();
		Font colFont = wb.createFont();
		colStyle.setBorderTop(BorderStyle.THIN);
		colStyle.setBorderBottom(BorderStyle.THIN);
		colStyle.setBorderLeft(BorderStyle.THIN);
		colStyle.setBorderRight(BorderStyle.THIN);
		colStyle.setFont(colFont);
		colStyle.setWrapText(true);
		colStyle.setFillForegroundColor(Short.valueOf("30"));
		colStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		colStyle.setAlignment(HorizontalAlignment.CENTER);
		colStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		//数据表格相关对象
		Row dataRow = null;
		Cell dataCell = null;
		CellStyle dataStyle = wb.createCellStyle();
		Font dataFont = wb.createFont();
		dataFont.setFontName("微软雅黑");
		dataFont = wb.createFont();
		dataFont.setBold(true);
		dataFont.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
		dataFont.setFontHeightInPoints((short)11);
		dataStyle.setFont(dataFont);
		dataStyle.setWrapText(true);
		dataStyle.setAlignment(HorizontalAlignment.CENTER);
		dataStyle.setVerticalAlignment(VerticalAlignment.CENTER);


		for (int i = 0; i < sheetName.length; i++) {
			//若该Sheet数据为空,则只创建标题
			if (dataList.get(i).size() == 0) {
				//创建Sheet - 设置列宽为14
				newSheet = wb.createSheet(sheetName[i]);
				newSheet.setDefaultColumnWidth(14);
				//设置标题 - 设置字体样式
				row = newSheet.createRow(0);
				row.setHeight(Short.valueOf("1200"));
				cell = row.createCell(0);
				cell.setCellValue(sheetName[i]);
				style.setFont(baseFont);
				cell.setCellStyle(style);
				//设置标题 - 合并第一行单元格
				region = new CellRangeAddress(0, 0, 0, 6);
				region.setFirstRow(0);
				newSheet.addMergedRegion(region);
		    } else {
				//创建Sheet - 设置列宽为14
				newSheet = wb.createSheet(sheetName[i]);
				newSheet.setDefaultColumnWidth(14);
				//设置标题 - 设置字体样式
				row = newSheet.createRow(0);
				row.setHeight(Short.valueOf("1200"));
				cell = row.createCell(0);
				cell.setCellValue(sheetName[i]);
				cell.setCellStyle(style);
				mapTmp =(HashMap<String, Object>) dataList.get(i).get(0);
				region  = new CellRangeAddress(0, 0, 0,  mapTmp.keySet().size() - 1);
				region.setFirstRow(0);
				newSheet.addMergedRegion(region);
				//创建表头列名
				row = newSheet.createRow(1);
				row.setHeight(Short.valueOf("1200"));
				int j = 0;
				for(String key: mapTmp.keySet()){
					colCell = row.createCell(j);
					colCell.setCellValue(key);
					colCell.setCellStyle(colStyle);
					j++;
				}
				for (int n = 0; n < mapTmp.keySet().size(); n++) {
					newSheet.autoSizeColumn(n);
					newSheet.setColumnWidth(n,newSheet.getColumnWidth(n)*17/10);
				}
				for (int z = 0; z < dataList.get(i).size(); z++) {
					j = 0;
					dataRow = newSheet.createRow(z + 2);
					mapTmp = (HashMap<String, Object>) dataList.get(i).get(z);
					for(Object value : mapTmp.values()){
						dataCell = dataRow.createCell(j);
						dataCell.setCellValue(String.valueOf(value));
						dataStyle.setFont(dataFont);
						dataCell.setCellStyle(dataStyle);
						j++;
					}
				}
			}
		}
		File file = new File(genPath);
		if (file.exists()) {
			file.delete();
		}
		OutputStream out = new FileOutputStream(genPath);
		wb.write(out);
		out.flush();
		out.close();
	}

	@SuppressWarnings({ "unchecked" })
	public static void map2ExcelFile(List<?> dataMapList,String fileName) {
		List<String> titleList = CollUtil.newArrayList();
		List<Object> dataList = CollUtil.newArrayList();
		HashMap<String,Object> mapTmp=(HashMap<String, Object>) dataMapList.get(0);
		for(Entry<String,Object> entryTmp:mapTmp.entrySet()) {
			titleList.add(entryTmp.getKey());
		}
		ExcelWriter overtimeWriter = ExcelUtil.getWriter(fileName);
//		CellStyle cRight = overtimeWriter.getCellStyle();
//		cRight.setDataFormat((short)2);
//		cRight.setAlignment(HorizontalAlignment.RIGHT);
//		CellStyle cCenter = overtimeWriter.getCellStyle();
//		cCenter.setDataFormat((short)2);
//		cCenter.setAlignment(HorizontalAlignment.CENTER);
		overtimeWriter.writeRow(titleList);
		for (int i=0;i<dataMapList.size();i++) {
			HashMap<String,Object> mapTmp1=(HashMap<String, Object>) dataMapList.get(i);
			for (int j=0;j<titleList.size();j++) {
				dataList.add(mapTmp1.get(titleList.get(j)));
//				if (mapTmp1.get(titleList.get(j)) instanceof Integer || mapTmp1.get(titleList.get(j)) instanceof Double) {
//					overtimeWriter.setStyle(cRight,j,i+1);//自动根据类型判断是不是数字
//				}
//				else {
//					overtimeWriter.setStyle(cCenter,j,i+1);
//				}
			}
			overtimeWriter.writeRow(dataList);
			dataList.clear();
		}
		overtimeWriter.close();
	}

	public static void map2ExcelFile(List<List<?>> dataMapList, String[] sheetName, String fileName) {
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
		}
		List<String> titleList = CollUtil.newArrayList();
		List<Object> dataList = CollUtil.newArrayList();
		ExcelWriter overtimeWriter = ExcelUtil.getWriter(fileName);
		for (int sheetIndex = 0; sheetIndex < sheetName.length; sheetIndex++) {
			if (sheetIndex == 0) {
				overtimeWriter.renameSheet(0, sheetName[sheetIndex]);
			} else {
				overtimeWriter.setSheet(sheetName[sheetIndex]);
			}
			List<?> sheetData = dataMapList.get(sheetIndex);
			if (sheetData.size() > 0) {
				HashMap<String,Object> mapTmp=(HashMap<String, Object>) dataMapList.get(sheetIndex).get(0);
				for(Entry<String,Object> entryTmp:mapTmp.entrySet()) {
					titleList.add(entryTmp.getKey());
				}
				overtimeWriter.writeRow(titleList);
				titleList.clear();
				for (int i=0;i<dataMapList.get(sheetIndex).size();i++) {
					HashMap<String,Object> mapTmp1=(HashMap<String, Object>)dataMapList.get(sheetIndex).get(i);
					for (Object value:mapTmp1.values()) {
						dataList.add(value);
					}
				/*for (int j=0;j<titleList.size();j++) {
					dataList.add(mapTmp1.get(titleList.get(j)));
				}*/
					overtimeWriter.writeRow(dataList);
					dataList.clear();
				}
			}
		}
		overtimeWriter.close();
	}

	/*public static void map2ExcelFile(List<List<?>> dataMapList, String[] sheetName, String fileName) {
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
		}
		List<String> titleList = CollUtil.newArrayList();
		List<Object> dataList = CollUtil.newArrayList();
		ExcelWriter overtimeWriter = ExcelUtil.getWriter(fileName);
		HashMap<String,Object> mapTmp = null;
		for (int sheetIndex = 0; sheetIndex < sheetName.length; sheetIndex++) {
			if (sheetIndex == 0) {
				overtimeWriter.renameSheet(0, sheetName[sheetIndex]);
			} else {
				overtimeWriter.setSheet(sheetName[sheetIndex]);
			}
			mapTmp = (HashMap<String, Object>) dataMapList.get(sheetIndex).get(0);
			for(Entry<String,Object> entryTmp:mapTmp.entrySet()) {
				titleList.add(entryTmp.getKey());
			}
			overtimeWriter.writeRow(titleList);
			titleList.clear();
			int rowLength = dataMapList.get(sheetIndex).size();
			for (int rowIndex=0;rowIndex<rowLength;rowIndex++) {
				mapTmp =(HashMap<String, Object>) dataMapList.get(sheetIndex).get(rowIndex);
				for (int j=0;j<titleList.size();j++) {
					dataList.add(mapTmp.get(titleList.get(j)));
				}
				overtimeWriter.writeRow(dataList);
			}
			dataList.clear();
		}
		overtimeWriter.close();
	}*/
}




//    public static void exportExcel(List<?> list, String title, String sheetName, Class<?> pojoClass, String fileName, boolean isCreateHeader, HttpServletResponse response){
//        ExportParams exportParams = new ExportParams(title, sheetName);
//        exportParams.setCreateHeadRows(isCreateHeader);
//        defaultExport(list, pojoClass, fileName, response, exportParams);
//
//    }
//    public static void exportExcel(List<?> list, String title, String sheetName, Class<?> pojoClass,String fileName, HttpServletResponse response){
//        defaultExport(list, pojoClass, fileName, response, new ExportParams(title, sheetName));
//    }
//    public static void exportExcel(List<Map<String, Object>> list, String fileName, HttpServletResponse response){
//        defaultExport(list, fileName, response);
//    }
//
//    private static void defaultExport(List<?> list, Class<?> pojoClass, String fileName, HttpServletResponse response, ExportParams exportParams) {
//        Workbook workbook = ExcelExportUtil.exportExcel(exportParams,pojoClass,list);
//        if (workbook != null);
//        downLoadExcel(fileName, response, workbook);
//    }
//
//    private static void downLoadExcel(String fileName, HttpServletResponse response, Workbook workbook) {
//        try {
//            response.setCharacterEncoding("UTF-8");
//            response.setHeader("content-Type", "application/vnd.ms-excel");
//            response.setHeader("Content-Disposition",
//                    "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
//            workbook.write(response.getOutputStream());
//        } catch (IOException e) {
//            throw new RuntimeException(e.getMessage());
//        }
//    }
//    private static void defaultExport(List<Map<String, Object>> list, String fileName, HttpServletResponse response) {
//        Workbook workbook = ExcelExportUtil.exportExcel(list, ExcelType.HSSF);
//        if (workbook != null);
//        downLoadExcel(fileName, response, workbook);
//    }
//
//    public static <T> List<T> importExcel(String filePath,Integer titleRows,Integer headerRows, Class<T> pojoClass){
//        if (StringUtils.isBlank(filePath)){
//            return null;
//        }
//        ImportParams params = new ImportParams();
//        params.setTitleRows(titleRows);
//        params.setHeadRows(headerRows);
//        List<T> list = null;
//        try {
//            list = ExcelImportUtil.importExcel(new File(filePath), pojoClass, params);
//        }catch (NoSuchElementException e){
//            throw new RuntimeException("模板不能为空");
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException(e.getMessage());
//        }
//        return list;
//    }
//    public static <T> List<T> importExcel(MultipartFile file, Integer titleRows, Integer headerRows, Class<T> pojoClass){
//        if (file == null){
//            return null;
//        }
//        ImportParams params = new ImportParams();
//        params.setTitleRows(titleRows);
//        params.setHeadRows(headerRows);
//        List<T> list = null;
//        try {
//            list = ExcelImportUtil.importExcel(file.getInputStream(), pojoClass, params);
//        }catch (NoSuchElementException e){
//            throw new RuntimeException("excel文件不能为空");
//        } catch (Exception e) {
//            throw new RuntimeException(e.getMessage());
//        }
//        return list;
//    }
//	}
