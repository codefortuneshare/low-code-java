package com.pgl.ss.utils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import cn.hutool.json.JSONArray;
import cn.hutool.json.XML;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONWriter;
import com.alibaba.fastjson.util.IOUtils;
import com.google.gson.stream.JsonWriter;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import net.sf.saxon.s9api.*;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;

//import javax.xml.bind.Element;

//import org.apache.poi.ooxml.util.DocumentHelper;
//import org.w3c.dom.Document;

public class XMLUtil {

	public static Element list2Xml2(List<?> list,Element element,boolean withType,String listname){
		int i = 0;
		Element nodeElement;
		for (Object o : list) {
			if (StringUtils.isNotEmpty(listname)) {
				nodeElement = element.addElement(listname);
			} else {
				nodeElement = element;
			}
			if (o instanceof Map) {
				if (withType) nodeElement.addAttribute("type", "o");
				Map<?, ?> m = (Map<?, ?>) o;
				for (Iterator<?> iterator = m.entrySet().iterator(); iterator.hasNext();) {
					Entry<?, ?> entry = (Entry<?, ?>) iterator.next();
					Element keyElement = nodeElement.addElement(entry.getKey().toString());
					if (entry.getValue() instanceof List) {
						if (withType) keyElement.addAttribute("type", "l");
						list2xml((List<?>) entry.getValue(),keyElement,withType,listname);
					} else {
						if (withType) keyElement.addAttribute("type", "s");
						if (entry.getValue() != null) {
							keyElement.setText(entry.getValue().toString());
						}
					}
				}
			} else if (o instanceof List) {
				if (withType) nodeElement.addAttribute("type", "l");
				list2xml((List<?>)o,nodeElement,withType,listname);
			}else {
				Element keyElement = nodeElement.addElement("value");
				keyElement.addAttribute("num", String.valueOf(i));
				keyElement.setText(String.valueOf(o));
			}
			i++;
		}
		return element;
	}


	public static void XmlToFile(String inXmlPath, String outFilePath, String outFileType, String fileEncoding, boolean isDeleteInXml) throws Exception {
		if ("json".equalsIgnoreCase(outFileType)) {
			File file=new File(inXmlPath);
			FileInputStream fis = new FileInputStream(file);
			FileChannel fc = fis.getChannel();
			ByteBuffer bb = ByteBuffer.allocate(new Long(file.length()).intValue());
			//fc向buffer中读入数据
			fc.read(bb);
			bb.flip();
			String xmlStr=new String(bb.array(),fileEncoding);
			fc.close();
			fis.close();
			boolean flag = false;
			Document doc= DocumentHelper.parseText(xmlStr);
			JSONObject json=new JSONObject();
			Element element = doc.getRootElement();
			//如果是属性
			for(Object o:element.attributes()){
				Attribute attr=(Attribute)o;
				if(StringUtils.isNotEmpty(attr.getValue())){
					json.put("@"+attr.getName(), attr.getValue());
				}
			}
			List<Element> chdEl=element.elements();
			if(chdEl.isEmpty() && StringUtils.isNotEmpty(element.getText())){//如果没有子元素,只有一个值
				json.put(element.getName(), element.getText());
			}
			for(Element e:chdEl){//有子元素
				if(!e.elements().isEmpty()){//子元素也有子元素
					JSONObject chdjson=new JSONObject();
					toJsonText(e,chdjson);
					Object o=json.get(e.getName());
					if(o!=null){
						JSONArray jsona=null;
						if(o instanceof JSONObject){//如果此元素已存在,则转为jsonArray
							JSONObject jsono=(JSONObject)o;
							json.remove(e.getName());
							jsona=new JSONArray();
							jsona.add(jsono);
							jsona.add(chdjson);
						}
						if(o instanceof JSONArray){
							jsona=(JSONArray)o;
							jsona.add(chdjson);
						}
						json.put(e.getName(), jsona);
					}else{
						if(!chdjson.isEmpty()){
							json.put(e.getName(), chdjson);
						}
					}
				}else{//子元素没有子元素
					for(Object o:element.attributes()){
						Attribute attr=(Attribute)o;
						if(StringUtils.isNotEmpty(attr.getValue())){
							json.put("@"+attr.getName(), attr.getValue());
						}
					}
					if(!e.getText().isEmpty()){
						json.put(e.getName(), e.getText());
					}
				}
			}
			if (isDeleteInXml == true) {
                file.delete();
			}
			JSONWriter writer = new JSONWriter(new FileWriter(new File(outFilePath)));
			writer.writeValue(json);
			writer.flush();
			writer.close();
		}
	}

	/**
	 * 转换为JSON文本
	 * @param element
	 * @param json
	 */
	public static void toJsonText(Element element,JSONObject json){
		//如果是属性
		for(Object o:element.attributes()){
			Attribute attr=(Attribute)o;
			if(StringUtils.isNotEmpty(attr.getValue())){
				json.put("@"+attr.getName(), attr.getValue());
			}
		}
		List<Element> chdEl=element.elements();
		if(chdEl.isEmpty()&&StringUtils.isNotEmpty(element.getText())){//如果没有子元素,只有一个值
			json.put(element.getName(), element.getText());
		}

		for(Element e:chdEl){//有子元素
			if(!e.elements().isEmpty()){//子元素也有子元素
				JSONObject chdjson=new JSONObject();
				toJsonText(e,chdjson);
				Object o=json.get(e.getName());
				if(o!=null){
					JSONArray jsona=null;
					if(o instanceof JSONObject){//如果此元素已存在,则转为jsonArray
						JSONObject jsono=(JSONObject)o;
						json.remove(e.getName());
						jsona=new JSONArray();
						jsona.add(jsono);
						jsona.add(chdjson);
					}
					if(o instanceof JSONArray){
						jsona=(JSONArray)o;
						jsona.add(chdjson);
					}
					json.put(e.getName(), jsona);
				}else{
					if(!chdjson.isEmpty()){
						json.put(e.getName(), chdjson);
					}
				}


			}else{//子元素没有子元素
				for(Object o:element.attributes()){
					Attribute attr=(Attribute)o;
					if(StringUtils.isNotEmpty(attr.getValue())){
						json.put("@"+attr.getName(), attr.getValue());
					}
				}
				if(!e.getText().isEmpty()){
					json.put(e.getName(), e.getText());
				}
			}
		}
	}

	public static String transforXMLByXSLT(String inXmlPath, String xsltPath, String outXmlPath) throws SaxonApiException {
		return transforXMLByXSLT(inXmlPath, xsltPath, outXmlPath, "no", false);
	}

	public static String transforXMLByXSLT(String inXmlPath, String xsltPath, String outXmlPath,  String isIndent, boolean licensedEdition) throws SaxonApiException {
		String oFileSuffix = outXmlPath.substring(outXmlPath.lastIndexOf(".")+1);
		Processor processor = new Processor(licensedEdition);
		XsltCompiler compiler = processor.newXsltCompiler();
		XsltExecutable stylesheet = null;
		stylesheet = compiler.compile(new StreamSource(new File(xsltPath)));
		Serializer out = processor.newSerializer(new File(outXmlPath));
		out.setOutputProperty(Serializer.Property.METHOD, oFileSuffix);
		out.setOutputProperty(Serializer.Property.INDENT, isIndent);
		Xslt30Transformer transformer = stylesheet.load30();
		transformer.transform(new StreamSource(new File(inXmlPath)), out);
		return outXmlPath;
	}

	public static String getXmlAndPathByList(List<?> dataList, String outPutPath, String rootName, String objName, String encoding) throws IOException {
		return getXmlAndPathByList(dataList, outPutPath, rootName, false,  objName, encoding);
	}

    public static String getXmlAndPathByList(List<?> dataList,String outPutPath, String rootName, boolean withType, String objName, String encoding) throws IOException {
		Document doc = DocumentHelper.createDocument();
		Element rootElement = doc.addElement(rootName);
		list2Xml2(dataList,rootElement,withType,objName);
		return getXmlAndPathByDoc(doc, outPutPath, encoding);
	}



	/**
	 * 根据Document生成XML文件和路径
	 * @param
	 * @return
	 */
	public static String getXmlAndPathByDoc(Document doc, String outXmlPath, String encoding) throws IOException {
		try {
			XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(outXmlPath),encoding));
			writer.write(doc);
			writer.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return outXmlPath;
	}
	/**
	* 类转xml方法.
	* @param List<?>
	* @return String
	* @throws
	*/
	@SuppressWarnings("unused")
	private static String map2Xml(Map<String, String> map) {
		Document  document=DocumentHelper.createDocument();
		Element root=document.addElement("xml");
		Set<String>  keys=map.keySet();
		for(String key:keys){
			root.addElement(key).addText(map.get(key));
		}
		StringWriter sw=new StringWriter();
		XMLWriter xw=new XMLWriter(sw);
		xw.setEscapeText(false);
		try {
			xw.write(document);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sw.toString();
		}
//	public String map2xml(Map map) {
//		Document document = DocumentHelper.createDocument();
//		Element nodesElement = document.addElement("ROOT");
//		map2xml(map,nodesElement);
//		
//		return doc2String(document);
//	}

	/**
	* 类转xml方法.
	* @param data List<?>
	* @return String
	* @throws
	*/
	public static String list2xml(List<?> list,String headname,String Listname) {
		Document document = DocumentHelper.createDocument();
		Element nodesElement = document.addElement(headname);
		list2xml(list,nodesElement,false,Listname);
		return doc2String(document);
	}
	
	/**
	* 类转xml方法.
	* @param data List<?>
	* @return String
	* @throws
	*/
	public static String list2xml(List<?> list) {
		Document document = DocumentHelper.createDocument();
		Element nodesElement = document.addElement("DATA");
		list2xml(list,nodesElement,true,"");
		return doc2String(document);
	}

//	2.XML转String 用dom4j的功能实现
	/**
	* xml转为String.
	* @param document
	* @return
	* @throws
	*/
	public static String doc2String(Document document) {
		String s = "";
		try {
			// 使用输出流来进行转化
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			// 使用UTF-8编码
			OutputFormat format = new OutputFormat(" ", true, "UTF-8");
			XMLWriter writer = new XMLWriter(out, format);
			writer.write(document);
			s = out.toString("UTF-8");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return s;
	}
//	3.List2XML方法实现
	/**
	* List2XML,目前支持List<List> List<Map> List<Map<String,List>>等只有
	* List Map 组合的数据进行转换.
	* @param list
	* @param element
	* @return
	* @throws
	*/
	public static Element list2xml(List<?> list,Element element,boolean withType,String listname){
		int i = 0;
		Element nodeElement;
		for (Object o : list) {
			if (("").equals(listname)) {
				nodeElement = element.addElement("LIST");
			}else {
				nodeElement = element.addElement(listname);
			}
			if (o instanceof Map) {
				if (withType) nodeElement.addAttribute("type", "o");
				Map<?, ?> m = (Map<?, ?>) o;
				for (Iterator<?> iterator = m.entrySet().iterator(); iterator.hasNext();) {
					Entry<?, ?> entry = (Entry<?, ?>) iterator.next();
					Element keyElement = nodeElement.addElement(entry.getKey().toString());
					if (entry.getValue() instanceof List) {
						if (withType) keyElement.addAttribute("type", "l");
						list2xml((List<?>) entry.getValue(),keyElement,withType,listname);
					} else {
						if (withType) keyElement.addAttribute("type", "s");
						if (entry.getValue() != null) {
							keyElement.setText(entry.getValue().toString());
						}
					}
				}
			} else if (o instanceof List) {
				if (withType) nodeElement.addAttribute("type", "l");
				list2xml((List<?>)o,nodeElement,withType,listname);
			}else {
				Element keyElement = nodeElement.addElement("value");
				keyElement.addAttribute("num", String.valueOf(i));
				keyElement.setText(String.valueOf(o));
			}
			i++;
		}
		return element;
	}

//	4.xml2List
	/**
	* xml转List方法.
	* @param xml
	* @return List<?>
	* @throws
	*/
	public static List<?> xml2List(String xml){
	try {
		List<Object> list = new ArrayList<Object>();
		Document document = DocumentHelper.parseText(xml);
		Element nodesElement = document.getRootElement();
		List<?> nodes = nodesElement.elements();
		for (Iterator<?> its = nodes.iterator(); its.hasNext();) {
			Element nodeElement = (Element) its.next();
			if(("l").equals(nodeElement.attributeValue("type"))){
				List<?> s = xml2List(nodeElement.asXML());
				list.add(s);
					s = null;
			}else if(("o").equals(nodeElement.attributeValue("type"))){
				Map<String, ?> map = xml2Map(nodeElement.asXML());
				list.add(map);
				map = null;
			}else{
				list.add(nodeElement.getText());
			}
		}
		nodes = null;
		nodesElement = null;
		document = null;
		return list;
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
	}
//	5.XML2MAP
	/**
	* xml 2 map
	* @param xml
	* @return
	*/
	public static Map<String, ?> xml2Map(String xml) {
	try {
		Map<String,Object> map = new HashMap<String, Object>();
		Document document = DocumentHelper.parseText(xml);
		Element nodeElement = document.getRootElement();
		List<?> node = nodeElement.elements();
		for (Iterator<?> it = node.iterator(); it.hasNext();) {
			Element elm = (Element) it.next();
			if("l".equals(elm.attributeValue("type"))){
				map.put(elm.getName(), xml2List(elm.asXML()));
			}else if("o".equals(elm.attributeValue("type"))){
				map.put(elm.getName(), xml2Map(elm.asXML()));
			}else{
				map.put(elm.getName(), elm.getText());
			}
			elm = null;
		}
		node = null;
		nodeElement = null;
		document = null;
		return map;
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
	}
}
