package com.pgl.ss.utils;

import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;

public class PropsUtil {
   private PropsUtil() {};

   public static Props getDefaultSetting(String spath[], String[] Default_Setting_Paths) {
      String tmpStr = System.getProperty("user.dir");
      String fileSeparator = System.getProperty("file.separator");
      if (spath == null || spath.length == 0 || spath[0].equals("")) {
         for (String SettingPath : Default_Setting_Paths) {
            try {
               return new Props(SettingPath);
            } catch (IORuntimeException ignore) {
               // ignore
               try {
                  return new Props(tmpStr + fileSeparator + SettingPath);
               } catch (IORuntimeException ignore1) {
                  // ignore
               }
            }
         }
      } else {
         try {
            return new Props(spath[0]);
         } catch (IORuntimeException ignore) {
            // ignore
         }
      }
      return null;
   }
}
