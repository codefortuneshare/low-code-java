package com.pgl.ss.utils;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

/**
* @author zhangzihao
* @version 创建时间：2018年5月8日 下午5:26:01
* 类说明:
*/
public class GsonUtil {
	public static void main(String[] args) {
		String mapString = "{\"success\":1,\"msg\":\"获取成功\"}";
//		Map<String, Object> map = string2Map(mapString);
		@SuppressWarnings("unchecked")
		Map<String, Object> map = new Gson().fromJson(mapString,Map.class);
		System.out.println(""+(Boolean)map.get("success")+"---msg"+map.get("msg").toString());
	}
	
    public static String map2String(Map<String, Object> map) {  
        Gson gson2 = new GsonBuilder().enableComplexMapKeySerialization().create();  
        return gson2.toJson(map);  
    }  
    public static Map<String, Object> string2Map(String mapString) {  
        java.lang.reflect.Type type = new TypeToken<Map<String, Object>>() {}.getType();  
        Map<String, Object> map2 = new Gson().fromJson(mapString, type);  
        return map2;  
    }
    
    public static boolean isGoodJson(String json) {    
        if(json==null || "".equals(json)) {
        	return false; 
        }
 	   try {    
            JsonElement jsonElement =  new JsonParser().parse(json);  
            return jsonElement.isJsonObject();   
 	   } catch (JsonParseException e) {    
 	       System.out.println("bad json: " + json);    
 	       return false;    
 	   }    
 }
    /**
     * 格式化输出JSON字符串
     * @return 格式化后的JSON字符串
     */
    public static String toPrettyFormat(String json) {
    	JsonParser jsonParser = new JsonParser();
    	JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();
    	Gson gson = new GsonBuilder().setPrettyPrinting().create();
    	return gson.toJson(jsonObject);
    }
}
