package com.pgl.ss.utils;

import cn.hutool.core.util.XmlUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileUitl {
    private static Logger log = LoggerFactory.getLogger(FileUitl.class);
    private FileUitl() {}

    /**
     * List<?> 生成任意文件，支持保留中间XML
     * @param list
     * @param encoding
     * @param outPath
     * @param rootTag
     * @param childTag
     * @param xslPath
     * @param isXls3Grammer
     * @param origPath
     * @return
     * @throws Exception
     */
    public static String genFile(List<?> list, String origPath, String encoding, String outPath, String rootTag, String childTag, String xslPath, boolean isXls3Grammer) throws Exception {
        String oFileSuffix = outPath.substring(outPath.lastIndexOf(".") + 1);
        String defOutPath = outPath.substring(0, outPath.lastIndexOf(".") + 1) + "xml";
        String getPath = checkFile(new File(outPath)).getAbsolutePath();
        if (StringUtils.isNotEmpty(origPath)) {
            String defOrigPath = origPath.substring(0, origPath.lastIndexOf(".") + 1) + "xml";
            if (!isXls3Grammer) { //支持XLST2.0转xml
                if ("xml".equalsIgnoreCase(oFileSuffix)) {
                    XMLUtil.getXmlAndPathByList(list, defOrigPath, rootTag, childTag, encoding);
                    if (StringUtils.isNotEmpty(xslPath)) {
                        XMLUtil.transforXMLByXSLT(defOrigPath, xslPath, outPath);
                    }
                } else if ("json".equalsIgnoreCase(oFileSuffix)) {//支持XLST2.0转json
                    XMLUtil.getXmlAndPathByList(list, defOrigPath, rootTag, childTag, encoding);
                    if (StringUtils.isNotEmpty(xslPath)) {
                        XMLUtil.transforXMLByXSLT(defOrigPath, xslPath, defOutPath);
                    }
                    XMLUtil.XmlToFile(defOutPath, outPath, "json", encoding, true);
                }
            } else if (isXls3Grammer) {//支持XLST3.0转json、xml
                XMLUtil.getXmlAndPathByList(list, defOrigPath, rootTag, childTag, encoding);
                if (StringUtils.isNotEmpty(xslPath)) {
                    XMLUtil.transforXMLByXSLT(defOrigPath, xslPath, outPath);
                }
            }
        } else {
            if (!isXls3Grammer) {
                if ("xml".equalsIgnoreCase(oFileSuffix)) {
                    XMLUtil.getXmlAndPathByList(list, outPath, rootTag, childTag, encoding);
                    if (StringUtils.isNotEmpty(xslPath)) {
                        XMLUtil.transforXMLByXSLT(outPath, xslPath, outPath);//xml
                    }
                } else if ("json".equalsIgnoreCase(oFileSuffix)) {
                    XMLUtil.getXmlAndPathByList(list, defOutPath, rootTag, childTag, encoding);
                    if (StringUtils.isNotEmpty(xslPath)) {
                        XMLUtil.transforXMLByXSLT(defOutPath, xslPath, defOutPath); //xml
                    }
                    XMLUtil.XmlToFile(defOutPath, outPath, "json", encoding, true);
                }
            } else if (isXls3Grammer) {
                XMLUtil.getXmlAndPathByList(list, defOutPath, rootTag, childTag, encoding);
                if (StringUtils.isNotEmpty(xslPath)) {
                    XMLUtil.transforXMLByXSLT(defOutPath, xslPath, outPath); //xml、json
                }
            }
        }
        return getPath;
    }

    public static File checkFile(File from) {
        File directory = from.getParentFile();
        if (!directory.exists()) {
            if (directory.mkdirs()) {
                log.info("Created directory " + directory.getAbsolutePath());
            }
        }
        String[] fileInfo = getFileInfo(from);
        String toPrefix=fileInfo[0];
        String toSuffix=fileInfo[1];
        File newFile = new File(directory, toPrefix + toSuffix);
        if (newFile.exists()) {
            for (int i = 1; newFile.exists() && i < Integer.MAX_VALUE; i++) {
                newFile = new File(directory, toPrefix + '-' + i +toSuffix);
            }
            if (!from.renameTo(newFile)) {
                log.info("Couldn't rename file to " + newFile.getAbsolutePath());
                return from;
            }
        }
        return newFile;
    }

    public static String[] getFileInfo(File from){
        String fileName=from.getName();
        int index = fileName.lastIndexOf(".");
        String toPrefix="";
        String toSuffix="";
        if(index==-1){
            toPrefix=fileName;
        }else{
            toPrefix=fileName.substring(0,index);
            toSuffix=fileName.substring(index,fileName.length());
        }
        return new String[]{toPrefix,toSuffix};
    }
}
