package com.pgl.ss.utils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.hutool.core.util.XmlUtil;
import net.sf.saxon.s9api.*;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jetbrains.annotations.TestOnly;
import org.junit.Ignore;
import org.junit.Test;

import cn.hutool.json.JSONUtil;

import javax.xml.transform.stream.StreamSource;

import static com.pgl.ss.utils.XMLUtil.list2xml;

//import sun.security.util.Debug;
public class XMLUtilTest {


	@Test
	public void map2ExcelFileTest() throws Exception {
	   List<Map<String, Object>> list = new ArrayList<>();
	    HashMap<String, Object> map1 = new HashMap<String,Object>();
	    map1.put("name", "zhou");
	    map1.put("age", 20);
	    map1.put("Address", "hubei");
	    map1.put("career", "student");
	    list.add(map1);
	    HashMap<String, Object> map2 = new HashMap<String,Object>();
	    map2.put("name", "zhangsan");
	    map2.put("age", 30);
	    map2.put("Address", "wuhan");
	    map2.put("career", "teacher");
	    list.add(map2);
	    HashMap<String, Object> map3 = new HashMap<String,Object>();
	    map3.put("name", "zhangsan2");
	    map3.put("age", 30);
	    map3.put("Address", "wuhan2");
	    map3.put("career", "teacher2");
	    list.add(map3);
	    HashMap<String, Object> map4 = new HashMap<String,Object>();
	    map4.put("name", "zhangsan3");
	    map4.put("age", 30);
	    map4.put("Address", "wuhan3");
	    map4.put("career", "teacher3");
	    list.add(map4);
	    HashMap<String, Object> map5 = new HashMap<String,Object>();
	    map5.put("name", "zhangsan5");
	    map5.put("age", 30);
	    map5.put("Address", "wuhan5");
	    map5.put("career", "teacher5");
	    list.add(map5);
		XMLUtil.getXmlAndPathByList(list, "C:\\Users\\xuzihan\\Desktop\\DEBUG\\test.xml", "root", "test", "UTF-8");
	}


	@Test
	public void testData() {
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String outXmlPath = "D:\\Appliacation\\200-work\\220-XLS\\ASN-test\\";
		String fileExtension = outXmlPath.substring(outXmlPath.lastIndexOf("."));
		String newOutFilePath = outXmlPath.substring(outXmlPath.lastIndexOf(File.separator) + 1, outXmlPath.lastIndexOf("."))+sdf.format(dt)+fileExtension;
		System.out.println(newOutFilePath);
	}
}
