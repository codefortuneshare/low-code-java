package com.pgl.ss.utils;

import java.io.IOException;
import java.util.*;
//import java.util.Collection;
//import java.util.Map.Entry;

//import org.apache.poi.ss.usermodel.CellStyle;
//import org.apache.poi.ss.usermodel.FillPatternType;
//import org.apache.poi.ss.usermodel.Font;
//import org.apache.poi.ss.usermodel.IndexedColors;
//import org.apache.poi.ss.util.CellRangeAddressList;

//import javax.servlet.http.HttpServletResponse;

import cn.hutool.poi.excel.ExcelWriter;
import org.junit.Ignore;
import org.junit.Test;

//import cn.hutool.core.collection.CollUtil;
//import cn.hutool.poi.excel.ExcelUtil;
//import cn.hutool.poi.excel.ExcelWriter;

public class ExcelUtilTest {

	@Test
	public void map2ExcelFileTest() throws IOException {
        List<List<?>> dataList = new ArrayList<>();
        List<Map<String, Object>> list1 = new ArrayList<>();
        List<Map<String, Object>> list2 = new ArrayList<>();
        LinkedHashMap<String, Object> map12 = new LinkedHashMap<String,Object>();
        map12.put("name", "zhangsan");
        map12.put("age", 30);
        map12.put("Address", "wuhan");
        map12.put("career", "teacher");
        list1.add(map12);
        LinkedHashMap<String, Object> map13 = new LinkedHashMap<String,Object>();
        map13.put("name2", "zhangsan");
        map13.put("age1", 40);
        map13.put("Address2", "wuhan");
        map13.put("career2", "teacher");
        list2.add(map13);
        LinkedHashMap<String, Object> map14 = new LinkedHashMap<String,Object>();
        map14.put("name2", "zhangsan");
        map14.put("age1", 40);
        map14.put("Address2", "wuhan");
        map14.put("career2", "teacher");
        list2.add(map14);
        dataList.add(list1);
        dataList.add(list2);
        ExcelFileUtil.map2ExcelFile(dataList, new String[] {"苏州三星盘点报告","差异明细（差异清单)"}, "D:\\苏州三星盘点报告.xls");
	}


        @Test
        public void map2ExcelFileTest2() {
                List<Map<String, Object>> list = new ArrayList<>();
//    HttpServletResponse response=null;
                HashMap<String, Object> map1 = new HashMap<String,Object>();
                map1.put("name", "zhou");
                map1.put("age", 20);
                map1.put("Address", "hubei");
                map1.put("career", "student");
                list.add(map1);
                HashMap<String, Object> map2 = new HashMap<String,Object>();
                map2.put("name", "zhangsan");
                map2.put("age", 30);
                map2.put("Address", "wuhan");
                map2.put("career", "teacher");
                list.add(map2);
                HashMap<String, Object> map3 = new HashMap<String,Object>();
                map3.put("name", "zhangsan2");
                map3.put("age", 30);
                map3.put("Address", "wuhan2");
                map3.put("career", "teacher2");
                list.add(map3);
                HashMap<String, Object> map4 = new HashMap<String,Object>();
                map4.put("name", "zhangsan3");
                map4.put("age", 30);
                map4.put("Address", "wuhan3");
                map4.put("career", "teacher3");
                list.add(map4);
                HashMap<String, Object> map5 = new HashMap<String,Object>();
                map5.put("name", "zhangsan5");
                map5.put("age", 30);
                map5.put("Address", "wuhan5");
                map5.put("career", "teacher5");
                list.add(map5);
                ExcelFileUtil.map2ExcelFile(list, "D:/single_line.xlsx");
        }
}
