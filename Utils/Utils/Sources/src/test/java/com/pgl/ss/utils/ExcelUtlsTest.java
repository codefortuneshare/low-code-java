package com.pgl.ss.utils;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.*;

public class ExcelUtlsTest {

    @Test
    public void testConfigureExcel() throws IOException {
        Workbook wb =new XSSFWorkbook(new FileInputStream("D:\\single_line.xls"));
        //2.创建表格的sheet页
        Sheet sheet = wb.getSheet("sheet1");
        //3.创建行
        Row row = sheet.createRow(0);
        //4.1创建列
        Cell cell = row.createCell(0);
        //4.2设置列宽便于展示
        sheet.setColumnWidth(3, 10000);
        //5设置内容
        cell.setCellValue("有困难找度娘");
        //6.设置样式
        //6.1创建字体
        Font font = wb.createFont();
        font.setFontHeightInPoints((short) 36);
        font.setFontName("华文琥珀");

        font.setColor(Font.COLOR_RED);

        //6.2创建单元格格式CellStyle

        CellStyle cellStyle = wb.createCellStyle();

        cellStyle.setFont(font);

        //6.3字体作用单元格

        cell.setCellStyle(cellStyle);

        //7.写入到硬盘

        //7.1创建流

        FileOutputStream os = new FileOutputStream(new File("D:\\single_line2.xls"));

        //7.2将wb的内容写入字符流中

        wb.write(os);

        //7.3关流

        os.flush();

        os.close();
    }

    @Test
    public void testCreateSheetExcel() throws IOException {
        Workbook workbook = new HSSFWorkbook(new FileInputStream("D:\\123.xlsx"));

    }
}
