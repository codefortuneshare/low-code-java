package com.pgl.ss.utils;

//import org.apache.log4j.Logger;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoggerTest {
   
	@Test
	public void  Test() {
//		 TRACE < DEBUG < INFO < WARN < ERROR
		    Logger logger = LoggerFactory.getLogger("TEST");
//		    logger.setLevel(Level.ALL);
		    logger.info("testinfo");
		    logger.error("errmsg");
	        logger.debug("调试debug信息");
	        logger.info("普通Info信息");
	        logger.warn("警告warn信息");
	        logger.error("error信息");   
	}
}
