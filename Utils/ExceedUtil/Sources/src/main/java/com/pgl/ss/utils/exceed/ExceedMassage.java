package com.pgl.ss.utils.exceed;

import java.util.ArrayList;
import java.util.List;

public class ExceedMassage
{
  private String srcString;
  private Integer massageLength;
  private Integer rowsAffected;
  private String multiResult;
  private String resultCode;
  private List<String> resultParms;
  private String results;

  public static ExceedMassage parseReturnMsg(String srcString)
  {
//    String temp = "*";
    srcString = srcString.replace("`EOS", "");
    srcString = srcString.replace("`EOH", "*`EOH");
    ExceedMassage forteMassage = new ExceedMassage();
    String[] arraySrc = srcString.split("`EOH");
    forteMassage.setMassageLength(Integer.valueOf(arraySrc[0].replace("*", "")));
    forteMassage.setResults(arraySrc[(arraySrc.length - 1)].replace("*", ""));
    forteMassage.setRowsAffected(Integer.valueOf(arraySrc[1].replace("*", "")));
    forteMassage.setMultiResult(arraySrc[2].replace("*", ""));
    forteMassage.setResultCode(arraySrc[3].replace("*", ""));
    List<String> resParms = new ArrayList<String>();
    for (int i = 4; i < arraySrc.length - 1; i++) {
      resParms.add(arraySrc[i].replace("*", ""));
    }
    forteMassage.setResultParms(resParms);
    return forteMassage;
  }
  public String getSrcString() {
    return this.srcString;
  }
  public void setSrcString(String srcString) {
    this.srcString = srcString;
  }
  public Integer getMassageLength() {
    return this.massageLength;
  }
  public void setMassageLength(Integer massageLength) {
    this.massageLength = massageLength;
  }
  public Integer getRowsAffected() {
    return this.rowsAffected;
  }
  public void setRowsAffected(Integer rowsAffected) {
    this.rowsAffected = rowsAffected;
  }
  public String getMultiResult() {
    return this.multiResult;
  }
  public void setMultiResult(String multiResult) {
    this.multiResult = multiResult;
  }
  public String getResultCode() {
    return this.resultCode;
  }
  public void setResultCode(String resultCode) {
    this.resultCode = resultCode;
  }
  public List<String> getResultParms() {
    return this.resultParms;
  }
  public void setResultParms(List<String> resultParms) {
    this.resultParms = resultParms;
  }
  public String getResults() {
    return this.results;
  }
  public void setResults(String results) {
    this.results = results;
  }

  public String toString() {
    return "ForteMassage [massageLength=" + this.massageLength + ", rowsAffected=" + this.rowsAffected + ", multiResult=" + this.multiResult + ", resultCode=" + this.resultCode + ", resultParms=" + this.resultParms + ", results=" + this.results + "]";
  }
}
