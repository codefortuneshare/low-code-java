package com.pgl.ss.utils.exceed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceedConnector {

    private static Logger logger = LoggerFactory.getLogger(ExceedConnector.class);
    private String codePage = "GB2312";
    //  private static final String USENAME = "exe";
    public static final String SEPARATOR = ";";
    public static final String FORTE_SPACE = "`EOH";
    public static final String FORTE_SEPAR = "`EOP";
    public static final String FORTE_END = "`EOS";
    //  private Logger logger = LoggerFactory.getLogger(ForteSocketConfig.class);
    public static final String DBResourceException = "FT_9998";
    public static final String FulfillLogicException = "FT_9997";
    public static final String FulfillSystemException = "FT_9999";

    private String dbName;
    private String ip;
    private Integer port;
    private Integer timeout = 500000;
    private String userName = "exe";

    /****  add by lzw   2021-07-08   */
    /****  分离登录与调用，使调用时能通过一次登录后，可执行多次调用   */
    private static int i = 150000;
    private static String lastIp = null;
    private static String lastDbName = null;
    private static Socket socket = null;
    private static boolean isLogin = false;
    private static PrintWriter localPrintWriter = null;
    private static BufferedReader localBufferedReader = null;
    /****  lzw  end   */

    public ExceedConnector(String ip, Integer port, String dbName) {
        this.dbName = dbName;
        this.ip = ip;
        this.port = port;
        this.init();
    }

    public ExceedConnector(String ip, Integer port, String userName, String dbName, Integer timeout, String codepage) {
        this.dbName = dbName;
        this.userName = userName;
        this.ip = ip;
        this.port = port;
        this.timeout = timeout;
        this.setCodePage(codepage);
        this.init();
    }

    public ExceedConnector(String ip, Integer port, String dbName, Integer timeout) {
        this.dbName = dbName;
        this.ip = ip;
        this.port = port;
        this.timeout = timeout;
        this.init();
    }

    /****  初始化socket   add by lzw   2021-07-08   */
    private void init() {
        //新ip？尝试释放旧连接
        if(!ip.equals(lastIp)){
            this.close();
            lastIp = this.ip;
        }
        if (socket == null) {
            try {
                isLogin = false;
                socket = new Socket(this.ip, this.port.intValue());
                socket.setKeepAlive(true);//心跳检测
                if ((this.timeout != null) && (this.timeout.intValue() > 0)) {
                    socket.setSoTimeout(this.timeout.intValue());
                }
                localBufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), codePage), i);
                localPrintWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), codePage), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(!dbName.equals(lastDbName)){
            isLogin = false;
            lastDbName = this.dbName;
        }
    }

    /****  关闭socket连接   add by lzw   2021-07-08   */
    private void close() {
        try {
            isLogin = false;
            if (localPrintWriter != null) localPrintWriter.close();
            if (localBufferedReader != null) localBufferedReader.close();
            if (socket != null) socket.close();
            socket = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /****  登录forte   add by lzw   2021-07-08   */
    private void login(String loginParam) throws IOException {
        if (!isLogin) {
            char[] arrayOfChar = new char[i];
            StringBuilder loginReturnSB = new StringBuilder();
            logger.debug(new StringBuilder().append("登录信息：").append(loginParam).toString());
            localPrintWriter.println(loginParam);//请求
            while (loginReturnSB.indexOf(FORTE_END) == -1) {
                int j = localBufferedReader.read(arrayOfChar, 0, i);//接收
                loginReturnSB.append(arrayOfChar, 0, j);
                arrayOfChar = new char[i];
            }
            logger.debug(new StringBuilder().append("登录返回：").append(loginReturnSB.toString()).toString());
            isLogin = true;
        }
    }

    /****  调用forte   add by lzw   2021-07-08   */
    private String callfunc(String paramString) throws IOException {
        char[] arrayOfChar = new char[i];
        StringBuilder operationReturnSB = new StringBuilder();
        logger.debug(new StringBuilder().append("业务消息：").append(paramString).toString());
        localPrintWriter.println(paramString);//请求
        while (operationReturnSB.indexOf(FORTE_END) == -1) {
            int j = localBufferedReader.read(arrayOfChar, 0, i);//接收
            operationReturnSB.append(arrayOfChar, 0, j);
            arrayOfChar = new char[i];
        }
        logger.debug(new StringBuilder().append("业务返回：").append(operationReturnSB.toString()).toString());
        return operationReturnSB.toString();
    }

    /****  重连socket、重新登录forte、发送业务信息   add by lzw   2021-07-08   */
    private MessageReturn reSend(String paramString, String loginParam) {
        MessageReturn forteReturn = new MessageReturn();
        forteReturn.setCode(Integer.valueOf(200));
        logger.debug("尝试重新发送！");
        forteReturn.setAccessMessage("尝试重新发送！");
        this.close();
        this.init();
        try {
            this.login(loginParam);
            String msg = this.callfunc(paramString);
            forteReturn.setOperateMessageSrc(msg);
        } catch (IOException localIOException) {
            //重新发送失败，关闭socket连接，返回错误信息
            logger.error(new StringBuilder().append("ERROR: Failed I/O:").append(localIOException.getMessage()).toString());
            forteReturn.setCode(Integer.valueOf(404));
            forteReturn.setAccessMessage(new StringBuilder().append("ERROR: Failed I/O:").append(localIOException.getMessage()).toString());
            forteReturn.setOperateMessageSrc(new StringBuilder().append("0`EOH0`EOH ").append(localIOException.getMessage()).append(" `EOH0`EOH`EOS").toString());
            this.close();
        }
        return forteReturn;
    }

    /****  分离登录与调用，使调用时能通过一次登录后，可执行多次调用
     *     调用失败时尝试重新调用一次：重置socket、重新登录forte、发送业务信息
     *     update by lzw   2021-07-08
     * */
    public MessageReturn send(String paramString, String loginParam) {
        MessageReturn forteReturn = new MessageReturn();
        forteReturn.setCode(Integer.valueOf(200));
        forteReturn.setAccessMessage("初始化成功！");
        try {
            this.login(loginParam);
            String msg = this.callfunc(paramString);
            forteReturn.setOperateMessageSrc(msg);
        } catch (IOException localIOException) {
            //调用失败时尝试重新调用一次：重置socket、重新登录forte、发送业务信息
            forteReturn = this.reSend(paramString, loginParam);
        }
        return forteReturn;
    }

    public MessageReturn sendXML(String paramXML) {
        return send(paramXML, getSALoginParam());
    }

    public MessageReturn send(String message) {
        return send(message, getLoginParam());
    }

    private String getLoginParam() {
        String login = new StringBuilder().append("LOGIN ").append(userName).append("$").append(this.dbName).append("$zh_cn$yyyy-M-d H:mm:ss").toString();
        login = new StringBuilder().append(FORTE_SPACE).append("exe").append(FORTE_SPACE).append(login).append(FORTE_SEPAR).append(FORTE_END).toString();
        login = new StringBuilder().append(login.length()).append(login).toString();
        return login;
    }

    private String getSALoginParam() {
        String login = new StringBuilder().append("SA").append(FORTE_SPACE).append("LOGIN ").append(userName).append("$").append(this.dbName).append(FORTE_SEPAR).toString();
        login = new StringBuilder().append(login.length()).append(FORTE_SPACE).append(login).append(FORTE_END).toString();
        return login;
    }

    public String packageParam(String param) {
        if (!param.endsWith(SEPARATOR)) {
            param = new StringBuilder().append(param).append(SEPARATOR).toString();
        }
        param = new StringBuilder().append(FORTE_SPACE).append(userName).append(FORTE_SPACE).append(param.replace(SEPARATOR, FORTE_SEPAR)).toString();
        param = new StringBuilder().append(param.length()).append(param).append(FORTE_END).toString();
        return param;
    }

    public String packageParamProc(String theprocname, String[] param) {
        String spr = ",";
        StringBuilder sb_param = new StringBuilder();
        sb_param.append("EXECUTE");
        sb_param.append(" ");
        sb_param.append(theprocname);
        sb_param.append(" ");
        for (int i = 0; (param != null) && (i < param.length); i++) {
            sb_param.append(param[i]);
            sb_param.append(spr);
        }
//     sb_param.delete(sb_param.length() - 1, sb_param.length());
        return packageParam(sb_param.toString());
    }

    public String packageParamSQL(String theSQLstr) {
        StringBuilder sb_param = new StringBuilder();
        sb_param.append(theSQLstr);
//     sb_param.delete(sb_param.length() - 1, sb_param.length());
        return packageParam(sb_param.toString());
    }

    public String getDbName() {
        return this.dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getTimeout() {
        return this.timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public Map<String, String> RunProc(ExecutorService executorService, String theProcName, String[] params) {
        Map<String, String> result = new HashMap<>();
        String sInfo = "";
        MessageReturn forteReturn = null;
        Future<?> ft = null;
        ExceedThreadCallBack FSThreadCallBack = new ExceedThreadCallBack(this);
        String requestParam = this.packageParamProc(theProcName, params);
        sInfo = new StringBuilder().append("请求的参数 ").append(requestParam).toString();
//	    logger.debug(sInfo);
        FSThreadCallBack.setSendMessage(requestParam);
        try {
            ft = executorService.submit(FSThreadCallBack);
        } catch (RejectedExecutionException e) {
            result.put("code", "7");
            result.put("msg", "请求被拒绝:" + sInfo);
            return result;
        }
        try {
            forteReturn = (MessageReturn) ft.get(this.timeout, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException | InterruptedException | ExecutionException | TimeoutException e) {
            result.put("code", "7");
            result.put("msg", "执行错误:" + sInfo);
            return result;
        }
        if (forteReturn == null) {
            result.put("code", "7");
            result.put("msg", "调用失败无返回值:" + sInfo);
            return result;
        }
        String operateMessageSrc = forteReturn.getOperateMessageSrc();
        sInfo = new StringBuilder().append("执行结果 ").append(operateMessageSrc).toString();
//	    logger.debug(sInfo);
        ExceedMassage forteBackMassage = ExceedMassage.parseReturnMsg(operateMessageSrc);
        if (!forteBackMassage.getResultCode().trim().isEmpty()) {
            result.put("code", "7");
            result.put("msg", "调用失败:" + sInfo);
            return result;
        }
        result.put("code", "9");
        result.put("msg", "调用成功:" + sInfo);
        return result;
    }

    public String getCodePage() {
        return codePage;
    }

    public void setCodePage(String codePage) {
        this.codePage = codePage;
    }

    public Map<String, String> RunSQL(ExecutorService executorService, String sqlstr) {
        Map<String, String> result = new HashMap<>();
        String sInfo = "";
        MessageReturn forteReturn = null;
        Future<?> ft = null;
        ExceedThreadCallBack FSThreadCallBack = new ExceedThreadCallBack(this);
        String requestParam = this.packageParamSQL(sqlstr);
        sInfo = new StringBuilder().append("请求的参数 ").append(requestParam).toString();
//    logger.debug(sInfo);
        FSThreadCallBack.setSendMessage(requestParam);
        try {
            ft = executorService.submit(FSThreadCallBack);
        } catch (RejectedExecutionException e) {
            result.put("code", "7");
            result.put("msg", "请求被拒绝:" + sInfo);
            return result;
        }
        try {
            forteReturn = (MessageReturn) ft.get(this.timeout, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException | InterruptedException | ExecutionException | TimeoutException e) {
            result.put("code", "7");
            result.put("msg", "执行错误:" + sInfo);
            return result;
        }
        if (forteReturn == null) {
            result.put("code", "7");
            result.put("msg", "调用失败无返回值:" + sInfo);
            return result;
        }
        String operateMessageSrc = forteReturn.getOperateMessageSrc();
        sInfo = new StringBuilder().append("执行结果 ").append(operateMessageSrc).toString();
        sInfo = sInfo
                .replace(FORTE_SPACE, "")
                .replace(FORTE_SEPAR, "")
                .replace(FORTE_END, "")
                .replace(DBResourceException, "")
                .replace(FulfillLogicException, "")
                .replace(FulfillSystemException, "");
//    public static final String FORTE_SPACE = "`EOH";
//    public static final String FORTE_SEPAR = "`EOP";
//    public static final String FORTE_END = "`EOS";
//  //  private Logger logger = LoggerFactory.getLogger(ForteSocketConfig.class);
//    public static final String DBResourceException = "FT_9998";
//    public static final String FulfillLogicException = "FT_9997";
//    public static final String FulfillSystemException = "FT_9999";

//    logger.debug(sInfo);
        ExceedMassage forteBackMassage = ExceedMassage.parseReturnMsg(operateMessageSrc);
        if (!forteBackMassage.getResultCode().trim().isEmpty()) {
            result.put("code", "7");
            result.put("msg", "调用失败:" + sInfo);
            return result;
        }
        result.put("code", "9");
        result.put("msg", "调用成功:" + sInfo);
        return result;
    }

}
