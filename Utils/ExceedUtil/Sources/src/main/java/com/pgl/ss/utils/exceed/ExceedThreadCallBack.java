package com.pgl.ss.utils.exceed;

import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceedThreadCallBack
  implements Callable<MessageReturn>
{
  private static Logger logger = LoggerFactory.getLogger(ExceedThread.class);

  private ExceedConnector exceedSPRunner = null;
//  private ExceedSQLRunner exceedSQLRunner = null;
  private String sendMessage = null;

  public ExceedThreadCallBack(ExceedConnector exceedSPRunner) {
    this(exceedSPRunner, null);
  }
  public ExceedThreadCallBack(ExceedConnector exceedSPRunner, String sendMessage) {
    this.exceedSPRunner = exceedSPRunner;
    this.sendMessage = sendMessage;
  }

  
  public MessageReturn call() throws Exception {
//    MessageReturn forteReturn = null;
    if ((this.sendMessage == null) || (this.sendMessage.trim().isEmpty())) {
      logger.error("发送数据为空，异常");
      return null;
    }
    if (this.exceedSPRunner == null) {
      logger.error("未设置forteSocke");
      return null;
    }
    System.out.println(this.sendMessage);
    return this.exceedSPRunner.send(this.sendMessage);
//    return forteReturn;
  }
  public ExceedConnector exceedSPRunner() {
    return this.exceedSPRunner;
  }
  public void setExceedSPRunner(ExceedConnector exceedSPRunner) {
    this.exceedSPRunner = exceedSPRunner;
  }

  public String getSendMessage() {
    return this.sendMessage;
  }
  public void setSendMessage(String sendMessage) {
    this.sendMessage = sendMessage;
  }
}
