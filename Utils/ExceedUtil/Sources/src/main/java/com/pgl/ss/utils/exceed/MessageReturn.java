package com.pgl.ss.utils.exceed;

public class MessageReturn
{
  private Integer code;
  private String accessMessage;
  private Boolean iSlogin;
  private String loginMessageSrc;
  private ExceedMassage loginMessage;
  private String OperateMessageSrc;
  private ExceedMassage OperateMessage;

  
  public Integer getCode()
  {
    return this.code;
  }
  public void setCode(Integer code) {
    this.code = code;
  }
  public String getAccessMessage() {
    return this.accessMessage;
  }
  public void setAccessMessage(String accessMessage) {
    this.accessMessage = accessMessage;
  }
  public Boolean getiSlogin() {
    return this.iSlogin;
  }
  public void setiSlogin(Boolean iSlogin) {
    this.iSlogin = iSlogin;
  }
  public String getLoginMessageSrc() {
    return this.loginMessageSrc;
  }
  public void setLoginMessageSrc(String loginMessageSrc) {
    this.loginMessageSrc = loginMessageSrc;
  }
  public ExceedMassage getLoginMessage() {
    return this.loginMessage;
  }
  public void setLoginMessage(ExceedMassage loginMessage) {
    this.loginMessage = loginMessage;
  }
  public String getOperateMessageSrc() {
    return this.OperateMessageSrc;
  }
  public void setOperateMessageSrc(String operateMessageSrc) {
    this.OperateMessageSrc = operateMessageSrc;
  }
  public ExceedMassage getOperateMessage() {
    return this.OperateMessage;
  }
  public void setOperateMessage(ExceedMassage operateMessage) {
    this.OperateMessage = operateMessage;
  }

  public String toString() {
    return "ForteReturn [code=" + this.code + ", accessMessage=" + this.accessMessage + ", iSlogin=" + this.iSlogin + ", loginMessageSrc=" + this.loginMessageSrc + ", loginMessage=" + this.loginMessage + ", OperateMessageSrc=" + this.OperateMessageSrc + ", OperateMessage=" + this.OperateMessage + "]";
  }
}
