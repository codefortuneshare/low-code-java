package com.pgl.ss.utils.exceed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceedThread
  implements Runnable
{
  private static Logger logger = LoggerFactory.getLogger(ExceedThread.class);
//  private static final int SUCCESSCODE = 9;
//  private static final int FAILECODE = 7;
  private ExceedConnector forteSocket = null;
  private String sendMessage = null;
  private ForteSocketBackListen callBacklisten = null;
  private volatile MessageReturn forteReturn = null;

  private volatile Boolean callOver = Boolean.valueOf(false);
  private Long stratData;
  private long timeOut;

  public ExceedThread(ExceedConnector forteSocket)
  {
    this(forteSocket, null, null);
  }
  public ExceedThread(ExceedConnector forteSocket, String sendMessage) {
    this(forteSocket, sendMessage, null);
  }
  public ExceedThread(ExceedConnector forteSocket, String sendMessage, ForteSocketBackListen callBacklisten) {
    this.forteSocket = forteSocket;
    this.sendMessage = sendMessage;
    this.callBacklisten = callBacklisten;
    this.timeOut = forteSocket.getTimeout().intValue();
  }

  public void run()
  {
    try {
      this.stratData = Long.valueOf(System.currentTimeMillis());
      if ((this.sendMessage == null) || (this.sendMessage.trim().isEmpty())) {
        logger.error("发送数据为空，异常");
        this.callBacklisten.callBack(7, "发送数据为空，异常", null);
        return;
      }
      if (this.forteSocket == null) {
        logger.error("未设置forteSocke");
        this.callBacklisten.callBack(7, "未设置forteSocket", null);
        return;
      }
      this.forteReturn = this.forteSocket.send(this.sendMessage);
      if (this.callBacklisten != null)
        this.callBacklisten.callBack(9, "", this.forteReturn);
    }
    finally {
      this.callOver = Boolean.valueOf(true);
    }
  }

  public boolean isCallOver() { return this.callOver.booleanValue(); }

  public boolean isCallOutTime() {
    if (this.stratData == null) {
      this.stratData = Long.valueOf(System.currentTimeMillis());
    }
    return this.stratData.longValue() + this.timeOut < System.currentTimeMillis();
  }

  public MessageReturn getForteReturn() {
    return this.forteReturn;
  }
  public void setForteReturn(MessageReturn forteReturn) {
    this.forteReturn = forteReturn;
  }
  public ExceedConnector getForteSocket() {
    return this.forteSocket;
  }
  public void setForteSocket(ExceedConnector forteSocket) {
    this.forteSocket = forteSocket;
  }
  public String getSendMessage() {
    return this.sendMessage;
  }
  public void setSendMessage(String sendMessage) {
    this.sendMessage = sendMessage;
  }
  public ForteSocketBackListen getCallBacklisten() {
    return this.callBacklisten;
  }
  public void setCallBacklisten(ForteSocketBackListen callBacklisten) {
    this.callBacklisten = callBacklisten;
  }

  public static abstract interface ForteSocketBackListen
  {
    public abstract void callBack(int paramInt, String paramString, MessageReturn paramForteReturn);
  }
}
