package com.pgl.ss.utils.exceed;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
//import java.util.concurrent.ThreadPoolExecutor.AbortPolicy;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import com.pgl.ss.utils.exceed.ExceedConnector;

import cn.hutool.core.io.IORuntimeException;
import cn.hutool.setting.dialect.Props;

@Configuration
public class ExceedUtil {
	static ExecutorService executorService = null;
	/**
	 * 
	 */
	private static final Map<String, String> FunctionPattern = new HashMap<String, String>() {
		/* 
		 * 当前支持功能
		 * 分配订单，功能名称：ALLOCATEORD，参数：Orderkey 
		 * 发运拣货行，功能名称：SHIPPICK，参数：Pickdetailkey
		 * 库位预约，功能名称：RECEIPTASNLINE，参数：receiptkey,receiptlinenumber
		 * 库位预约，功能名称：ASNBOOKING，参数：receiptkey
		 * 执行库存转换，功能名称：TRANSFERFINALIZE，参数：TransferKey,TransferLineNumber
		 * 完成任务，功能名称：TASKCOMPLETE，参数：TaskDetailKey
		 * 计算上架，功能名称：ASNCALCPUTAWAY，参数：receiptkey 
		 * 生成托盘号，功能名称：ASNGENERATEID，参数：receiptkey
		 * 确认上架，功能名称：ASNCONFIRMPUTAWAY，参数：receiptkey 
		 * 发运订单，功能名称：SHIPORD，参数：orderkey
		 * 发布上架任务，功能名称：RELEASEPATASK，参数：receiptkey,storerkey
		 * 反分配订单行，功能名称：UNALLOCORDERLINE，参数：ORDERKEY,ORDERLINENUMBER 
		 * 按托盘做库存保留（生成新Hold货记录） ，功能名称：HOLDBYID，参数：status（原因，可查codelkup code=RECHOLD）,ID
		 * 按库位做库存保留（生成新Hold货记录），功能名称：HOLDBYLOC，参数：status（原因，可查codelkup code=RECHOLD）,LOC
		 * 按系统批次做库存保留（生成新Hold货记录），功能名称：HOLDBYLOT，参数：status（原因，可查codelkup code=RECHOLD）,LOT 
		 * 按托盘做库存保留（通过Hold货记录），功能名称：HOLDBYIDWITHKEY，参数：status（原因，可查codelkup code=RECHOLD）,ID
		 * 按库位做库存保留（通过Hold货记录），功能名称：HOLDBYLOCWITHKEY，参数：status（原因，可查codelkup code=RECHOLD）,LOC
		 * 按系统批次做库存保留（通过Hold货记录），功能名称：HOLDBYLOTWITHKEY，参数：status（原因，可查codelkup	code=RECHOLD）,LOT 
		 * 按托盘解除库存保留（生成新Hold货记录），功能名称：UNHOLDBYID，参数：* status（原因，可查codelkup code=RECHOLD）,ID
		 * 按库位解除库存保留（生成新Hold货记录），功能名称：UNHOLDBYLOC，参数： status（原因，可查codelkup code=RECHOLD）,LOC
		 * 按系统批次解除库存保留（生成新Hold货记录） ，功能名称：UNHOLDBYLOT，参数： status（原因，可查codelkup code=RECHOLD）,LOT 
		 * 按托盘解除库存保留（通过Hold货记录），功能名称：UNHOLDBYIDWITHKEY，参数： status（原因，可查codelkup code=RECHOLD）,ID 
		 * 按库位解除库存保留（通过Hold货记录），功能名称：UNHOLDBYLOCWITHKEY，参数： status（原因，可查codelkup code=RECHOLD）,LOC
		 * 按系统批次解除库存保留（通过Hold货记录），功能名称：UNHOLDBYLOTWITHKEY，参数： status（原因，可查codelkup code=RECHOLD）,LOT
		 * 库存移位，功能名称：MOVE2，参数： fromloc,Storerkey（可留空）,Sku（可留空）,fromid（可留空）,toloc
		 */
		private static final long serialVersionUID = 1L;
		{
			/*
				 wavekey，波次号（可选，若为空则生成一个新的）
				 oskey，波次生成条件代码（对应Orderselection表内容）
				 statusgroup，状态组（可选，若为空则不引用，该内容为sql语句中where字句的Like关键字引用内容，因此需用到SQL中的通配符)
				  extview，视图名称（可选，默认为空）
			* */
			put("BUILDWAVEP2S1", "EXECUTE BUILDWAVEP2S1 {0},{1},{2},{3}");
			put("ALLOCATEORD", "EXECUTE nsporderprocessing {0},,Y,N,,N"); // 分配订单 Orderkey
			put("SHIPPICK", "UPDATE PICKDETAIL SET status = ''9'' WHERE pickdetailkey = ''{0}'' "); // 发运拣货行
																									// Pickdetailkey
			put("RECEIPTASNLINE",
					"UPDATE RECEIPTDETAIL SET status= ''9'', qtyreceived = qtyexpected WHERE receiptkey=''{0}'' AND receiptlinenumber=''{1}''"); // 按行全收
			// receiptkey,receiptlinenumber
			put("RECEIPTASNLINE2",
					"EXECUTE NSPRFRCC001 ,,,,,,,,{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34}"); // 按行全收
//						,,,,,,,,receiptkey(必录),storerkey,lot,prokey(无意义),sku(无意义),pokey(无意义),qty,uom(无意义),packkey(无意义),loc(无意义),id,hold(无意义),isrp,drid,lottable01,lottable02,lottable03,lottable04,lottable05,lottable06,lottable07,lottable08,lottable09,lottable10,other1,other2,other3,printerID,counter,wgt,reasoncode,RejectQty,Receiptlinenumber(必录),Currentpalletid,newpalletid
//			"EXECUTE NSPRFRCC001 ,,,,,,,,*{0},,{2},,,,{6},,,,{10},,{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},*{32},{33},{34}"); // 按行全收
//						,,,,,,,,receiptkey(必录) 0,storerkey(必录) 1,lot 2,prokey(必录) 3,sku(必录) 4,pokey(无意义) 5,qty 6,uom(无意义) 7,packkey(无意义) 8,loc(无意义) 9,id 10,hold(无意义) 11,isrp 12,drid 13,lottable01 14,lottable02 15,lottable03 16,lottable04 17,lottable05 18,lottable06 19,lottable07 20,lottable08 21,lottable09 22,lottable10 23,other1 24,other2 25,other3 26,printerID 27,counter 28,wgt 29,reasoncode 30,RejectQty 31,Receiptlinenumber(必录) 32,Currentpalletid 33,newpalletid 34

			put("ASNBOOKING", "EXECUTE nspbookingslocation {0}"); // 库位预约 receiptkey
			put("TRANSFERFINALIZE",
					"UPDATE TRANSFERDETAIL SET Status = ''9'' WHERE TransferKey = ''{0}'' AND TransferLineNumber = ''{1}''"); // 执行库存转换
																																// TransferKey,TransferLineNumber
			put("TASKCOMPLETE", "UPDATE TaskDetail SET Status = ''9'' WHERE TaskDetailKey = ''{0}''"); // 完成任务
																										// TaskDetailKey
			put("ASNCALCPUTAWAY", "EXECUTE nspcalculateputaway {0}"); // 计算上架 receiptkey
			put("ASNGENERATEID", "EXECUTE NSPGENERATEID {0}"); // 生成托盘号 receiptkey
			put("ASNCONFIRMPUTAWAY", "EXECUTE NSPCONFIRMPUTAWAY {0}"); // 确认上架 receiptkey
			put("SHIPORD", "EXECUTE NSPMASSSHIPORDERS {0}"); // 发运订单 orderkey
			put("RELEASEPATASK", "EXECUTE RELEASEPATASK {0},{1}"); // 发布上架任务 receiptkey,storerkey
			put("UNALLOCORDERLINE", "EXECUTE UNALLOCORDERS {0},{1}"); // 反分配订单行 ORDERKEY,ORDERLINENUMBER
			put("HOLDBYID", "EXECUTE NSPHOLD  ,1,{0},,{1},"); // 按托盘做库存保留（生成新Hold货记录） status（原因，可查codelkup code=RECHOLD）,ID
			put("HOLDBYLOC", "EXECUTE NSPHOLD  ,1,{0},{1},,"); // 按库位做库存保留（生成新Hold货记录） status（原因，可查codelkup
															// code=RECHOLD）,LOC
			put("HOLDBYLOT", "EXECUTE NSPHOLD  ,1,{0},,,{1}"); // 按系统批次做库存保留（生成新Hold货记录） status（原因，可查codelkup
															// code=RECHOLD）,LOT
			put("HOLDBYIDWITHKEY", "EXECUTE NSPHOLD  {0},1,{1},,{2},"); // 按托盘做库存保留（通过Hold货记录） status（原因，可查codelkup listname=RECHOLD）,ID
			put("HOLDBYLOCWITHKEY", "EXECUTE NSPHOLD  {0},1,{1},{2},,"); // 按库位做库存保留（通过Hold货记录） status（原因，可查codelkup listname=RECHOLD）,LOC
			put("HOLDBYLOTWITHKEY", "EXECUTE NSPHOLD  {0},1,{1},,,{2}"); // 按系统批次做库存保留（通过Hold货记录） status（原因，可查codelkup listname=RECHOLD）,LOT
			put("UNHOLDBYID", "EXECUTE NSPHOLD  ,0,{0},,{1},"); // 按托盘解除库存保留（生成新Hold货记录） status（原因，可查codelkup listname=RECHOLD）,ID
			put("UNHOLDBYLOC", "EXECUTE NSPHOLD  ,0,{0},{1},,"); // 按库位解除库存保留（生成新Hold货记录） status（原因，可查codelkup listname=RECHOLD）,LOC
			put("UNHOLDBYLOT", "EXECUTE NSPHOLD  ,0,{0},,,{1}"); // 按系统批次解除库存保留（生成新Hold货记录） status（原因，可查codelkup listname=RECHOLD）,LOT
			put("UNHOLDBYIDWITHKEY", "EXECUTE NSPHOLD  {0},0,{1},,{2},"); // 按托盘解除库存保留（通过Hold货记录） status（原因，可查codelkup listname=RECHOLD）,ID
			put("UNHOLDBYLOCWITHKEY", "EXECUTE NSPHOLD  {0},0,{1},{2},,"); // 按库位解除库存保留（通过Hold货记录） status（原因，可查codelkup listname=RECHOLD）,LOC
			put("UNHOLDBYLOTWITHKEY", "EXECUTE NSPHOLD  {0},0,{1},,,{2}"); // 按系统批次解除库存保留（通过Hold货记录） status（原因，可查codelkup listname=RECHOLD）,LOT
			put("MOVE2", "EXECUTE MOVE2 {0},{1},{2},{3},{4},{5},{6},{7},,{8},{9}"); // 库存移位 fromloc,Storerkey（可留空）,Sku（可留空）,fromid（可留空）,toloc,toid（可留空）
																			// ,uomqty（可留空）,uom（可留空）,packkey（可留空）,refnum（可留空）,sourcetype（可留空）
			put("MOVE3", "EXECUTE MOVE3 {0},{1},{2},{3},{4},{5},{6}"); // 库存移位 fromloc(源库位）,Storerkey（货主,可留空）,Sku（商品,可留空）,fromid（源托盘号,可留空）,
																			// toloc(目标库位）,refnum（单号，可留空）,sourcetype（引用类型,可留空）
			put("RELEASEWAVE", "EXECUTE NSPRELEASEWAVE {0},{1},{2}"); // 按波次分配、发布 success,err,DoRelease  （DoRelease='N' 表示只做分配  'Y'表示分配和发布）
			put("UNBATCHORDER", "EXECUTE UNBATCHORDER {0}"); // 取消波次 Wavekey
			put("GETDATALIST", "EXECUTE GETDATALIST2 ,,{0},{1},{2},{3},{4},,,,,,,,,,,{5},{6},{7},{8},"); // 获取数据行 userid（用户名，若留空则不检查用户权限）,area(区域，可留空）,
			                                                                                            // param1（查询参数1，若留空或‘NA’内容，则忽略）,param2（查询参数2，若留空或‘NA’内容，则忽略）,
			                                                                                            // param3（查询参数3，若留空或‘NA’内容，则忽略）,param4（查询参数2，若留空或‘NA’内容，则忽略）,
			                                                                                            // param5（查询参数5，若留空或‘NA’内容，则忽略）,viewname（视图名称）,
			                                                                                            // porderby（排序设置，若空则不处理，若有内容则按两两一组，排序字段‘O_’+0或1处理）,
			                                                                                            // tasktype（任务类型，若留空则不检查用户权限）,tmfunc (解锁时间（tmfunc=1时查询视图loctime必须存在）)
			                                                                                            // userid,area,param1,param2,param3,param4,param5,param6_1,param6_2,param6_3,param6_4,param6_5,opr_1,opr_2,opr_3,opr_4,opr_5,viewname,orderby,tasktype,tmfunc,react

			put("GETDATASET", "EXECUTE GETDATASET2 ,,{0},{1},{2},{3},{4},,,,,,,,,,,{5},{6},{7},{8},");  // 获取数据行 userid（用户名，若留空则不检查用户权限）,area(区域，可留空）,
																							           // param1（查询参数1，若留空或‘NA’内容，则忽略）,param2（查询参数2，若留空或‘NA’内容，则忽略）,
																							           // param3（查询参数3，若留空或‘NA’内容，则忽略）,param4（查询参数2，若留空或‘NA’内容，则忽略）,
																							           // param5（查询参数5，若留空或‘NA’内容，则忽略）,viewname（视图名称）,
																							           // porderby（排序设置，若空则不处理，若有内容则按两两一组，排序字段‘O_’+0或1处理）,
																							           // tasktype（任务类型，若留空则不检查用户权限）,tmfunc (解锁时间（tmfunc=1时查询视图loctime必须存在）)
																							           // userid,area,param1,param2,param3,param4,param5,param6_1,param6_2,param6_3,param6_4,param6_5,opr_1,opr_2,opr_3,opr_4,opr_5,viewname,orderby,tasktype,tmfunc,react

			put("GETDATALIST2", "EXECUTE GETDATALIST {0},{1},{2},{3},{4},{5},{6},{7}"); // 获取数据列 userid（0.用户名，若留空则不检查用户权限）,area(1.区域，可留空）,viewname（视图名称）,
			// param1（2.查询参数1，若留空或‘NA’内容，则忽略）,param2（3.查询参数2，若留空或‘NA’内容，则忽略）,
			// param3（4.查询参数3，若留空或‘NA’内容，则忽略）,param4（5.查询参数4，若留空或‘NA’内容，则忽略）,
			// param5（6.查询参数5，若留空或‘NA’内容，则忽略）,param6_1（7.查询参数6_1，若留空或‘NA’内容，则忽略）,			put("BATCHORDER", "EXECUTE BATCHORDER {0},{1}"); // 建立波次 Wavekey,BatchSelectionkey
			// param6_2（8.查询参数6_2，若留空或‘NA’内容，则忽略）,param6_3（9.查询参数6_3，若留空或‘NA’内容，则忽略）,
			// param6_4（10.查询参数6_4，若留空或‘NA’内容，则忽略）,param6_5（11.查询参数6_5，若留空或‘NA’内容，则忽略）,
			// opr_1 (12.查询参数1操作符）,opr_2 (13.查询参数1操作符）,opr_3 (14.查询参数1操作符）,opr_4 (15.查询参数1操作符）,opr_5 (16.查询参数1操作符）

			// porderby（排序设置，若空则不处理，若有内容则按两两一组，排序字段‘O_’+0或1处理）,
			// tasktype（任务类型，若留空则不检查用户权限）,tmfunc (解锁时间（tmfunc=1时查询视图loctime必须存在）)
			put("GETDATASET2", "EXECUTE GETDATASET {0},{1},{2},{3},{4},{5},{6},{7}"); // 获取数据行 userid（用户名，若留空则不检查用户权限）,area(区域，可留空）,viewname（视图名称）,
			// param1（查询参数1，若留空或‘NA’内容，则忽略）,param2（查询参数2，若留空或‘NA’内容，则忽略）,
			// porderby（排序设置，若空则不处理，若有内容则按两两一组，排序字段‘O_’+0或1处理）,
			// tasktype（任务类型，若留空则不检查用户权限）,tmfunc (解锁时间（tmfunc=1时查询视图loctime必须存在）)

			put("BINDCS2PL", "EXECUTE BINDCS2PL {0},{1},{2},{3}"); // 绑定箱到托盘 receiptkey（收货单号）,palletid（托盘号）,caseid（箱号，可空，若空则只执行关闭托盘）,isplclose(是否关闭托盘，1=关闭）
			put("USERDBRIGHT", "EXECUTE USERDBRIGHT ,{0},{1}"); // 用户数据库权限(注：本内容调用的DB只能是ENTERPRISE) sendDelimiter（暂未知道用途，留空）,userid,password
			put("TASKCSCHK", "EXECUTE TASKCSCHK {0},{1}"); // 按出库任务扫箱执行 taskdetailkey（任务号）,caseid（箱号）
			put("PICKCSCHK", "EXECUTE PICKCSCHK {0},{1}"); // 按拣货信息扫箱执行 pickdetailkey（拣货号）,caseid（箱号）
			put("BINDCS2PL2", "EXECUTE BINDCS2PL2 {0},{1},{2},{3},{4}"); // 绑定箱到托盘2 act(操作类型，可空，若为1则关闭托盘，若为0则重置该托盘，即清除该托盘对应的所有扫描信息[已收货托盘不可重置]）
																						// receiptkey（收货单号）,palletid（托盘号）,caseid（箱号，可空，若空则只执行关闭托盘）,qty数量（可空，若空则按找到的箱信息中的数量处理）
			put("PICKCSCHK2", "EXECUTE PICKCSCHK2 {0},{1},{2},{3}"); // 按拣货信息扫箱执行2 act(操作类型，可空，若为0则重置拣货扫码，即清除该拣货对应的所有扫描信息[已完成拣选的不可重置]）
																						// pickdetailkey（拣货号）,caseid（箱号）,qty数量（可空，若空则按找到的箱信息中的数量处理）
			put("PHYSICAL", "EXECUTE PHYSICAL {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}"); //盘点 TEAM 盘点小组名称（可空，默认为'A'）,LOC 库位（不能为空）,SKU 产品（不能为空）,
																										// STORERKEY 货主（可空）,INVENTORYTAG 库存标记（可空）,ID 托盘号（可空）,
																										// cLOT1 客户批次信息1（可空）,cLOT2 客户批次信息2（可空）,invstat 库存状态（可空）,
																										// UOM 包装（可空，仅支持EA、CS、PL 3个规格，空则根据查询视图定义，若查询视图中无定义，则视为EA）,
																										// UOMQTY 包装数量（不能为空）
			put("TESTSQL", "EXECUTE TESTSQL {0},{1}");
			put("GETSEL", "EXECUTE NSPRFGSEL ,,,,,,,,{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}");			//key1,key2,key3,key4,key5,key6,key7,key8,key9,choicenum,spera
			put("CREACCTASK", "EXECUTE NSPTMCCP2S1 ");
			put("UPDATETASK", "EXECUTE UPDATETASK {0},{1},{2},{3},{4}");	//更新任务状态 taskdetailkey（任务号）,toStatus（目标状态）,UserID（用户信息）,StatMsg（状态信息）,ReasonKey（原因代码）
			put("CHECKDATA", "EXECUTE CHECKDATA {0},{1},{2},{3},{4},{5},{6}");	//数据检查功能 mode（模式）,stdval（标准值）,chkval（检查值）,param1（参数1）,param2（参数2）,param3（参数3）,param4（参数4）
//			EXECUTE USERDBRIGHT ,xjx,123456
			// GETKEY keyname,fieldlength,increment
//			TMSHIPDROPID dropid
//			ADDPICKLOCATION storerkey,sku,loc 添加一行库存记录（若当前库存表没有）
//			HOLDRESULTSET InventoryHoldKey,holdlot,holdloc,holdid,status,hold,storerkey,sku,lottable02,lottable03,qty,whoon,lottable06,lottable07
//			MOVE ItrnSysId,StorerKey,Sku,Lot,FromID,FromLoc,ToLoc,ToID,Status,lottable01,lottable02,lottable03,lottable04,lottable05,lottable06,lottable07,lottable08,lottable09,lottable10,casecnt,innerpack,Qty,pallet,cube,grosswgt,netwgt,otherunit1,otherunit2,SourceKey,SourceType,PackKey,UOM,UOMCalc,EffectiveDate,Dummy1,Dummy2,Dummy3, trxtype, reasoncode, referenceno, invtrx_comment
//			NSPHOLD inventoryholdkey,hold,status,holdloc,holdid,holdlot status不能为空（codelkup RECHOLD），holdloc,holdid,holdlot只填一个，hold只有0（unhold）和1（hold）
//			BATCHCANDIDATES Wavekey,BatchSelectionkey
//			NSP_UPDATEOPCOMPONENTS  SkuByOpId,OperationId,WkcSkuId,QtyRequired,QtyUsed,Status
//			NSP_WOCREATE workorderkey,externwokey,duedate,externwodate,quantity
//			NSP_WORKORDERUPDATE workorderid,status,quantity,qtycomplete,notes,instructionsurl
//			NSPRFRCC001	sendDelimiter,ptcid,userid,taskId,databasename,appflag,recordType,server,receiptkey,storerkey,lot,prokey,sku,pokey,qty,uom,packkey,loc,id,hold,isrp,drid,lottable01,lottable02,lottable03,lottable04,lottable05,lottable06,lottable07,lottable08,lottable09,lottable10,other1,other2,other3,printerID,counter,wgt,reasoncode,RejectQty,Receiptlinenumber,Currentpalletid,newpalletid
//						,,,,,,,,receiptkey,storerkey,lot,prokey,sku,pokey,qty,uom,packkey,loc,id,hold,isrp,drid,lottable01,lottable02,lottable03,lottable04,lottable05,lottable06,lottable07,lottable08,lottable09,lottable10,other1,other2,other3,printerID,counter,wgt,reasoncode,RejectQty,Receiptlinenumber,Currentpalletid,newpalletid
//			AdjustmentAPI	user,function,xml
//			AdvancedShipNoticeAPI	user,function,xml
//			CycleCountAPI	user,function,xml
//			FlowThruOrderAPI	user,function,xml
//			InventoryBalanceAPI	user,function,xml
//			ItemMasterAPI	user,function,xml
//			PurchaseOrderAPI	user,function,xml
//			ShipmentOrderAPI	user,function,xml
//			StorerAPI	user,function,xml
//			TransferAPI	user,function,xml
//			TransShipASNAPI	user,function,xml
//			TransShipOrderAPI	user,function,xml
//			BPCSLotAPI	user,function,xml
//			LotMasterAPI	user,function,xml
//			HandleAPI	user,function,xml
		}
	};
	private static final Map<String, Integer> FunctionParanum = new HashMap<String, Integer>() {
		/**
		* 
		*/
		private static final long serialVersionUID = 1L;
		{
			put("BUILDWAVEP2S1", 4); // 按视图创建波次
			put("ALLOCATEORD", 1); // Orderkey
			put("SHIPPICK", 1); // Pickdetailkey
			put("RECEIPTASNLINE", 2); // receiptkey,receiptlinenumber
			put("RECEIPTASNLINE2",35);// receiptkey,storerkey,lot,prokey,sku,pokey,qty,uom,packkey,loc,id,hold,isrp,drid,lottable01,lottable02,lottable03,lottable04,lottable05,lottable06,lottable07,lottable08,lottable09,lottable10,other1,other2,other3,printerID,counter,wgt,reasoncode,RejectQty,Receiptlinenumber,Currentpalletid,newpalletid

			put("ASNBOOKING", 1); // receiptkey
			put("TRANSFERFINALIZE", 2); // TransferKey,TransferLineNumber
			put("TASKCOMPLETE", 1); // TaskDetailKey
			put("ASNCALCPUTAWAY", 1); // receiptkey
			put("ASNGENERATEID", 1); // receiptkey
			put("ASNCONFIRMPUTAWAY", 1);// receiptkey
			put("SHIPORD", 1); // orderkey
			put("RELEASEPATASK", 2); // 发布上架任务 receiptkey,storerkey
			put("UNALLOCORDERLINE", 2); // ORDERKEY,ORDERLINENUMBER
			put("HOLDBYID", 2); // status（原因，可查codelkup code=RECHOLD）,ID
			put("HOLDBYLOC", 2); // status（原因，可查codelkup code=RECHOLD）,LOC
			put("HOLDBYLOT", 2); // status（原因，可查codelkup code=RECHOLD）,LOT
			put("HOLDBYIDWITHKEY", 3); // status（原因，可查codelkup code=RECHOLD）,ID
			put("HOLDBYLOCWITHKEY", 3); // status（原因，可查codelkup code=RECHOLD）,LOC
			put("HOLDBYLOTWITHKEY", 3); // status（原因，可查codelkup code=RECHOLD）,LOT
			put("UNHOLDBYID", 2); // status（原因，可查codelkup code=RECHOLD）,ID
			put("UNHOLDBYLOC", 2); // status（原因，可查codelkup code=RECHOLD）,LOC
			put("UNHOLDBYLOT", 2); // status（原因，可查codelkup code=RECHOLD）,LOT
			put("UNHOLDBYIDWITHKEY", 3); // status（原因，可查codelkup code=RECHOLD）,ID
			put("UNHOLDBYLOCWITHKEY", 3); // status（原因，可查codelkup code=RECHOLD）,LOC
			put("UNHOLDBYLOTWITHKEY", 3); // status（原因，可查codelkup code=RECHOLD）,LOT
			put("MOVE2", 10); // fromloc,Storerkey（可留空）,Sku（可留空）,fromid（可留空）,toloc
			put("MOVE3", 7); // 库存移位 fromloc(源库位）,Storerkey（货主,可留空）,Sku（商品,可留空）,fromid（源托盘号,可留空）,
																			// toloc(目标库位）,refnum（单号，可留空）,sourcetype（引用类型,可留空）
			put("RELEASEWAVE", 3); // 按波次分配、发布 success,err,DoRelease  （DoRelease='N' 表示只做分配  'Y'表示分配和发布）
			put("BATCHORDER", 2); // 建立波次 Wavekey,BatchSelectionkey
			put("UNBATCHORDER", 1); // 取消波次 Wavekey
			put("GETDATALIST", 9);  // 获取数据列 userid（用户名，若留空则不检查用户权限）,area(区域，可留空）,viewname（视图名称）,
											// param1（查询参数1，若留空或‘NA’内容，则忽略）,param2（查询参数2，若留空或‘NA’内容，则忽略）,
											// porderby（排序设置，若空则不处理，若有内容则按两两一组，排序字段‘O_’+0或1处理）,
											// tasktype（任务类型，若留空则不检查用户权限）,tmfunc (解锁时间（tmfunc=1时查询视图loctime必须存在）)
			put("GETDATASET", 9); // 获取数据行 userid（用户名，若留空则不检查用户权限）,area(区域，可留空）,viewname（视图名称）,
											// param1（查询参数1，若留空或‘NA’内容，则忽略）,param2（查询参数2，若留空或‘NA’内容，则忽略）,
											// porderby（排序设置，若空则不处理，若有内容则按两两一组，排序字段‘O_’+0或1处理）,
											// tasktype（任务类型，若留空则不检查用户权限）,tmfunc (解锁时间（tmfunc=1时查询视图loctime必须存在）)
			put("BINDCS2PL", 4); // 绑定箱到托盘 receiptkey（收货单号）,palletid（托盘号）,caseid（箱号）,isplclose(是否关闭托盘，1=关闭）
			put("USERDBRIGHT",2); // 用户数据库权限 sendDelimiter（暂未知道用途，留空）,userid,password
			put("TASKCSCHK", 2); // 按出库任务扫箱执行 taskdetailkey（任务号）,caseid（箱号）
			put("PICKCSCHK", 2); // 按拣货信息扫箱执行 pickdetailkey（任务号）,caseid（箱号）
			put("BINDCS2PL2", 5); // 绑定箱到托盘2 act(操作类型，可空，若为1则关闭托盘，若为0则重置该托盘，即清除该托盘对应的所有扫描信息[已收货托盘不可重置]）
											// receiptkey（收货单号）,palletid（托盘号）,caseid（箱号，可空，若空则只执行关闭托盘）,qty数量（可空，若空则按找到的箱信息中的数量处理）
			put("PICKCSCHK2", 4); // 按拣货信息扫箱执行2 act(操作类型，可空，若为0则重置拣货扫码，即清除该拣货对应的所有扫描信息[已完成拣选的不可重置]）
											// pickdetailkey（拣货号）,caseid（箱号）,qty数量（可空，若空则按找到的箱信息中的数量处理）
			put("PHYSICAL", 11); //盘点 TEAM 盘点小组名称（可空，默认为'A'）,LOC 库位（不能为空）,SKU 产品（不能为空）,
											// STORERKEY 货主（可空）,INVENTORYTAG 库存标记（可空）,ID 托盘号（可空）,
											// cLOT1 客户批次信息1（可空）,cLOT2 客户批次信息2（可空）,invstat 库存状态（可空）,
											// UOM 包装（可空，仅支持EA、CS、PL 3个规格，空则根据查询视图定义，若查询视图中无定义，则视为EA）,
											// UOMQTY 包装数量（不能为空）
			put("TESTSQL",2);
			put("GETSEL", 11);			//key1,key2,key3,key4,key5,key6,key7,key8,key9,choicenum
			put("CREACCTASK", 0);
			put("UPDATETASK", 5);	//taskdetailkey,toStatus,UserID,StatMsg,ReasonKey
			put("CHECKDATA", 7);	//mode,stdval,chkval,param1,param2,param3,param4
		}
	};
	private static Logger logger = LoggerFactory.getLogger(ExceedUtil.class);
	public static final String DBResourceException = "FT_9998";
	public static final String FulfillLogicException = "FT_9997";
	public static final String FulfillSystemException = "FT_9999";
	public static final String[] Default_Setting_Paths = new String[] { "config/exceed.setting", "exceed.setting" };

	private static String ip = "";
	private static Integer port = 6867;
	private static String dbName;
	private static String userName;
	private static Integer socketTimeout;
	private static Integer blockingQueueSize;
	private static Integer corePoolSize = 1;
	private static Integer maximumPoolSize = 1;
	private static String codePage = "GB2312";
	private static Boolean settingloaded = false;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		ExceedUtil.ip = ip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		ExceedUtil.port = port;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		ExceedUtil.dbName = dbName;
	}

	public Integer getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(Integer socketTimeout) {
		ExceedUtil.socketTimeout = socketTimeout;
	}

	public Integer getBlockingQueueSize() {
		return blockingQueueSize;
	}

	public void setBlockingQueueSize(Integer blockingQueueSize) {
		ExceedUtil.blockingQueueSize = blockingQueueSize;
	}

	public static boolean getDefaultSetting() {
		for (String SettingPath : Default_Setting_Paths) {
			Props props;
			try {
				props = new Props(SettingPath);
				return LoadSetting(props);
			} catch (IORuntimeException ignore) {
				// ignore
			}
		}
		return false;
	}

	public static boolean LoadSetting(Props props) {
		if (props == null) {
			return false;
		}
		ip = props.getStr("IP").trim();
		port = props.getInt("Port");
		dbName = props.getStr("DBName").trim();
		socketTimeout = props.getInt("SocketTimeout");
		blockingQueueSize = props.getInt("BlockingQueueSize");
		corePoolSize = props.getInt("CorePoolSize");
		maximumPoolSize = props.getInt("MaximumPoolSize");
		userName = props.getStr("UserName").trim();
		codePage = props.getStr("CodePage").trim();
		settingloaded = true;
		logger.debug(props.toString());
		return true;
	}

	public static Map<String, String> runProc(String theProcName, String... args) {
		Map<String, String> result = new HashMap<String, String>();
		if (settingloaded == false) {
			if (!getDefaultSetting()) {
				result.put("code", "7");
				result.put("msg", "缺少配置");
				return result;
			}
		}
		ExceedConnector exceedSPRunner;
//		exceedSPRunner = new ExceedConnector(ip, port, dbName, socketTimeout);
		exceedSPRunner = new ExceedConnector(ip, port, userName,dbName, socketTimeout,codePage);
		if (executorService == null) {
			executorService = buildSingleThreadExecutor();
		}
//		ExecutorService executorService = buildSingleThreadExecutor();
		return exceedSPRunner.RunProc(executorService, theProcName, args);
	}

	public static Map<String, String> runSQL(String theSQLstr) {
		Map<String, String> result = new HashMap<String, String>();
		if (settingloaded == false) {
			if (!getDefaultSetting()) {
				result.put("code", "7");
				result.put("msg", "缺少配置");
				return result;
			}
		}
		ExceedConnector exceedSPRunner;
		exceedSPRunner = new ExceedConnector(ip, port, userName, dbName, socketTimeout, codePage);
		if (executorService == null) {
			executorService = buildSingleThreadExecutor();
		}
//		ExecutorService executorService = buildSingleThreadExecutor();
		return exceedSPRunner.RunSQL(executorService, theSQLstr);
	}

	public static Map<String, String> runFunc(String functionName, String args[]) {
		Map<String, String> result = new HashMap<String, String>();
		if (settingloaded == false) {
			if (!getDefaultSetting()) {
				result.put("code", "7");
				result.put("msg", "缺少配置");
				return result;
			}
		}
		ExceedConnector exceedConnector;
		logger.debug("Function Name:" + functionName + ",Args:" + args.length);
//		exceedConnector = new ExceedConnector(ip, port, dbName, socketTimeout);
		exceedConnector = new ExceedConnector(ip, port, userName,dbName, socketTimeout,codePage);
		if (executorService == null) {
			executorService = buildSingleThreadExecutor();
		}
		String theSQLstr = FunctionPattern.get(functionName);
		Integer paraNum = FunctionParanum.get(functionName);
//		logger.debug(theSQLstr);
//		logger.debug(args[0]);
		if (args.length!=paraNum){
			result.put("code", "7");
			result.put("msg", "参数数量不对，要求："+paraNum+" 实际："+args.length);
			return result;
		}

		switch (paraNum) {
		case 1:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0]));
			break;
		case 2:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1]));
			break;
		case 3:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2]));
			break;
		case 4:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3]));
			break;
		case 5:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3], args[4]));
			break;
		case 6:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3], args[4], args[5]));
			break;
		case 7:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3], args[4], args[5], args[6]));
			break;
		case 8:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]));
			break;
		case 9:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]));
			break;
		case 10:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]));
			break;
		case 11:
			theSQLstr = (MessageFormat.format(theSQLstr, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10]));
			break;
		}
//		logger.debug(theSQLstr);
		return exceedConnector.RunSQL(executorService, theSQLstr);
	}

	public static Map<String, String> runFunc(String theSQLstr) {
		Map<String, String> result = new HashMap<String, String>();
		if (settingloaded == false) {
			if (!getDefaultSetting()) {
				result.put("code", "7");
				result.put("msg", "缺少配置");
				return result;
			}
		}
		ExceedConnector exceedConnector;
		logger.debug("SQL:" + theSQLstr);
		exceedConnector = new ExceedConnector(ip, port, userName,dbName, socketTimeout,codePage);
		if (executorService == null) {
			executorService = buildSingleThreadExecutor();
		}
		return exceedConnector.RunSQL(executorService, theSQLstr);
	}

//  forteSocketConfig.buildForteSocket().RunProc(executorService,theProcName,autoTaskData.getKey1());
//  @Bean
	public static ExecutorService buildSingleThreadExecutor() {
		logger.info("初始化线程池......");
		logger.info("线程池配置(拒绝策略：放不进去抛错;阻塞队列size:{}", blockingQueueSize);
		logger.info("核心线程数：{};最大线程数：{}", corePoolSize, maximumPoolSize);
		RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
		BlockingQueue<Runnable> blockingQueue = new LinkedBlockingQueue<Runnable>(blockingQueueSize.intValue());
		ExecutorService pool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 0L, TimeUnit.MILLISECONDS,
				blockingQueue, handler);
		return pool;
	}

	public String getCodePage() {
		return codePage;
	}

	public void setCodePage(String codePage) {
		ExceedUtil.codePage = codePage;
	}
}
